package by.VetClient.form.model;

import javax.persistence.*;

@Entity
@Table(name = "blood_analysis_leukogram")
public class BloodAnalysisLeukogram {


    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Integer id;


    @Column(name = "DATE_ANALYSIS_LEUKOGRAM")
    private String dateAnalysisLeukogram;

    @Column(name = "TIME_ANALYSIS_LEUKOGRAM")
    private String timeAnalysisLeukogram;

    @Column(name = "OWNER_LEUKOGRAM")
    private String ownerLeukogram;

    @Column(name = "KIND_LEUKOGRAM")
    private String kindLeukogram;

    @Column(name = "BASE")
    private String base;

    @Column(name = "EOSIN")
    private String eosin;

    @Column(name = "NEUTROPHILS")
    private String neutrophils;

    @Column(name = "LYMPHOMA")
    private String lymphoma;

    @Column(name = "MONT")
    private String mont;

    @Column(name = "NUCLEAR")
    private String nuclear ;

    @Column(name = "ASSISTANT_LEUKOGRAM")
    private String assistantLeukogram;

    @Column(name = "L_CONCLUSION")
    private String l_conclusion;

    public BloodAnalysisLeukogram() {
    }

    public BloodAnalysisLeukogram(String dateAnalysisLeukogram, String timeAnalysisLeukogram, String ownerLeukogram, String kindLeukogram, String base, String eosin, String neutrophils, String lymphoma, String mont, String nuclear, String assistantLeukogram, String l_conclusion) {
        this.dateAnalysisLeukogram = dateAnalysisLeukogram;
        this.timeAnalysisLeukogram = timeAnalysisLeukogram;
        this.ownerLeukogram = ownerLeukogram;
        this.kindLeukogram = kindLeukogram;
        this.base = base;
        this.eosin = eosin;
        this.neutrophils = neutrophils;
        this.lymphoma = lymphoma;
        this.mont = mont;
        this.nuclear = nuclear;
        this.assistantLeukogram = assistantLeukogram;
        this.l_conclusion = l_conclusion;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDateAnalysisLeukogram() {
        return dateAnalysisLeukogram;
    }

    public void setDateAnalysisLeukogram(String dateAnalysisLeukogram) {
        this.dateAnalysisLeukogram = dateAnalysisLeukogram;
    }

    public String getTimeAnalysisLeukogram() {
        return timeAnalysisLeukogram;
    }

    public void setTimeAnalysisLeukogram(String timeAnalysisLeukogram) {
        this.timeAnalysisLeukogram = timeAnalysisLeukogram;
    }

    public String getOwnerLeukogram() {
        return ownerLeukogram;
    }

    public void setOwnerLeukogram(String ownerLeukogram) {
        this.ownerLeukogram = ownerLeukogram;
    }

    public String getKindLeukogram() {
        return kindLeukogram;
    }

    public void setKindLeukogram(String kindLeukogram) {
        this.kindLeukogram = kindLeukogram;
    }

    public String getBase() {
        return base;
    }

    public void setBase(String base) {
        this.base = base;
    }

    public String getEosin() {
        return eosin;
    }

    public void setEosin(String eosin) {
        this.eosin = eosin;
    }

    public String getNeutrophils() {
        return neutrophils;
    }

    public void setNeutrophils(String neutrophils) {
        this.neutrophils = neutrophils;
    }

    public String getLymphoma() {
        return lymphoma;
    }

    public void setLymphoma(String lymphoma) {
        this.lymphoma = lymphoma;
    }

    public String getMont() {
        return mont;
    }

    public void setMont(String mont) {
        this.mont = mont;
    }

    public String getNuclear() {
        return nuclear;
    }

    public void setNuclear(String nuclear) {
        this.nuclear = nuclear;
    }

    public String getAssistantLeukogram() {
        return assistantLeukogram;
    }

    public void setAssistantLeukogram(String assistantLeukogram) {
        this.assistantLeukogram = assistantLeukogram;
    }

    public String getL_conclusion() {
        return l_conclusion;
    }

    public void setL_conclusion(String l_conclusion) {
        this.l_conclusion = l_conclusion;
    }
}
