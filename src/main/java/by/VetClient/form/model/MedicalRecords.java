package by.VetClient.form.model;

import javax.persistence.*;

@Entity
@Table(name = "medical_records")
public class MedicalRecords {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column(name = "NUMBER_HISTORY")
    private Integer numberMedicalHistory;

    @Column(name = "name")
    private String name;

    @Column(name = "LAST_NAME")
    private String lastName;

    @Column(name = "SECOND_NAME")
    private String secondName;

    @Column(name = "NUMBER_PASSPORT")
    private String numberPassport;

    @Column(name = "MOBILE_PHONE")
    private String mobilePhone;


    @Column(name = "COUNTRY")
    private String country;

    @Column(name = "DISTRICT")
    private String district;

    @Column(name = "CITY")
    private String city;

    @Column(name = "STREET")
    private String street;

    @Column(name = "NUMBER_HOME")
    private String numberHome;

    @Column(name = "NUMBER_FLAT")
    private String numberFlat;


    @Column(name = "DOCTOR")
    private String doctor;

    @Column(name = "TARGET")
    private String target;


    @Column(name = "KIND")
    private String kind;

    @Column(name = "ANIMAL_NAME")
    private String animalName;

    @Column(name = "SEX")
    private String sex;

    @Column(name = "AGE")
    private String age;

    @Column(name = "BREED")
    private String breed;

    @Column(name = "FATNESS")
    private String fatness;

    @Column(name = "DEWORMING")
    private String deworming;

    @Column(name = "NUMBER_VET_PASSPORT")
    private String numberVetPassport;

    @Column(name = "DATE_OF_ISSUE")
    private String dateOfIssue;

    @Column(name = "ISSUED")
    private String issued;

    @Column(name = "VACCINE")
    private String vaccine;

    @Column(name = "DATE_VACCINE")
    private String dateVaccine;

    @Column(name = "VACCINE_BY")
    private String vaccinedBy;

    @Column(name = "SPECIAL_SIGNS")
    private String specialSigns;

    @Column(name = "DATA_AND_TIME_REQUEST")
    private String dataAndTimeRequest;

    @Column(name = "REQUEST")
    private String request;

    @Column(name = "PRE_DIAGNOSIS")
    private String preDiagnosis;

    @Column(name = "DATE_AND_TIME_ANALYSIS")
    private String dateAndTimeAnalysis;

    @Column(name = "TEMPERATURE")
    private String temperature;

    @Column(name = "PULSE")
    private String pulse;

    @Column(name = "BREATH")
    private String breath;

    @Column(name = "HABIT")
    private String habit;

    @Column(name = "DERMIS_ANALYSIS")
    private String dermisAnalysis;

    @Column(name = "LYMPH_ANALYSIS")
    private String lymphAnalysis;

    @Column(name = "MUCOUS_ANALYSIS")
    private String mucousAnalysis;

    @Column(name = "SYSTEM_CIRCULATORY")
    private String systemCirculatory;

    @Column(name = "SYSTEM_BREATH")
    private String systemBreath;

    @Column(name = "SYSTEM_INDIGESTION")
    private String systemIndigestion;

    @Column(name = "SYSTEM_URINATION")
    private String systemUrination;

    @Column(name = "SYSTEM_GENITAL")
    private String systemGenital;

    @Column(name = "SYSTEM_BREAST")
    private String systemBreast;

    @Column(name = "SYSTEM_MOVEMENT")
    private String systemMovement;

    @Column(name = "SYSTEM_SENSES")
    private String systemSenses;

    @Column(name = "SYSTEM_NERVOUS")
    private String systemNervous;

    @Column(name = "SEROLOGICAL_ANALYSIS")
    private String serologicalAnalysis;

    @Column(name = "ALLERGIC_ANALYSIS")
    private String allergicAnalysis;

    @Column(name = "BACTERIOLOGICAL_ANALYSIS")
    private String bacteriologicalAnalysis;

    @Column(name = "OTHER_ANALYSIS")
    private String othersAnalysis;

    @Column(name = "BLOOD_ANALYSIS")
    private String bloodAnalysis;

    @Column(name = "URINA_ANALYSIS")
    private String urinaAnalysis;

    @Column(name = "FAECES_ANALYSIS")
    private String faecesAnalysis;


    public MedicalRecords() {

    }

    public MedicalRecords(Integer numberMedicalHistory, String name, String lastName, String secondName, String numberPassport, String mobilePhone, String country, String district, String city, String street, String numberHome, String numberFlat, String doctor, String target, String kind, String animalName, String sex, String age, String breed, String fatness, String deworming, String numberVetPassport, String dateOfIssue, String issued, String vaccine, String dateVaccine, String vaccinedBy, String specialSigns, String dataAndTimeRequest, String request, String preDiagnosis, String dateAndTimeAnalysis, String temperature, String pulse, String breath, String habit, String dermisAnalysis, String lymphAnalysis, String mucousAnalysis, String systemCirculatory, String systemBreath, String systemIndigestion, String systemUrination, String systemGenital, String systemBreast, String systemMovement, String systemSenses, String systemNervous, String serologicalAnalysis, String allergicAnalysis, String bacteriologicalAnalysis, String othersAnalysis, String bloodAnalysis, String urinaAnalysis, String faecesAnalysis) {
        this.numberMedicalHistory = numberMedicalHistory;
        this.name = name;
        this.lastName = lastName;
        this.secondName = secondName;
        this.numberPassport = numberPassport;
        this.mobilePhone = mobilePhone;
        this.country = country;
        this.district = district;
        this.city = city;
        this.street = street;
        this.numberHome = numberHome;
        this.numberFlat = numberFlat;
        this.doctor = doctor;
        this.target = target;
        this.kind = kind;
        this.animalName = animalName;
        this.sex = sex;
        this.age = age;
        this.breed = breed;
        this.fatness = fatness;
        this.deworming = deworming;
        this.numberVetPassport = numberVetPassport;
        this.dateOfIssue = dateOfIssue;
        this.issued = issued;
        this.vaccine = vaccine;
        this.dateVaccine = dateVaccine;
        this.vaccinedBy = vaccinedBy;
        this.specialSigns = specialSigns;
        this.dataAndTimeRequest = dataAndTimeRequest;
        this.request = request;
        this.preDiagnosis = preDiagnosis;
        this.dateAndTimeAnalysis = dateAndTimeAnalysis;
        this.temperature = temperature;
        this.pulse = pulse;
        this.breath = breath;
        this.habit = habit;
        this.dermisAnalysis = dermisAnalysis;
        this.lymphAnalysis = lymphAnalysis;
        this.mucousAnalysis = mucousAnalysis;
        this.systemCirculatory = systemCirculatory;
        this.systemBreath = systemBreath;
        this.systemIndigestion = systemIndigestion;
        this.systemUrination = systemUrination;
        this.systemGenital = systemGenital;
        this.systemBreast = systemBreast;
        this.systemMovement = systemMovement;
        this.systemSenses = systemSenses;
        this.systemNervous = systemNervous;
        this.serologicalAnalysis = serologicalAnalysis;
        this.allergicAnalysis = allergicAnalysis;
        this.bacteriologicalAnalysis = bacteriologicalAnalysis;
        this.othersAnalysis = othersAnalysis;
        this.bloodAnalysis = bloodAnalysis;
        this.urinaAnalysis = urinaAnalysis;
        this.faecesAnalysis = faecesAnalysis;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getNumberMedicalHistory() {
        return numberMedicalHistory;
    }

    public void setNumberMedicalHistory(Integer numberMedicalHistory) {
        this.numberMedicalHistory = numberMedicalHistory;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public String getNumberPassport() {
        return numberPassport;
    }

    public void setNumberPassport(String numberPassport) {
        this.numberPassport = numberPassport;
    }

    public String getMobilePhone() {
        return mobilePhone;
    }

    public void setMobilePhone(String mobilePhone) {
        this.mobilePhone = mobilePhone;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getNumberHome() {
        return numberHome;
    }

    public void setNumberHome(String numberHome) {
        this.numberHome = numberHome;
    }

    public String getNumberFlat() {
        return numberFlat;
    }

    public void setNumberFlat(String numberFlat) {
        this.numberFlat = numberFlat;
    }

    public String getDoctor() {
        return doctor;
    }

    public void setDoctor(String doctor) {
        this.doctor = doctor;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public String getAnimalName() {
        return animalName;
    }

    public void setAnimalName(String animalName) {
        this.animalName = animalName;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getBreed() {
        return breed;
    }

    public void setBreed(String breed) {
        this.breed = breed;
    }

    public String getFatness() {
        return fatness;
    }

    public void setFatness(String fatness) {
        this.fatness = fatness;
    }

    public String getDeworming() {
        return deworming;
    }

    public void setDeworming(String deworming) {
        this.deworming = deworming;
    }

    public String getNumberVetPassport() {
        return numberVetPassport;
    }

    public void setNumberVetPassport(String numberVetPassport) {
        this.numberVetPassport = numberVetPassport;
    }

    public String getDateOfIssue() {
        return dateOfIssue;
    }

    public void setDateOfIssue(String dateOfIssue) {
        this.dateOfIssue = dateOfIssue;
    }

    public String getIssued() {
        return issued;
    }

    public void setIssued(String issued) {
        this.issued = issued;
    }

    public String getVaccine() {
        return vaccine;
    }

    public void setVaccine(String vaccine) {
        this.vaccine = vaccine;
    }

    public String getDateVaccine() {
        return dateVaccine;
    }

    public void setDateVaccine(String dateVaccine) {
        this.dateVaccine = dateVaccine;
    }

    public String getVaccinedBy() {
        return vaccinedBy;
    }

    public void setVaccinedBy(String vaccinedBy) {
        this.vaccinedBy = vaccinedBy;
    }

    public String getSpecialSigns() {
        return specialSigns;
    }

    public void setSpecialSigns(String specialSigns) {
        this.specialSigns = specialSigns;
    }

    public String getDataAndTimeRequest() {
        return dataAndTimeRequest;
    }

    public void setDataAndTimeRequest(String dataAndTimeRequest) {
        this.dataAndTimeRequest = dataAndTimeRequest;
    }

    public String getRequest() {
        return request;
    }

    public void setRequest(String request) {
        this.request = request;
    }

    public String getPreDiagnosis() {
        return preDiagnosis;
    }

    public void setPreDiagnosis(String preDiagnosis) {
        this.preDiagnosis = preDiagnosis;
    }

    public String getDateAndTimeAnalysis() {
        return dateAndTimeAnalysis;
    }

    public void setDateAndTimeAnalysis(String dateAndTimeAnalysis) {
        this.dateAndTimeAnalysis = dateAndTimeAnalysis;
    }

    public String getTemperature() {
        return temperature;
    }

    public void setTemperature(String temperature) {
        this.temperature = temperature;
    }

    public String getPulse() {
        return pulse;
    }

    public void setPulse(String pulse) {
        this.pulse = pulse;
    }

    public String getBreath() {
        return breath;
    }

    public void setBreath(String breath) {
        this.breath = breath;
    }

    public String getHabit() {
        return habit;
    }

    public void setHabit(String habit) {
        this.habit = habit;
    }

    public String getDermisAnalysis() {
        return dermisAnalysis;
    }

    public void setDermisAnalysis(String dermisAnalysis) {
        this.dermisAnalysis = dermisAnalysis;
    }

    public String getLymphAnalysis() {
        return lymphAnalysis;
    }

    public void setLymphAnalysis(String lymphAnalysis) {
        this.lymphAnalysis = lymphAnalysis;
    }

    public String getMucousAnalysis() {
        return mucousAnalysis;
    }

    public void setMucousAnalysis(String mucousAnalysis) {
        this.mucousAnalysis = mucousAnalysis;
    }

    public String getSystemCirculatory() {
        return systemCirculatory;
    }

    public void setSystemCirculatory(String systemCirculatory) {
        this.systemCirculatory = systemCirculatory;
    }

    public String getSystemBreath() {
        return systemBreath;
    }

    public void setSystemBreath(String systemBreath) {
        this.systemBreath = systemBreath;
    }

    public String getSystemIndigestion() {
        return systemIndigestion;
    }

    public void setSystemIndigestion(String systemIndigestion) {
        this.systemIndigestion = systemIndigestion;
    }

    public String getSystemUrination() {
        return systemUrination;
    }

    public void setSystemUrination(String systemUrination) {
        this.systemUrination = systemUrination;
    }

    public String getSystemGenital() {
        return systemGenital;
    }

    public void setSystemGenital(String systemGenital) {
        this.systemGenital = systemGenital;
    }

    public String getSystemBreast() {
        return systemBreast;
    }

    public void setSystemBreast(String systemBreast) {
        this.systemBreast = systemBreast;
    }

    public String getSystemMovement() {
        return systemMovement;
    }

    public void setSystemMovement(String systemMovement) {
        this.systemMovement = systemMovement;
    }

    public String getSystemSenses() {
        return systemSenses;
    }

    public void setSystemSenses(String systemSenses) {
        this.systemSenses = systemSenses;
    }

    public String getSystemNervous() {
        return systemNervous;
    }

    public void setSystemNervous(String systemNervous) {
        this.systemNervous = systemNervous;
    }

    public String getSerologicalAnalysis() {
        return serologicalAnalysis;
    }

    public void setSerologicalAnalysis(String serologicalAnalysis) {
        this.serologicalAnalysis = serologicalAnalysis;
    }

    public String getAllergicAnalysis() {
        return allergicAnalysis;
    }

    public void setAllergicAnalysis(String allergicAnalysis) {
        this.allergicAnalysis = allergicAnalysis;
    }

    public String getBacteriologicalAnalysis() {
        return bacteriologicalAnalysis;
    }

    public void setBacteriologicalAnalysis(String bacteriologicalAnalysis) {
        this.bacteriologicalAnalysis = bacteriologicalAnalysis;
    }

    public String getOthersAnalysis() {
        return othersAnalysis;
    }

    public void setOthersAnalysis(String othersAnalysis) {
        this.othersAnalysis = othersAnalysis;
    }

    public String getBloodAnalysis() {
        return bloodAnalysis;
    }

    public void setBloodAnalysis(String bloodAnalysis) {
        this.bloodAnalysis = bloodAnalysis;
    }

    public String getUrinaAnalysis() {
        return urinaAnalysis;
    }

    public void setUrinaAnalysis(String urinaAnalysis) {
        this.urinaAnalysis = urinaAnalysis;
    }

    public String getFaecesAnalysis() {
        return faecesAnalysis;
    }

    public void setFaecesAnalysis(String faecesAnalysis) {
        this.faecesAnalysis = faecesAnalysis;
    }
}
