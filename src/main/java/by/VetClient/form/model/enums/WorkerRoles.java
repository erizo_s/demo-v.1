package by.VetClient.form.model.enums;

public enum WorkerRoles {
    ADMIN,
    USER,
    ANONYMOUS;

    WorkerRoles() {
    }
}
