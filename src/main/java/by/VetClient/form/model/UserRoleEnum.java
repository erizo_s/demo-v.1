package by.VetClient.form.model;

public enum UserRoleEnum {

    ADMIN,
    USER,
    ANONYMOUS;

    UserRoleEnum() {
    }
}
