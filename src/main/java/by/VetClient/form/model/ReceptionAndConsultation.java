package by.VetClient.form.model;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "reception_and_consultation")
public class ReceptionAndConsultation {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column(name = "NAME_SERVICE")
    private String nameService;

    @Column(name = "PRICE")
    private Double price;

    @Column(name = "AMOUNT")
    private Double amount;

    @Column(name = "PRICE_USD")
    private Integer priceUsd;

    @OneToMany()
    private Set<MedicalOrders> medicalOrders;

    public ReceptionAndConsultation() {
    }

    public ReceptionAndConsultation(String nameService, Double price, Double amount, Integer priceUsd, Set<MedicalOrders> medicalOrders) {
        this.nameService = nameService;
        this.price = price;
        this.amount = amount;
        this.priceUsd = priceUsd;
        this.medicalOrders = medicalOrders;
    }

    public ReceptionAndConsultation(Set<MedicalOrders> medicalOrders) {
        this.medicalOrders = medicalOrders;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNameService() {
        return nameService;
    }

    public void setNameService(String nameService) {
        this.nameService = nameService;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Integer getPriceUsd() {
        return priceUsd;
    }

    public void setPriceUsd(Integer priceUsd) {
        this.priceUsd = priceUsd;
    }

    public Set<MedicalOrders> getMedicalOrders() {
        return medicalOrders;
    }

    public void setMedicalOrders(Set<MedicalOrders> medicalOrders) {
        this.medicalOrders = medicalOrders;
    }
}
