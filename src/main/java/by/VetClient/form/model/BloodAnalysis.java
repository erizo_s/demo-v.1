package by.VetClient.form.model;


import javax.persistence.*;

@Entity
@Table(name = "blood_analysis")
public class BloodAnalysis {

    @OneToOne(cascade = CascadeType.ALL)
    @PrimaryKeyJoinColumn
    private MedicalRecords medicalRecords;

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column(name = "DATE_ANALYSIS")
    private String dateAnalysis;

    @Column(name = "TIME_ANALYSIS")
    private String timeAnalysis;

    @Column(name = "OWNER")
    private String owner;

    @Column(name = "KIND")
    private String kind;

    @Column(name = "SPECIFIC_GRAVITY")
    private String specificGravity;

    @Column(name = "VISCOSITY")
    private String viscosity;

    @Column(name = "SOE")
    private String soe;

    @Column(name = "HEMOGLOBIN")
    private String hemoglobin;

    @Column(name = "CALCIUM")
    private String calcium;

    @Column(name = "INOGRANIC_PHOSPHORUS")
    private String inogranicPhosphorus;

    @Column(name = "RESERVE_ALKALINITY")
    private String reserveAlkalinity;

    @Column(name = "CAROTENE")
    private String carotene;

    @Column(name = "TOTAL_PROTEIN")
    private String totalProtein;

    @Column(name = "BILIRUBIN")
    private String biliRubin;

    @Column(name = "GLUCOSE")
    private String glucose;

    @Column(name = "ASSISTANT")
    private String assistant;

    @Column(name = "CONCLUSION")
    private String conclusion;


    public BloodAnalysis(){

    }

    public BloodAnalysis(MedicalRecords medicalRecords, String dateAnalysis, String timeAnalysis, String owner, String kind, String specificGravity, String viscosity, String soe, String hemoglobin, String calcium, String inogranicPhosphorus, String reserveAlkalinity, String carotene, String totalProtein, String biliRubin, String glucose, String assistant, String conclusion) {
        this.medicalRecords = medicalRecords;
        this.dateAnalysis = dateAnalysis;
        this.timeAnalysis = timeAnalysis;
        this.owner = owner;
        this.kind = kind;
        this.specificGravity = specificGravity;
        this.viscosity = viscosity;
        this.soe = soe;
        this.hemoglobin = hemoglobin;
        this.calcium = calcium;
        this.inogranicPhosphorus = inogranicPhosphorus;
        this.reserveAlkalinity = reserveAlkalinity;
        this.carotene = carotene;
        this.totalProtein = totalProtein;
        this.biliRubin = biliRubin;
        this.glucose = glucose;
        this.assistant = assistant;
        this.conclusion = conclusion;
    }

    public MedicalRecords getMedicalRecords() {
        return medicalRecords;
    }

    public void setMedicalRecords(MedicalRecords medicalRecords) {
        this.medicalRecords = medicalRecords;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDateAnalysis() {
        return dateAnalysis;
    }

    public void setDateAnalysis(String dateAnalysis) {
        this.dateAnalysis = dateAnalysis;
    }

    public String getTimeAnalysis() {
        return timeAnalysis;
    }

    public void setTimeAnalysis(String timeAnalysis) {
        this.timeAnalysis = timeAnalysis;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public String getSpecificGravity() {
        return specificGravity;
    }

    public void setSpecificGravity(String specificGravity) {
        this.specificGravity = specificGravity;
    }

    public String getViscosity() {
        return viscosity;
    }

    public void setViscosity(String viscosity) {
        this.viscosity = viscosity;
    }

    public String getSoe() {
        return soe;
    }

    public void setSoe(String soe) {
        this.soe = soe;
    }

    public String getHemoglobin() {
        return hemoglobin;
    }

    public void setHemoglobin(String hemoglobin) {
        this.hemoglobin = hemoglobin;
    }

    public String getCalcium() {
        return calcium;
    }

    public void setCalcium(String calcium) {
        this.calcium = calcium;
    }

    public String getInogranicPhosphorus() {
        return inogranicPhosphorus;
    }

    public void setInogranicPhosphorus(String inogranicPhosphorus) {
        this.inogranicPhosphorus = inogranicPhosphorus;
    }

    public String getReserveAlkalinity() {
        return reserveAlkalinity;
    }

    public void setReserveAlkalinity(String reserveAlkalinity) {
        this.reserveAlkalinity = reserveAlkalinity;
    }

    public String getCarotene() {
        return carotene;
    }

    public void setCarotene(String carotene) {
        this.carotene = carotene;
    }

    public String getTotalProtein() {
        return totalProtein;
    }

    public void setTotalProtein(String totalProtein) {
        this.totalProtein = totalProtein;
    }

    public String getBiliRubin() {
        return biliRubin;
    }

    public void setBiliRubin(String biliRubin) {
        this.biliRubin = biliRubin;
    }

    public String getGlucose() {
        return glucose;
    }

    public void setGlucose(String glucose) {
        this.glucose = glucose;
    }

    public String getAssistant() {
        return assistant;
    }

    public void setAssistant(String assistant) {
        this.assistant = assistant;
    }

    public String getConclusion() {
        return conclusion;
    }

    public void setConclusion(String conclusion) {
        this.conclusion = conclusion;
    }
}
