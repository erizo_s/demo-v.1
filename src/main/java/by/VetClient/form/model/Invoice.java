package by.VetClient.form.model;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "invoice")
public class Invoice {


    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Integer id;

    @Column(name = "NUMBER_ORDER")
    private Integer number_order;

    @Column(name = "STATUS")
    private String status;

    @Column(name = "DATE")
    private String date;

    @Column(name = "NAME")
    private String name;

    @Column(name = "SKU")
    private Integer sku;

    @Column(name = "NOMINATION")
    private String nomination;

    @Column(name = "AMOUNT")
    private Integer amount;

    @Column(name = "PRICE")
    private Integer price;

    @Column(name = "ORDER")
    private Integer order;

    @Column(name = "TOTAL_PRICE")
    private Integer total_price;


    public Invoice(){

    }

    public Invoice(Integer number_order, String status, String date, String name, Integer sku, String nomination, Integer amount, Integer price, Integer order, Integer total_price) {
        this.number_order = number_order;
        this.status = status;
        this.date = date;
        this.name = name;
        this.sku = sku;
        this.nomination = nomination;
        this.amount = amount;
        this.price = price;
        this.order = order;
        this.total_price = total_price;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getNumber_order() {
        return number_order;
    }

    public void setNumber_order(Integer number_order) {
        this.number_order = number_order;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getSku() {
        return sku;
    }

    public void setSku(Integer sku) {
        this.sku = sku;
    }

    public String getNomination() {
        return nomination;
    }

    public void setNomination(String nomination) {
        this.nomination = nomination;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }

    public Integer getTotal_price() {
        return total_price;
    }

    public void setTotal_price(Integer total_price) {
        this.total_price = total_price;
    }


}
