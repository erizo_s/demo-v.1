package by.VetClient.form.model;


import javax.persistence.*;

@Entity
@Table(name = "faecal_flotation_microscopic")
public class FaecalFlotationMicroscopic {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column(name = "MICROSCOPIC_DATE_ANALYSIS")
    private String microscopicDateAnalysis;

    @Column(name = "MICROSCOPIC_TIME_ANALYSIS")
    private String microscopicTimeAnalysis;

    @Column(name = "MICROSCOPIC_OWNER")
    private String microscopicOwner;

    @Column(name = "MICROSCOPIC_KIND")
    private String microscopicKind;

    @Column(name = "BLOOD")
    private String blood;

    @Column(name = "MICROSCOPIC_SLIME")
    private String microscopicSlime;

    @Column(name = "DISEASES_BY_DARLING")
    private String diseasesByDarling;

    @Column(name = "DISEASES_BY_BERMAN_ORLOV")
    private String diseasesByBermanOrlov;

    @Column(name = "DISEASES_BY_NATIVE_SMEAR")
    private String diseasesByNativeSmear;

    @Column(name = "DISEASES_BY_SUCCESSIVE_WASHINGS")
    private String diseasesBySuccessiveWashings;

    @Column(name = "MICROSCOPIC_ASSISTANT")
    private String microscopicAssistant;

    @Column(name = "MICROSCOPIC_CONCLUSION")
    private String microscopicConclusion;

    public FaecalFlotationMicroscopic() {
    }

    public FaecalFlotationMicroscopic(String microscopicDateAnalysis, String microscopicTimeAnalysis, String microscopicOwner, String microscopicKind, String blood, String microscopicSlime, String diseasesByDarling, String diseasesByBermanOrlov, String diseasesByNativeSmear, String diseasesBySuccessiveWashings, String microscopicAssistant, String microscopicConclusion) {
        this.microscopicDateAnalysis = microscopicDateAnalysis;
        this.microscopicTimeAnalysis = microscopicTimeAnalysis;
        this.microscopicOwner = microscopicOwner;
        this.microscopicKind = microscopicKind;
        this.blood = blood;
        this.microscopicSlime = microscopicSlime;
        this.diseasesByDarling = diseasesByDarling;
        this.diseasesByBermanOrlov = diseasesByBermanOrlov;
        this.diseasesByNativeSmear = diseasesByNativeSmear;
        this.diseasesBySuccessiveWashings = diseasesBySuccessiveWashings;
        this.microscopicAssistant = microscopicAssistant;
        this.microscopicConclusion = microscopicConclusion;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMicroscopicDateAnalysis() {
        return microscopicDateAnalysis;
    }

    public void setMicroscopicDateAnalysis(String microscopicDateAnalysis) {
        this.microscopicDateAnalysis = microscopicDateAnalysis;
    }

    public String getMicroscopicTimeAnalysis() {
        return microscopicTimeAnalysis;
    }

    public void setMicroscopicTimeAnalysis(String microscopicTimeAnalysis) {
        this.microscopicTimeAnalysis = microscopicTimeAnalysis;
    }

    public String getMicroscopicOwner() {
        return microscopicOwner;
    }

    public void setMicroscopicOwner(String microscopicOwner) {
        this.microscopicOwner = microscopicOwner;
    }

    public String getMicroscopicKind() {
        return microscopicKind;
    }

    public void setMicroscopicKind(String microscopicKind) {
        this.microscopicKind = microscopicKind;
    }

    public String getBlood() {
        return blood;
    }

    public void setBlood(String blood) {
        this.blood = blood;
    }

    public String getMicroscopicSlime() {
        return microscopicSlime;
    }

    public void setMicroscopicSlime(String microscopicSlime) {
        this.microscopicSlime = microscopicSlime;
    }

    public String getDiseasesByDarling() {
        return diseasesByDarling;
    }

    public void setDiseasesByDarling(String diseasesByDarling) {
        this.diseasesByDarling = diseasesByDarling;
    }

    public String getDiseasesByBermanOrlov() {
        return diseasesByBermanOrlov;
    }

    public void setDiseasesByBermanOrlov(String diseasesByBermanOrlov) {
        this.diseasesByBermanOrlov = diseasesByBermanOrlov;
    }

    public String getDiseasesByNativeSmear() {
        return diseasesByNativeSmear;
    }

    public void setDiseasesByNativeSmear(String diseasesByNativeSmear) {
        this.diseasesByNativeSmear = diseasesByNativeSmear;
    }

    public String getDiseasesBySuccessiveWashings() {
        return diseasesBySuccessiveWashings;
    }

    public void setDiseasesBySuccessiveWashings(String diseasesBySuccessiveWashings) {
        this.diseasesBySuccessiveWashings = diseasesBySuccessiveWashings;
    }

    public String getMicroscopicAssistant() {
        return microscopicAssistant;
    }

    public void setMicroscopicAssistant(String microscopicAssistant) {
        this.microscopicAssistant = microscopicAssistant;
    }

    public String getMicroscopicConclusion() {
        return microscopicConclusion;
    }

    public void setMicroscopicConclusion(String microscopicConclusion) {
        this.microscopicConclusion = microscopicConclusion;
    }
}
