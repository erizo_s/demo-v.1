package by.VetClient.form.model;

import javax.persistence.*;

@Entity
@Table(name = "uroscopy_analysis_chemical")
public class UroscopyAnalysisChemical {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column(name = "CHEMISTRY_DATE_ANALYSIS")
    private String chemistryDateAnalysis;

    @Column(name = "CHEMISTRY_TIME_ANALYSIS")
    private String chemistryTimeAnalysis;

    @Column(name = "OWNER_CHEMICAL")
    private String ownerChemical;

    @Column(name = "KIND_CHEMICAL")
    private String kindChemical;

    @Column(name = "REACTION")
    private String reaction;

    @Column(name = "PROTEIN")
    private String protein;

    @Column(name = "PROTEOSE")
    private String proteose;

    @Column(name = "SUGAR")
    private String sugar;

    @Column(name = "PIGMENTS_BLOOD")
    private String pigmentsBlood;

    @Column(name = "CHOLOCHROME")
    private String cholochrome;

    @Column(name = "INDICAN")
    private String indican;

    @Column(name = "UROBILIN")
    private String urobilin;

    @Column(name = "KETONE_BODIES")
    private String ketoneBodies;

    @Column(name = "ASSISTANT_CHEMICAL")
    private String assistantChemical;

    @Column(name = "CHEMISTRY_CONCLUSION")
    private String chemistryConclusion;

    public UroscopyAnalysisChemical() {
    }

    public UroscopyAnalysisChemical(String chemistryDateAnalysis, String chemistryTimeAnalysis, String ownerChemical, String kindChemical, String reaction, String protein, String proteose, String sugar, String pigmentsBlood, String cholochrome, String indican, String urobilin, String ketoneBodies, String assistantChemical, String chemistryConclusion) {
        this.chemistryDateAnalysis = chemistryDateAnalysis;
        this.chemistryTimeAnalysis = chemistryTimeAnalysis;
        this.ownerChemical = ownerChemical;
        this.kindChemical = kindChemical;
        this.reaction = reaction;
        this.protein = protein;
        this.proteose = proteose;
        this.sugar = sugar;
        this.pigmentsBlood = pigmentsBlood;
        this.cholochrome = cholochrome;
        this.indican = indican;
        this.urobilin = urobilin;
        this.ketoneBodies = ketoneBodies;
        this.assistantChemical = assistantChemical;
        this.chemistryConclusion = chemistryConclusion;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getChemistryDateAnalysis() {
        return chemistryDateAnalysis;
    }

    public void setChemistryDateAnalysis(String chemistryDateAnalysis) {
        this.chemistryDateAnalysis = chemistryDateAnalysis;
    }

    public String getChemistryTimeAnalysis() {
        return chemistryTimeAnalysis;
    }

    public void setChemistryTimeAnalysis(String chemistryTimeAnalysis) {
        this.chemistryTimeAnalysis = chemistryTimeAnalysis;
    }

    public String getOwnerChemical() {
        return ownerChemical;
    }

    public void setOwnerChemical(String ownerChemical) {
        this.ownerChemical = ownerChemical;
    }

    public String getKindChemical() {
        return kindChemical;
    }

    public void setKindChemical(String kindChemical) {
        this.kindChemical = kindChemical;
    }

    public String getReaction() {
        return reaction;
    }

    public void setReaction(String reaction) {
        this.reaction = reaction;
    }

    public String getProtein() {
        return protein;
    }

    public void setProtein(String protein) {
        this.protein = protein;
    }

    public String getProteose() {
        return proteose;
    }

    public void setProteose(String proteose) {
        this.proteose = proteose;
    }

    public String getSugar() {
        return sugar;
    }

    public void setSugar(String sugar) {
        this.sugar = sugar;
    }

    public String getPigmentsBlood() {
        return pigmentsBlood;
    }

    public void setPigmentsBlood(String pigmentsBlood) {
        this.pigmentsBlood = pigmentsBlood;
    }

    public String getCholochrome() {
        return cholochrome;
    }

    public void setCholochrome(String cholochrome) {
        this.cholochrome = cholochrome;
    }

    public String getIndican() {
        return indican;
    }

    public void setIndican(String indican) {
        this.indican = indican;
    }

    public String getUrobilin() {
        return urobilin;
    }

    public void setUrobilin(String urobilin) {
        this.urobilin = urobilin;
    }

    public String getKetoneBodies() {
        return ketoneBodies;
    }

    public void setKetoneBodies(String ketoneBodies) {
        this.ketoneBodies = ketoneBodies;
    }

    public String getAssistantChemical() {
        return assistantChemical;
    }

    public void setAssistantChemical(String assistantChemical) {
        this.assistantChemical = assistantChemical;
    }

    public String getChemistryConclusion() {
        return chemistryConclusion;
    }

    public void setChemistryConclusion(String chemistryConclusion) {
        this.chemistryConclusion = chemistryConclusion;
    }
}
