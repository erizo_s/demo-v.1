package by.VetClient.form.model;

import javax.persistence.*;

@Entity
@Table(name = "faecal_flotation_chemical")
public class FaecalFlotationChemical {


    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column(name = "CHEMISTRY_DATE_ANALYSIS")
    private String chemistryDateAnalysis;

    @Column(name = "CHEMISTRY_TIME_ANALYSIS")
    private String chemistryTimeAnalysis;

    @Column(name = "CHEMICAL_OWNER")
    private String chemicalOwner;

    @Column(name = "CHEMICAL_KIND")
    private String chemicalKind;

    @Column(name = "REACTION")
    private String reaction;

    @Column(name = "TOTAL_ACIDITY")
    private String totalAcidity;

    @Column(name = "PROTEIN")
    private String protein;

    @Column(name = "PIGMENTS_BLOOD")
    private String pigmentsBlood;

    @Column(name = "CHOLOCHROME")
    private String cholochrome;

    @Column(name = "AMMONIA")
    private String ammonia;

    @Column(name = "FERMENTATION_TEST")
    private String fermentationTest;

    @Column(name = "CHEMISTRY_ASSISTANT")
    private String chemistryAssistant;

    @Column(name = "CHEMISTRY_CONCLUSION")
    private String chemistryConclusion;

    public FaecalFlotationChemical() {
    }

    public FaecalFlotationChemical(String chemistryDateAnalysis, String chemistryTimeAnalysis, String chemicalOwner, String chemicalKind, String reaction, String totalAcidity, String protein, String pigmentsBlood, String cholochrome, String ammonia, String fermentationTest, String chemistryAssistant, String chemistryConclusion) {
        this.chemistryDateAnalysis = chemistryDateAnalysis;
        this.chemistryTimeAnalysis = chemistryTimeAnalysis;
        this.chemicalOwner = chemicalOwner;
        this.chemicalKind = chemicalKind;
        this.reaction = reaction;
        this.totalAcidity = totalAcidity;
        this.protein = protein;
        this.pigmentsBlood = pigmentsBlood;
        this.cholochrome = cholochrome;
        this.ammonia = ammonia;
        this.fermentationTest = fermentationTest;
        this.chemistryAssistant = chemistryAssistant;
        this.chemistryConclusion = chemistryConclusion;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getChemistryDateAnalysis() {
        return chemistryDateAnalysis;
    }

    public void setChemistryDateAnalysis(String chemistryDateAnalysis) {
        this.chemistryDateAnalysis = chemistryDateAnalysis;
    }

    public String getChemistryTimeAnalysis() {
        return chemistryTimeAnalysis;
    }

    public void setChemistryTimeAnalysis(String chemistryTimeAnalysis) {
        this.chemistryTimeAnalysis = chemistryTimeAnalysis;
    }

    public String getChemicalOwner() {
        return chemicalOwner;
    }

    public void setChemicalOwner(String chemicalOwner) {
        this.chemicalOwner = chemicalOwner;
    }

    public String getChemicalKind() {
        return chemicalKind;
    }

    public void setChemicalKind(String chemicalKind) {
        this.chemicalKind = chemicalKind;
    }

    public String getReaction() {
        return reaction;
    }

    public void setReaction(String reaction) {
        this.reaction = reaction;
    }

    public String getTotalAcidity() {
        return totalAcidity;
    }

    public void setTotalAcidity(String totalAcidity) {
        this.totalAcidity = totalAcidity;
    }

    public String getProtein() {
        return protein;
    }

    public void setProtein(String protein) {
        this.protein = protein;
    }

    public String getPigmentsBlood() {
        return pigmentsBlood;
    }

    public void setPigmentsBlood(String pigmentsBlood) {
        this.pigmentsBlood = pigmentsBlood;
    }

    public String getCholochrome() {
        return cholochrome;
    }

    public void setCholochrome(String cholochrome) {
        this.cholochrome = cholochrome;
    }

    public String getAmmonia() {
        return ammonia;
    }

    public void setAmmonia(String ammonia) {
        this.ammonia = ammonia;
    }

    public String getFermentationTest() {
        return fermentationTest;
    }

    public void setFermentationTest(String fermentationTest) {
        this.fermentationTest = fermentationTest;
    }

    public String getChemistryAssistant() {
        return chemistryAssistant;
    }

    public void setChemistryAssistant(String chemistryAssistant) {
        this.chemistryAssistant = chemistryAssistant;
    }

    public String getChemistryConclusion() {
        return chemistryConclusion;
    }

    public void setChemistryConclusion(String chemistryConclusion) {
        this.chemistryConclusion = chemistryConclusion;
    }
}
