package by.VetClient.form.model;


import java.sql.Time;
import java.time.LocalDate;

public class UserViewModel {

    private Integer id;
    private String name;
    private String lastName;
    private String email;
    private String mobilePhone;
    private String country;
    private String target;
    private String animal;
    private String passport;
    private String vaccination;
    private String symptoms;
    private String status;
    private Time time;
    private LocalDate localDate;
    private String doctorName;
    private Integer idDoctor;
    private String nameDepartment;
    private Integer idDepartment;



    public UserViewModel() {

    }

    public UserViewModel(Integer id, String name, String lastName, String email, String mobilePhone, String country, String target, String animal, String passport, String vaccination, String symptoms, String status, Time time, LocalDate localDate, String doctorName, Integer idDoctor, String nameDepartment, Integer idDepartment) {
        this.id = id;
        this.name = name;
        this.lastName = lastName;
        this.email = email;
        this.mobilePhone = mobilePhone;
        this.country = country;
        this.target = target;
        this.animal = animal;
        this.passport = passport;
        this.vaccination = vaccination;
        this.symptoms = symptoms;
        this.status = status;
        this.time = time;
        this.localDate = localDate;
        this.doctorName = doctorName;
        this.idDoctor = idDoctor;
        this.nameDepartment = nameDepartment;
        this.idDepartment = idDepartment;
    }

    public Integer getIdDepartment() {
        return idDepartment;
    }

    public void setIdDepartment(Integer idDepartment) {
        this.idDepartment = idDepartment;
    }

    public String getNameDepartment() {
        return nameDepartment;
    }

    public void setNameDepartment(String nameDepartment) {
        this.nameDepartment = nameDepartment;
    }

    public Time getTime() {
        return time;
    }

    public void setTime(Time time) {
        this.time = time;
    }

    public LocalDate getLocalDate() {
        return localDate;
    }

    public void setLocalDate(LocalDate localDate) {
        this.localDate = localDate;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobilePhone() {
        return mobilePhone;
    }

    public void setMobilePhone(String mobilePhone) {
        this.mobilePhone = mobilePhone;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public String getAnimal() {
        return animal;
    }

    public void setAnimal(String animal) {
        this.animal = animal;
    }

    public String getPassport() {
        return passport;
    }

    public void setPassport(String passport) {
        this.passport = passport;
    }

    public String getVaccination() {
        return vaccination;
    }

    public void setVaccination(String vaccination) {
        this.vaccination = vaccination;
    }

    public String getSymptoms() {
        return symptoms;
    }

    public void setSymptoms(String symptoms) {
        this.symptoms = symptoms;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDoctorName() {
        return doctorName;
    }

    public void setDoctorName(String doctorName) {
        this.doctorName = doctorName;
    }

    public Integer getIdDoctor() {
        return idDoctor;
    }

    public void setIdDoctor(Integer idDoctor) {
        this.idDoctor = idDoctor;
    }
}

