package by.VetClient.form.model;

import javax.persistence.*;



@Entity
@Table(name = "uroscopy_analysis")
public class UroscopyAnalysis {


    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    //Физические свойства мочи
    @Column(name = "DATE_ANALYSIS")
    private String dateAnalysis;

    @Column(name = "TIME_ANALYSIS")
    private String timeAnalysis;

    @Column(name = "OWNER")
    private String owner;

    @Column(name = "KIND")
    private String kind;

    @Column(name = "NUMBER")
    private String number;

    @Column(name = "COLOR")
    private String color;

    @Column(name = "TRANSPARENCY")
    private String transparency;

    @Column(name = "CONSISTENCY")
    private String consistency;

    @Column(name = "SMELL")
    private String smell;

    @Column(name = "DENSITY")
    private String density;

    @Column(name = "ASSISTANT")
    private String assistant;

    @Column(name = "CONCLUSION")
    private String conclusion;

    public UroscopyAnalysis(){

    }


    public UroscopyAnalysis(String dateAnalysis, String timeAnalysis, String owner, String kind, String number, String color, String transparency, String consistency, String smell, String density, String assistant, String conclusion) {
        this.dateAnalysis = dateAnalysis;
        this.timeAnalysis = timeAnalysis;
        this.owner = owner;
        this.kind = kind;
        this.number = number;
        this.color = color;
        this.transparency = transparency;
        this.consistency = consistency;
        this.smell = smell;
        this.density = density;
        this.assistant = assistant;
        this.conclusion = conclusion;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDateAnalysis() {
        return dateAnalysis;
    }

    public void setDateAnalysis(String dateAnalysis) {
        this.dateAnalysis = dateAnalysis;
    }

    public String getTimeAnalysis() {
        return timeAnalysis;
    }

    public void setTimeAnalysis(String timeAnalysis) {
        this.timeAnalysis = timeAnalysis;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getTransparency() {
        return transparency;
    }

    public void setTransparency(String transparency) {
        this.transparency = transparency;
    }

    public String getConsistency() {
        return consistency;
    }

    public void setConsistency(String consistency) {
        this.consistency = consistency;
    }

    public String getSmell() {
        return smell;
    }

    public void setSmell(String smell) {
        this.smell = smell;
    }

    public String getDensity() {
        return density;
    }

    public void setDensity(String density) {
        this.density = density;
    }

    public String getAssistant() {
        return assistant;
    }

    public void setAssistant(String assistant) {
        this.assistant = assistant;
    }

    public String getConclusion() {
        return conclusion;
    }

    public void setConclusion(String conclusion) {
        this.conclusion = conclusion;
    }
}
