package by.VetClient.form.model;

import javax.persistence.*;

@Entity
@Table(name = "medical_services")
public class MedicalServices {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column(name = "NAME_SERVICE")
    private String nameService;

    public MedicalServices() {
    }

    public MedicalServices(String nameService) {
        this.nameService = nameService;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNameService() {
        return nameService;
    }

    public void setNameService(String nameService) {
        this.nameService = nameService;
    }
}
