package by.VetClient.form.model;

import javax.persistence.*;


@Entity
@Table(name = "faecal_flotation")
public class FaecalFlotation {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column(name = "DATE_ANALYSIS")
    private String dateAnalysis;

    @Column(name = "TIME_ANALYSIS")
    private String timeAnalysis;

    @Column(name = "OWNER")
    private String owner;

    @Column(name = "KIND")
    private String kind;

    @Column(name = "AMOUNT")
    private String amount;

    @Column(name = "COLOR")
    private String color;

    @Column(name = "SMELL")
    private String smell;

    @Column(name = "FORM")
    private String form;

    @Column(name = "CONSISTENCY")
    private String consistency;

    @Column(name = "DIGESTIBILITY")
    private String digestibility;

    @Column(name = "SLIME")
    private String slime;

    @Column(name = "ASSISTANT")
    private String assistant;

    @Column(name = "CONCLUSION")
    private String conclusion;


    public FaecalFlotation() {
    }


    public FaecalFlotation(String dateAnalysis, String timeAnalysis, String owner, String kind, String amount, String color, String smell, String form, String consistency, String digestibility, String slime, String assistant, String conclusion) {
        this.dateAnalysis = dateAnalysis;
        this.timeAnalysis = timeAnalysis;
        this.owner = owner;
        this.kind = kind;
        this.amount = amount;
        this.color = color;
        this.smell = smell;
        this.form = form;
        this.consistency = consistency;
        this.digestibility = digestibility;
        this.slime = slime;
        this.assistant = assistant;
        this.conclusion = conclusion;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDateAnalysis() {
        return dateAnalysis;
    }

    public void setDateAnalysis(String dateAnalysis) {
        this.dateAnalysis = dateAnalysis;
    }

    public String getTimeAnalysis() {
        return timeAnalysis;
    }

    public void setTimeAnalysis(String timeAnalysis) {
        this.timeAnalysis = timeAnalysis;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getSmell() {
        return smell;
    }

    public void setSmell(String smell) {
        this.smell = smell;
    }

    public String getForm() {
        return form;
    }

    public void setForm(String form) {
        this.form = form;
    }

    public String getConsistency() {
        return consistency;
    }

    public void setConsistency(String consistency) {
        this.consistency = consistency;
    }

    public String getDigestibility() {
        return digestibility;
    }

    public void setDigestibility(String digestibility) {
        this.digestibility = digestibility;
    }

    public String getSlime() {
        return slime;
    }

    public void setSlime(String slime) {
        this.slime = slime;
    }

    public String getAssistant() {
        return assistant;
    }

    public void setAssistant(String assistant) {
        this.assistant = assistant;
    }

    public String getConclusion() {
        return conclusion;
    }

    public void setConclusion(String conclusion) {
        this.conclusion = conclusion;
    }
}
