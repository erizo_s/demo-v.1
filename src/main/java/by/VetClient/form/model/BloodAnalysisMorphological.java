package by.VetClient.form.model;

import javax.persistence.*;

@Entity
@Table(name = "blood_analysis_morphological")
public class BloodAnalysisMorphological {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Integer id;

    @Column(name = "DATE_ANALYSIS_MORPHOLOGICAL")
    private String dateAnalysisMorphological;

    @Column(name = "TIME_ANALYSIS_MORPHOLOGICAL")
    private String timeAnalysisMorphological;

    @Column(name = "OWNER_MORPHOLOGICAL")
    private String ownerMorphological;

    @Column(name = "KIND_MORPHOLOGICAL")
    private String kindMorphological;


    @Column(name = "RES_BLOOD_CELLS")
    private String redBloodCells;

    @Column(name = "LEUKOCYTE")
    private String leukocyte;

    @Column(name = "ASSISTANT_MORPHOLOGICAL")
    private String assistantMorphological;

    @Column(name = "M_CONCLUSION")
    private String m_conclusion;


    public BloodAnalysisMorphological() {
    }

    public BloodAnalysisMorphological(String dateAnalysisMorphological, String timeAnalysisMorphological, String ownerMorphological, String kindMorphological, String redBloodCells, String leukocyte, String assistantMorphological, String m_conclusion) {
        this.dateAnalysisMorphological = dateAnalysisMorphological;
        this.timeAnalysisMorphological = timeAnalysisMorphological;
        this.ownerMorphological = ownerMorphological;
        this.kindMorphological = kindMorphological;
        this.redBloodCells = redBloodCells;
        this.leukocyte = leukocyte;
        this.assistantMorphological = assistantMorphological;
        this.m_conclusion = m_conclusion;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDateAnalysisMorphological() {
        return dateAnalysisMorphological;
    }

    public void setDateAnalysisMorphological(String dateAnalysisMorphological) {
        this.dateAnalysisMorphological = dateAnalysisMorphological;
    }

    public String getTimeAnalysisMorphological() {
        return timeAnalysisMorphological;
    }

    public void setTimeAnalysisMorphological(String timeAnalysisMorphological) {
        this.timeAnalysisMorphological = timeAnalysisMorphological;
    }

    public String getOwnerMorphological() {
        return ownerMorphological;
    }

    public void setOwnerMorphological(String ownerMorphological) {
        this.ownerMorphological = ownerMorphological;
    }

    public String getKindMorphological() {
        return kindMorphological;
    }

    public void setKindMorphological(String kindMorphological) {
        this.kindMorphological = kindMorphological;
    }

    public String getRedBloodCells() {
        return redBloodCells;
    }

    public void setRedBloodCells(String redBloodCells) {
        this.redBloodCells = redBloodCells;
    }

    public String getLeukocyte() {
        return leukocyte;
    }

    public void setLeukocyte(String leukocyte) {
        this.leukocyte = leukocyte;
    }

    public String getAssistantMorphological() {
        return assistantMorphological;
    }

    public void setAssistantMorphological(String assistantMorphological) {
        this.assistantMorphological = assistantMorphological;
    }

    public String getM_conclusion() {
        return m_conclusion;
    }

    public void setM_conclusion(String m_conclusion) {
        this.m_conclusion = m_conclusion;
    }
}
