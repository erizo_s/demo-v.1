package by.VetClient.form.model;

import javax.persistence.*;

@Entity
@Table(name = "medical_orders")
public class MedicalOrders {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Integer id;

    @Column(name = "ID_ORDER")
    private Integer idOrder;

    @ManyToOne()
    @JoinColumn(name = "RECEPTION_AND_CONSULTATION_ID")
    private ReceptionAndConsultation receptionAndConsultation;

    public MedicalOrders() {
    }

    public MedicalOrders(Integer idOrder, ReceptionAndConsultation receptionAndConsultation) {
        this.idOrder = idOrder;
        this.receptionAndConsultation = receptionAndConsultation;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdOrder() {
        return idOrder;
    }

    public void setIdOrder(Integer idOrder) {
        this.idOrder = idOrder;
    }

    public ReceptionAndConsultation getReceptionAndConsultation() {
        return receptionAndConsultation;
    }

    public void setReceptionAndConsultation(ReceptionAndConsultation receptionAndConsultation) {
        this.receptionAndConsultation = receptionAndConsultation;
    }
}
