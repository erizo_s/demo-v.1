package by.VetClient.form.model;

import by.VetClient.form.converters.LocalDateConverter;

import javax.persistence.*;
import java.sql.Time;
import java.time.LocalDate;

@Entity
@Table(name = "users")
public class User {

    @Id
    @Column(name = "id")
    @GeneratedValue
    private Integer id;

    @Column(name = "name")
    private String name;

    @Column(name = "LAST_NAME")
    private String lastName;

    @Column(name = "email")
    private String email;

    @Column(name = "MOBILE_PHONE")
    private String mobilePhone;

    @Column(name = "COUNTRY")
    private String country;

    @ManyToOne()
    @JoinColumn(name = "DEPARTMENT_ID")
    private Department department;

    @ManyToOne()
    @JoinColumn(name = "DOCTOR_ID")
    private Doctor doctor;

    @Column(name = "ANIMAL")
    private String animal;

    @Column(name = "PASSPORT")
    private String passport;

    @Column(name = "VACCINATION")
    private String vaccination;

    @Column(name = "SYMPTOMS")
    private String symptoms;

    @Column(name = "STATUS")
    private String status;

    @Column(name = "TIME")
    private Time time;

    @Column(name = "INSERT_TIME")
    private Time insertTime;

    @Convert(converter = LocalDateConverter.class)
    @Column(name = "DATE")
    private LocalDate localDate;

    @Convert(converter = LocalDateConverter.class)
    @Column(name = "INSERT_DATE")
    private LocalDate insertDate;

    public User(Doctor doctor) {
        this.doctor = doctor;
    }

    public User(Department department) {
        this.department = department;
    }

    public User(String name) {
        this.name = name;
    }

    public User() {
    }

    public User(String name, String lastName, String email, String mobilePhone, String country, Department department, Doctor doctor, String animal, String passport, String vaccination, String symptoms, String status, Time time, Time insertTime, LocalDate localDate, LocalDate insertDate) {
        this.name = name;
        this.lastName = lastName;
        this.email = email;
        this.mobilePhone = mobilePhone;
        this.country = country;
        this.department = department;
        this.doctor = doctor;
        this.animal = animal;
        this.passport = passport;
        this.vaccination = vaccination;
        this.symptoms = symptoms;
        this.status = status;
        this.time = time;
        this.insertTime = insertTime;
        this.localDate = localDate;
        this.insertDate = insertDate;
    }

    public String getDoctorName() {
        if (doctor == null) {
            return null;
        }
        return doctor.getDoctorName();
    }

    public String getNameDepartment() {
        if (department == null) {
            return null;
        }
        return department.getNameDepartment();
    }

    public Time getInsertTime() {
        return insertTime;
    }

    public void setInsertTime(Time insertTime) {
        this.insertTime = insertTime;
    }

    public LocalDate getInsertDate() {
        return insertDate;
    }

    public void setInsertDate(LocalDate insertDate) {
        this.insertDate = insertDate;
    }

    public Integer getIdDoctor() {
        return id;
    }

    public Integer getIdDepartment() {
        return id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobilePhone() {
        return mobilePhone;
    }

    public void setMobilePhone(String mobilePhone) {
        this.mobilePhone = mobilePhone;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public Doctor getDoctor() {
        return doctor;
    }

    public void setDoctor(Doctor doctor) {
        this.doctor = doctor;
    }

    public String getAnimal() {
        return animal;
    }

    public void setAnimal(String animal) {
        this.animal = animal;
    }

    public String getPassport() {
        return passport;
    }

    public void setPassport(String passport) {
        this.passport = passport;
    }

    public String getVaccination() {
        return vaccination;
    }

    public void setVaccination(String vaccination) {
        this.vaccination = vaccination;
    }

    public String getSymptoms() {
        return symptoms;
    }

    public void setSymptoms(String symptoms) {
        this.symptoms = symptoms;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Time getTime() {
        return time;
    }

    public void setTime(Time time) {
        this.time = time;
    }

    public LocalDate getLocalDate() {
        return localDate;
    }

    public void setLocalDate(LocalDate localDate) {
        this.localDate = localDate;
    }
}
