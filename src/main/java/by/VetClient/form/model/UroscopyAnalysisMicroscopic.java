package by.VetClient.form.model;

import javax.persistence.*;

@Entity
@Table(name = "uroscopy_analysis_microscopic")
public class UroscopyAnalysisMicroscopic {


    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column(name = "MICROSCOPIC_DATE_ANALYSIS")
    private String microscopicDateAnalysis;

    @Column(name = "MICROSCOPIC_TIME_ANALYSIS")
    private String microscopicTimeAnalysis;

    @Column(name = "MICROSCOPIC_OWNER")
    private String microscopicOwner;

    @Column(name = "MICROSCOPIC_KIND")
    private String microscopicKind;

    @Column(name = "FUGITIVE_RAINFALL")
    private String fugitiveRainfall;

    @Column(name = "ORGANIZED_RAINFALL")
    private String organizedRainfall;

    @Column(name = "MICROSCOPIC_ASSISTANT")
    private String microscopicAssistant;

    @Column(name = "MICROSCOPIC_CONCLUSION")
    private String microscopicConclusion;


    public UroscopyAnalysisMicroscopic() {
    }

    public UroscopyAnalysisMicroscopic(String microscopicDateAnalysis, String microscopicTimeAnalysis, String microscopicOwner, String microscopicKind, String fugitiveRainfall, String organizedRainfall, String microscopicAssistant, String microscopicConclusion) {
        this.microscopicDateAnalysis = microscopicDateAnalysis;
        this.microscopicTimeAnalysis = microscopicTimeAnalysis;
        this.microscopicOwner = microscopicOwner;
        this.microscopicKind = microscopicKind;
        this.fugitiveRainfall = fugitiveRainfall;
        this.organizedRainfall = organizedRainfall;
        this.microscopicAssistant = microscopicAssistant;
        this.microscopicConclusion = microscopicConclusion;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMicroscopicDateAnalysis() {
        return microscopicDateAnalysis;
    }

    public void setMicroscopicDateAnalysis(String microscopicDateAnalysis) {
        this.microscopicDateAnalysis = microscopicDateAnalysis;
    }

    public String getMicroscopicTimeAnalysis() {
        return microscopicTimeAnalysis;
    }

    public void setMicroscopicTimeAnalysis(String microscopicTimeAnalysis) {
        this.microscopicTimeAnalysis = microscopicTimeAnalysis;
    }

    public String getMicroscopicOwner() {
        return microscopicOwner;
    }

    public void setMicroscopicOwner(String microscopicOwner) {
        this.microscopicOwner = microscopicOwner;
    }

    public String getMicroscopicKind() {
        return microscopicKind;
    }

    public void setMicroscopicKind(String microscopicKind) {
        this.microscopicKind = microscopicKind;
    }

    public String getFugitiveRainfall() {
        return fugitiveRainfall;
    }

    public void setFugitiveRainfall(String fugitiveRainfall) {
        this.fugitiveRainfall = fugitiveRainfall;
    }

    public String getOrganizedRainfall() {
        return organizedRainfall;
    }

    public void setOrganizedRainfall(String organizedRainfall) {
        this.organizedRainfall = organizedRainfall;
    }

    public String getMicroscopicAssistant() {
        return microscopicAssistant;
    }

    public void setMicroscopicAssistant(String microscopicAssistant) {
        this.microscopicAssistant = microscopicAssistant;
    }

    public String getMicroscopicConclusion() {
        return microscopicConclusion;
    }

    public void setMicroscopicConclusion(String microscopicConclusion) {
        this.microscopicConclusion = microscopicConclusion;
    }
}
