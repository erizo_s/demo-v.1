package by.VetClient.form.dao;

import by.VetClient.form.model.Department;
import by.VetClient.form.model.Doctor;
import by.VetClient.form.model.User;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;


public interface UserDao {

	User findById(Integer id);

	List<User> findAllToday(LocalDate dateMin, LocalDate dateMax);

	void saveOrUpdate(User user);

//	void update(User user);

	void delete(Integer id);

	void update(Integer id);

	List loadUsersByName(String searchText);

	List loadUsersByDates(LocalDate minDate, LocalDate maxDate);

	List<Doctor> loadAllDoctors();

	List<Department> loadAllDepartments();
}