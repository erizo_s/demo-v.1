package by.VetClient.form.dao;


import by.VetClient.form.model.OrderTransit;

import java.util.List;

public interface OrderTransitDao {

    OrderTransit findById(Integer id);

    List<OrderTransit> findAll();

    void save(Integer id);

    void delete();

    void goToStorage();
}
