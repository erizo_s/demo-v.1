package by.VetClient.form.dao;

import by.VetClient.form.model.Invoice;
import by.VetClient.form.model.Storage;

import java.util.List;


public interface InvoiceDao {

    List<Invoice> findAll();


    void save(Invoice invoice);
}
