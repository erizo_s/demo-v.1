package by.VetClient.form.dao;

import by.VetClient.form.model.UroscopyAnalysis;
import by.VetClient.form.model.UroscopyAnalysisChemical;
import by.VetClient.form.model.UroscopyAnalysisMicroscopic;

import java.util.List;


public interface UroscopyAnalysisDao {
    List<UroscopyAnalysis> findAll();

    void save(UroscopyAnalysis uroscopyAnalysis);

    void saveChemicalAnalysis(UroscopyAnalysisChemical uroscopyAnalysisChemical);

    void saveUroscopyAnalysisMicroscopic(UroscopyAnalysisMicroscopic uroscopyAnalysisMicroscopic);

    UroscopyAnalysis findById(Integer userId);

    UroscopyAnalysisChemical findByIdUroscopyAnalysisChemical(Integer userId);

    UroscopyAnalysisMicroscopic findByIdUroscopyAnalysisMicroscopic(Integer userId);
}
