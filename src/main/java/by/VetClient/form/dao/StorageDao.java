package by.VetClient.form.dao;


import by.VetClient.form.model.Storage;
import by.VetClient.form.model.Transit;
import by.VetClient.form.model.User;

import java.util.List;

public interface StorageDao {


    List<Storage> findAll();


    void save(Storage storage);

}


