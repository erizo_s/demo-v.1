package by.VetClient.form.dao;


import by.VetClient.form.model.MedicalServices;
import by.VetClient.form.model.ReceptionAndConsultation;

import java.util.List;

public interface MedicalServicesDao {

    List<MedicalServices> loadAll();

    void save(MedicalServices medicalServices);

    void delete(Integer id);

    List<ReceptionAndConsultation> loadAllReception();

    void saveReception(ReceptionAndConsultation receptionAndConsultation);

    void deleteReceptionService(int id);

    ReceptionAndConsultation getIdReception(Integer id);
}
