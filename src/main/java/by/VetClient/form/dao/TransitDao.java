package by.VetClient.form.dao;

import by.VetClient.form.model.Transit;

import java.util.List;

public interface TransitDao {

    List<Transit> findAll();

    Transit findById(Integer userId);
}
