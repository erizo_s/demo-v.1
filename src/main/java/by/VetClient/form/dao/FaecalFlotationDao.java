package by.VetClient.form.dao;

import by.VetClient.form.model.FaecalFlotation;
import by.VetClient.form.model.FaecalFlotationChemical;
import by.VetClient.form.model.FaecalFlotationMicroscopic;
import by.VetClient.form.model.UroscopyAnalysis;

import java.util.List;

/**
 * Created by erizo on 18.5.16.
 */
public interface FaecalFlotationDao {

    List<FaecalFlotation> findAll();

    void save(FaecalFlotation faecalFlotation);

    void saveFaecalFlotationChemical(FaecalFlotationChemical faecalFlotationChemical);

    void saveFaecalFlotationMicroscopic(FaecalFlotationMicroscopic faecalFlotationMicroscopic);

    FaecalFlotation findById(Integer userId);

    FaecalFlotationChemical findByIdFaecalFlotationChemical(Integer userId);

    FaecalFlotationMicroscopic findByIdFaecalFlotationMicroscopic(Integer userId);
}
