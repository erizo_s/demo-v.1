package by.VetClient.form.dao;

import by.VetClient.form.model.BloodAnalysis;
import by.VetClient.form.model.BloodAnalysisLeukogram;
import by.VetClient.form.model.BloodAnalysisMorphological;

import java.util.List;

public interface BloodAnalysisDao {

    List<BloodAnalysis> findAll();

    void save(BloodAnalysis bloodAnalysis);

    void saveBloodAnalysisMorphological(BloodAnalysisMorphological bloodAnalysisMorphological);

    void saveBloodAnalysisLeukogram(BloodAnalysisLeukogram bloodAnalysisLeukogram);

    BloodAnalysis findById(Integer id);

    BloodAnalysisMorphological findByIdBloodAnalysisMorphological(Integer id);

    BloodAnalysisLeukogram findByIdBloodAnalysisLeukogram(Integer id);
}
