package by.VetClient.form.dao;

import by.VetClient.form.model.*;

import java.util.List;


public interface MedicalRecordsDao {

    List<MedicalRecords> findAll();

    void save(MedicalRecords medicalRecords);

    MedicalRecords findById(Integer userId);

    List<MedicalOrders> getAllServices(Integer id);

    void saveOrders(ReceptionAndConsultation receptionAndConsultation);

    List<ReceptionAndConsultation> findByName(String tagName);

    void saveR(ReceptionAndConsultation receptionAndConsultation);
}

