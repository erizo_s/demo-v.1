package by.VetClient.form.dao;

import by.VetClient.form.model.Worker;

import java.util.List;

public interface WorkerDao {

    Worker findById(Integer id);

    List<Worker> findAll();

    void save(Worker worker);

//	void update(User user);
    void delete(Integer id);

    void update(Integer id);

    Worker findByLogin(String login);
}
