package by.VetClient.form.dao.impl;

import by.VetClient.form.dao.MedicalRecordsDao;
import by.VetClient.form.model.MedicalOrders;
import by.VetClient.form.model.MedicalRecords;
import by.VetClient.form.model.MedicalServices;
import by.VetClient.form.model.ReceptionAndConsultation;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MedicalRecordsDaoImp implements MedicalRecordsDao {


    @Autowired
    private SessionFactory sessionFactory;

    @SuppressWarnings("unchecked")
    public List<MedicalRecords> findAll() {
        return sessionFactory.getCurrentSession().createQuery("from MedicalRecords")
                .list();
    }

    @Override
    public void save(MedicalRecords medicalRecords) {
        sessionFactory.getCurrentSession().save(medicalRecords);
    }

    @Override
    public MedicalRecords findById(Integer userId) {
        return (MedicalRecords) sessionFactory.getCurrentSession().createQuery("select u from MedicalRecords u where id = :id").setParameter("id", userId).uniqueResult();
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<MedicalOrders> getAllServices(Integer id) {
        return (List<MedicalOrders>) sessionFactory.getCurrentSession().createQuery("select u from MedicalOrders u where idOrder = :id").setParameter("id", id).list();
    }

    @Override
    public void saveOrders(ReceptionAndConsultation receptionAndConsultation) {
        sessionFactory.getCurrentSession().save(receptionAndConsultation);
    }

    @Override
    public List<ReceptionAndConsultation> findByName(String tagName) {
        Query query = sessionFactory.getCurrentSession().createQuery("select u from Transit u where name LIKE :tagName");
        return query.setParameter("tagName", tagName + "%").list();
    }

    @Override
    public void saveR(ReceptionAndConsultation receptionAndConsultation) {
        sessionFactory.getCurrentSession().save(receptionAndConsultation);
    }
}
