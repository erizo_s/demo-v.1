package by.VetClient.form.dao.impl;

import by.VetClient.form.dao.WorkerDao;
import by.VetClient.form.model.Worker;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class WorkerDaoImpl implements WorkerDao {

    @Autowired
    private SessionFactory sessionFactory;

    NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Override
    public Worker findById(Integer id) {
        return (Worker) sessionFactory.getCurrentSession().createQuery("select u from Worker u where id = :id").setParameter("id", id).uniqueResult();
    }

    @SuppressWarnings("unchecked")
    public List<Worker> findAll() {
        return sessionFactory.getCurrentSession().createQuery("from Worker")
                .list();
    }

    @Override
    public void save(Worker worker) {
        sessionFactory.getCurrentSession().save(worker);
    }

    @Override
    public void delete(Integer id) {
        Worker worker = (Worker) sessionFactory.getCurrentSession().load(
                Worker.class, id);
        if (null != worker) {
            sessionFactory.getCurrentSession().delete(worker);
        }
    }

    @Override
    public void update(Integer id) {

    }

    @Override
    public Worker findByLogin(String login) {
        return (Worker) sessionFactory.getCurrentSession().createQuery("select u from Worker u where login = :login").setParameter("login", login).uniqueResult();
    }
}
