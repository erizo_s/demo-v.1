package by.VetClient.form.dao.impl;

import by.VetClient.form.dao.UroscopyAnalysisDao;
import by.VetClient.form.model.UroscopyAnalysis;
import by.VetClient.form.model.UroscopyAnalysisChemical;
import by.VetClient.form.model.UroscopyAnalysisMicroscopic;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class UroscopyAnalysisDaoImpl implements UroscopyAnalysisDao {


    @Autowired
    private SessionFactory sessionFactory;

    @SuppressWarnings("unchecked")
    @Override
    public List<UroscopyAnalysis> findAll() {
        return sessionFactory.getCurrentSession().createQuery("from UroscopyAnalysis ")
                .list();
    }

    @Override
    public void save(UroscopyAnalysis uroscopyAnalysis) {
        sessionFactory.getCurrentSession().save(uroscopyAnalysis);
    }

    @Override
    public void saveChemicalAnalysis(UroscopyAnalysisChemical uroscopyAnalysisChemical) {
        sessionFactory.getCurrentSession().save(uroscopyAnalysisChemical);
    }

    @Override
    public void saveUroscopyAnalysisMicroscopic(UroscopyAnalysisMicroscopic uroscopyAnalysisMicroscopic) {
        sessionFactory.getCurrentSession().save(uroscopyAnalysisMicroscopic);
    }

    @Override
    public UroscopyAnalysis findById(Integer userId) {
        return (UroscopyAnalysis) sessionFactory.getCurrentSession().createQuery("select u from UroscopyAnalysis u where id = :id").setParameter("id", userId).uniqueResult();
    }

    @Override
    public UroscopyAnalysisChemical findByIdUroscopyAnalysisChemical(Integer userId) {
        return (UroscopyAnalysisChemical) sessionFactory.getCurrentSession().createQuery("select u from UroscopyAnalysisChemical u where id = :id").setParameter("id", userId).uniqueResult();
    }

    @Override
    public UroscopyAnalysisMicroscopic findByIdUroscopyAnalysisMicroscopic(Integer userId) {
        return (UroscopyAnalysisMicroscopic) sessionFactory.getCurrentSession().createQuery("select u from UroscopyAnalysisMicroscopic u where id = :id").setParameter("id", userId).uniqueResult();
    }
}
