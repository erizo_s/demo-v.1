package by.VetClient.form.dao.impl;


import by.VetClient.form.dao.InvoiceDao;
import by.VetClient.form.model.Invoice;
import by.VetClient.form.model.Storage;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class InvoiceDaoImpl implements InvoiceDao {


    @Autowired
    private SessionFactory sessionFactory;


    @SuppressWarnings("unchecked")
    public List<Invoice> findAll() {
        return sessionFactory.getCurrentSession().createQuery("from Invoice")
                .list();
    }

    @Override
    public void save(Invoice invoice) {
        Session session = sessionFactory.openSession();
        sessionFactory.getCurrentSession().saveOrUpdate(invoice);
        session.close();

    }
}
