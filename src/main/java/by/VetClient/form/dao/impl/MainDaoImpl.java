package by.VetClient.form.dao.impl;


import by.VetClient.form.dao.MainDao;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MainDaoImpl implements MainDao{

    @Autowired
    private SessionFactory sessionFactory;


    @Override
    public List findById(String tagName) {
        Query query = sessionFactory.getCurrentSession().createQuery("select u from MedicalServices u where nameService LIKE :tagName");
        List list = query.setParameter("tagName", tagName + "%").list();
        return list;

    }
}
