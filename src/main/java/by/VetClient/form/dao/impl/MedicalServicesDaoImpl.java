package by.VetClient.form.dao.impl;


import by.VetClient.form.dao.MedicalServicesDao;
import by.VetClient.form.model.MedicalServices;
import by.VetClient.form.model.ReceptionAndConsultation;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MedicalServicesDaoImpl implements MedicalServicesDao {

    @Autowired
    private SessionFactory sessionFactory;

    @SuppressWarnings("unchecked")
    @Override
    public List<MedicalServices> loadAll() {
        return sessionFactory.getCurrentSession().createQuery("from MedicalServices")
                .list();
    }

    @Override
    public void save(MedicalServices medicalServices) {
        sessionFactory.getCurrentSession().saveOrUpdate(medicalServices);
    }

    @Override
    public void delete(Integer id) {
        MedicalServices medicalServices = (MedicalServices) sessionFactory.getCurrentSession().load(
                MedicalServices.class, id);
        if (null != medicalServices) {
            sessionFactory.getCurrentSession().delete(medicalServices);
        }
    }

    @Override
    public List<ReceptionAndConsultation> loadAllReception() {
        return sessionFactory.getCurrentSession().createQuery("from ReceptionAndConsultation")
                .list();
    }

    @Override
    public void saveReception(ReceptionAndConsultation receptionAndConsultation) {
        sessionFactory.getCurrentSession().saveOrUpdate(receptionAndConsultation);
    }

    @Override
    public void deleteReceptionService(int id) {
        ReceptionAndConsultation receptionAndConsultation = (ReceptionAndConsultation) sessionFactory.getCurrentSession().load(
                ReceptionAndConsultation.class, id);
        if (null != receptionAndConsultation) {
            sessionFactory.getCurrentSession().delete(receptionAndConsultation);
        }
    }

    @Override
    public ReceptionAndConsultation getIdReception(Integer id) {
        return (ReceptionAndConsultation) sessionFactory.getCurrentSession().createQuery("select u from ReceptionAndConsultation u where id = :id").setParameter("id", id).uniqueResult();
    }
}
