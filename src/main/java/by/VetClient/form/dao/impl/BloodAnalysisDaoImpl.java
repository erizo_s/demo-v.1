package by.VetClient.form.dao.impl;

import by.VetClient.form.dao.BloodAnalysisDao;
import by.VetClient.form.model.BloodAnalysis;
import by.VetClient.form.model.BloodAnalysisLeukogram;
import by.VetClient.form.model.BloodAnalysisMorphological;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BloodAnalysisDaoImpl implements BloodAnalysisDao {

    @Autowired
    private SessionFactory sessionFactory;

    @SuppressWarnings("unchecked")
    public List<BloodAnalysis> findAll() {
        return sessionFactory.getCurrentSession().createQuery("from BloodAnalysis ")
                .list();

    }

    @Override
    public void save(BloodAnalysis bloodAnalysis) {
        sessionFactory.getCurrentSession().save(bloodAnalysis);

    }

    @Override
    public void saveBloodAnalysisMorphological(BloodAnalysisMorphological bloodAnalysisMorphological) {
        sessionFactory.getCurrentSession().save(bloodAnalysisMorphological);
    }

    @Override
    public void saveBloodAnalysisLeukogram(BloodAnalysisLeukogram bloodAnalysisLeukogram) {
        sessionFactory.getCurrentSession().save(bloodAnalysisLeukogram);
    }

    @Override
    public BloodAnalysis findById(Integer id) {
        return (BloodAnalysis) sessionFactory.getCurrentSession().createQuery("select u from BloodAnalysis u where id = :id").setParameter("id", id).uniqueResult();
    }

    @Override
    public BloodAnalysisMorphological findByIdBloodAnalysisMorphological(Integer id) {
        return (BloodAnalysisMorphological) sessionFactory.getCurrentSession().createQuery("select u from BloodAnalysisMorphological u where id = :id").setParameter("id", id).uniqueResult();
    }

    @Override
    public BloodAnalysisLeukogram findByIdBloodAnalysisLeukogram(Integer id) {
        return (BloodAnalysisLeukogram) sessionFactory.getCurrentSession().createQuery("select u from BloodAnalysisLeukogram u where id = :id").setParameter("id", id).uniqueResult();
    }
}
