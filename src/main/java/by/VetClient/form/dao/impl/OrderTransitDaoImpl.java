package by.VetClient.form.dao.impl;


import by.VetClient.form.dao.OrderTransitDao;
import by.VetClient.form.model.OrderTransit;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrderTransitDaoImpl implements OrderTransitDao {

    @Autowired
    private SessionFactory sessionFactory;


    @Override
    public OrderTransit findById(Integer id) {
        return null;
    }

    @SuppressWarnings("unchecked")
    public List<OrderTransit> findAll() {
        return sessionFactory.getCurrentSession().createQuery("from OrderTransit")
                .list();
    }

    @Override
    public void save(Integer id) {
        sessionFactory.getCurrentSession().createQuery("insert into OrderTransit (sku,name, amount, price) select c.id,c.name, c.amount, c.price from Transit c where id = :id").setParameter("id", id).executeUpdate();
    }

    @Override
    public void delete() {
        sessionFactory.getCurrentSession().createQuery("delete from OrderTransit").executeUpdate();
    }

    @Override
    public void goToStorage() {
        sessionFactory.getCurrentSession().createQuery("insert into Storage (sku, name, amount, price) select c.sku, c.name, c.amount,c.price from OrderTransit c ").executeUpdate();
    }


}
