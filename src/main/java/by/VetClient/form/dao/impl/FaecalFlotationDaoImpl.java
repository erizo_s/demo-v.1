package by.VetClient.form.dao.impl;

import by.VetClient.form.dao.FaecalFlotationDao;
import by.VetClient.form.model.FaecalFlotation;
import by.VetClient.form.model.FaecalFlotationChemical;
import by.VetClient.form.model.FaecalFlotationMicroscopic;
import by.VetClient.form.model.UroscopyAnalysis;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FaecalFlotationDaoImpl implements FaecalFlotationDao {

    @Autowired
    private SessionFactory sessionFactory;

    @SuppressWarnings("unchecked")
    @Override
    public List<FaecalFlotation> findAll() {
        return  sessionFactory.getCurrentSession().createQuery("from FaecalFlotation ")
                .list();
    }

    @Override
    public void save(FaecalFlotation faecalFlotation) {
        sessionFactory.getCurrentSession().save(faecalFlotation);
    }

    @Override
    public void saveFaecalFlotationChemical(FaecalFlotationChemical faecalFlotationChemical) {
        sessionFactory.getCurrentSession().save(faecalFlotationChemical);
    }

    @Override
    public void saveFaecalFlotationMicroscopic(FaecalFlotationMicroscopic faecalFlotationMicroscopic) {
        sessionFactory.getCurrentSession().save(faecalFlotationMicroscopic);
    }

    @Override
    public FaecalFlotation findById(Integer userId) {
        return (FaecalFlotation) sessionFactory.getCurrentSession().createQuery("select u from FaecalFlotation u where id = :id").setParameter("id", userId).uniqueResult();
    }

    @Override
    public FaecalFlotationChemical findByIdFaecalFlotationChemical(Integer userId) {
        return (FaecalFlotationChemical) sessionFactory.getCurrentSession().createQuery("select u from FaecalFlotationChemical u where id = :id").setParameter("id", userId).uniqueResult();
    }

    @Override
    public FaecalFlotationMicroscopic findByIdFaecalFlotationMicroscopic(Integer userId) {
        return (FaecalFlotationMicroscopic) sessionFactory.getCurrentSession().createQuery("select u from FaecalFlotationMicroscopic u where id = :id").setParameter("id", userId).uniqueResult();
    }
}
