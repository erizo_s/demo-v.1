package by.VetClient.form.dao.impl;

import by.VetClient.form.dao.TransitDao;
import by.VetClient.form.model.MedicalRecords;
import by.VetClient.form.model.Transit;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TtransitDaoImpl implements TransitDao {


    @Autowired
    private SessionFactory sessionFactory;


    @SuppressWarnings("unchecked")
    public List<Transit> findAll() {
        return sessionFactory.getCurrentSession().createQuery("from Transit")
                .list();
    }

    @Override
    public Transit findById(Integer userId) {

        return (Transit) sessionFactory.getCurrentSession().createQuery("select name from Transit u where id = :id").setParameter("id", userId).list();
    }
}
