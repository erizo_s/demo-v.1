package by.VetClient.form.dao.impl;

import by.VetClient.form.dao.UserDao;
import by.VetClient.form.model.Department;
import by.VetClient.form.model.Doctor;
import by.VetClient.form.model.User;
import org.hibernate.*;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Service;

import java.sql.Time;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

@Service
public class UserDaoImpl implements UserDao {


    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public User findById(Integer id) {
        return (User) sessionFactory.getCurrentSession().createQuery("select u from User u where id = :id").setParameter("id", id).uniqueResult();
    }

    @SuppressWarnings("unchecked")
    public List<User> findAllToday(LocalDate dateMin, LocalDate dateMax) {
        Session session = sessionFactory.getCurrentSession();
        Criteria criteria;
        criteria = session.createCriteria(User.class);
        criteria.add(Restrictions.ge("localDate", dateMin));
        criteria.add(Restrictions.lt("localDate", dateMax));
        return criteria.list();
    }

    public void saveOrUpdate(User user) {
        Doctor doctor = (Doctor) sessionFactory.getCurrentSession().createQuery("select u from Doctor u where id = :id").setParameter("id", user.getDoctor().getId()).uniqueResult();
        user.setDoctor(doctor);
        if (user.getId() != null){
            user.setTime(user.getTime());
            user.setLocalDate(user.getLocalDate());
            user.setInsertTime(Time.valueOf(LocalTime.now()));
            user.setInsertDate(LocalDate.now());

        }else {
            user.setTime(Time.valueOf(LocalTime.now()));
            user.setLocalDate(LocalDate.now());
        }
        sessionFactory.getCurrentSession().saveOrUpdate(user);
    }

    public void delete(Integer id) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        User user = (User) sessionFactory.getCurrentSession().load(
                User.class, id);
        if (null != user) {
            sessionFactory.getCurrentSession().delete(user);
            tx.commit();
            session.close();
        }
    }

    public void update(Integer id) {
        User user = (User) sessionFactory.getCurrentSession().load(
                User.class, id);
        if (null != user) {
            sessionFactory.getCurrentSession().update(user);
        }
    }

    @Override
    public List loadUsersByName(String searchText) {
        Query query = sessionFactory.getCurrentSession().createQuery("select u from User u where lastName LIKE :tagName");
        return query.setParameter("tagName", "%" + searchText + "%").list();
    }

    @Override
    public List loadUsersByDates(LocalDate minDate, LocalDate maxDate) {
        Session session = sessionFactory.getCurrentSession();
        Criteria criteria = session.createCriteria(User.class);
        criteria.add(Restrictions.ge("localDate", minDate));
        criteria.add(Restrictions.lt("localDate", maxDate));
        return criteria.list();
    }

    @Override
    public List<Doctor> loadAllDoctors() {
        return sessionFactory.getCurrentSession().createQuery("FROM Doctor").list();
    }

    @Override
    public List<Department> loadAllDepartments() {
        return sessionFactory.getCurrentSession().createQuery("FROM Department").list();
    }
}
