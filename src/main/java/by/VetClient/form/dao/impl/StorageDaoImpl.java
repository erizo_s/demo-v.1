package by.VetClient.form.dao.impl;


import by.VetClient.form.dao.StorageDao;
import by.VetClient.form.model.Storage;
import by.VetClient.form.model.Transit;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StorageDaoImpl implements StorageDao {


    @Autowired
    private SessionFactory sessionFactory;


    @SuppressWarnings("unchecked")
    public List<Storage> findAll() {
        return sessionFactory.getCurrentSession().createQuery("from Storage")
                .list();
    }

    @Override
    public void save(Storage storage) {
            Session session = sessionFactory.openSession();
            sessionFactory.getCurrentSession().saveOrUpdate(storage);
            session.close();

    }


}
