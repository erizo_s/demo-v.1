package by.VetClient.form.parser;

import by.VetClient.form.model.Transit;
import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;
import com.mysql.jdbc.Statement;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.Row;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class EXCELTODB {


    public static void main() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection con = (Connection) DriverManager.getConnection("jdbc:mysql://localhost/testdb", "erizo", "4123");
            con.setAutoCommit(false);
            PreparedStatement pstm = null;
            FileInputStream file = new FileInputStream("D:\\price_0002.xls");
            POIFSFileSystem fs = new POIFSFileSystem(file);
            HSSFWorkbook wb = new HSSFWorkbook(fs);
            HSSFSheet sheet = wb.getSheetAt(0);
            Row row;
            for (int i = 1; i <= sheet.getLastRowNum(); i++) {
                row = sheet.getRow(i);
                String name = row.getCell(0).getStringCellValue();
                int amount = (int) row.getCell(1).getNumericCellValue();
                double price = row.getCell(2).getNumericCellValue();
                String sql = "INSERT INTO transit"
                        + "(name, amount, price) VALUES"
                        + "(?,?,?)";
                java.sql.PreparedStatement preparedStmt = con.prepareStatement(sql);
                preparedStmt.setString(1, name);
                preparedStmt.setInt(2, amount);
                preparedStmt.setDouble(3, price);
                preparedStmt.executeUpdate();
                System.out.println("Import rows " + i + name);
            }
            con.commit();
            con.close();
            file.close();
            System.out.println("Success import excel to mysql table");
        } catch (ClassNotFoundException | SQLException | IOException e) {
            System.out.println(e);
        }

    }


    public static void updatePrice() throws SQLException, ClassNotFoundException, IOException {

        Class.forName("com.mysql.jdbc.Driver");
        Connection con = (Connection) DriverManager.getConnection("jdbc:mysql://localhost/testdb", "erizo", "4123");
        con.setAutoCommit(false);
        PreparedStatement pstm = null;
        FileInputStream file = new FileInputStream("D:\\price_0002.xls");
        POIFSFileSystem fs = new POIFSFileSystem(file);
        HSSFWorkbook wb = new HSSFWorkbook(fs);
        HSSFSheet sheet = wb.getSheetAt(0);
        Row row;
        for (int i = 1; i <= sheet.getLastRowNum(); i++) {
            row = sheet.getRow(i);
            String name = row.getCell(0).getStringCellValue();
            int amount = (int) row.getCell(1).getNumericCellValue();
            double price = row.getCell(2).getNumericCellValue();
            String query = "SELECT id FROM transit where name=?";
            java.sql.PreparedStatement stmt = con.prepareStatement(query);
            stmt.setString(1, name);
            ResultSet r = stmt.executeQuery();
            if (r.next()) {
                String sqq = "UPDATE transit SET price=? WHERE id =?";
                java.sql.PreparedStatement preparedStatement = con.prepareStatement(sqq);
                preparedStatement.setDouble(1, price);
                preparedStatement.setLong(2, r.getLong("id"));
                preparedStatement.executeUpdate();
            } else {
                System.out.println("Добавлен новый товар");
                String sq = "INSERT INTO transit"
                        + "(name, amount, price) VALUES"
                        + "(?,?,?)";
                java.sql.PreparedStatement prepareStatement = con.prepareStatement(sq);
                prepareStatement.setString(1, name);
                prepareStatement.setInt(2, amount);
                prepareStatement.setDouble(3, price);
                prepareStatement.executeUpdate();
            }

        }
        con.commit();
        con.close();
        file.close();
    }
}








