package by.VetClient.form.converters;


import by.VetClient.form.model.User;
import by.VetClient.form.model.UserViewModel;

public class UserToUserViewModelConverter {

    public UserViewModel convert(User user) {
        UserViewModel userViewModel = new UserViewModel();
        userViewModel.setId(user.getId());
        userViewModel.setLastName(user.getLastName());
        userViewModel.setName(user.getName());
        userViewModel.setEmail(user.getEmail());
        userViewModel.setMobilePhone(user.getMobilePhone());
        userViewModel.setCountry(user.getCountry());
        userViewModel.setAnimal(user.getAnimal());
        userViewModel.setPassport(user.getPassport());
        userViewModel.setVaccination(user.getVaccination());
        userViewModel.setSymptoms(user.getSymptoms());
        userViewModel.setStatus(user.getStatus());
        userViewModel.setTime(user.getTime());
        userViewModel.setLocalDate(user.getLocalDate());
        return userViewModel;
    }
}
