package by.VetClient.form.converters;


import by.VetClient.form.model.Department;
import by.VetClient.form.model.Doctor;
import by.VetClient.form.model.User;
import by.VetClient.form.model.UserViewModel;

public class UserViewModelToUserConverter {

    public User convert(UserViewModel userViewModel) {
        User user = new User();
        Doctor doctor = new Doctor();
        Department department = new Department();
        user.setId(userViewModel.getId());
        user.setLastName(userViewModel.getLastName());
        user.setName(userViewModel.getName());
        user.setEmail(userViewModel.getEmail());
        user.setMobilePhone(userViewModel.getMobilePhone());
        user.setCountry(userViewModel.getCountry());
        user.setAnimal(userViewModel.getAnimal());
        user.setPassport(userViewModel.getPassport());
        user.setVaccination(userViewModel.getVaccination());
        user.setSymptoms(userViewModel.getSymptoms());
        user.setStatus(userViewModel.getStatus());
        user.setTime(userViewModel.getTime());
        user.setLocalDate(userViewModel.getLocalDate());
        doctor.setDoctorName(userViewModel.getDoctorName());
        doctor.setId(userViewModel.getIdDoctor());
        user.setDoctor(doctor);
        department.setNameDepartment(userViewModel.getNameDepartment());
        department.setId(userViewModel.getIdDepartment());
        user.setDepartment(department);
        return user;
    }
}
