package by.VetClient.form.service;


import by.VetClient.form.model.Invoice;
import by.VetClient.form.model.Storage;

import java.util.List;

public interface InvoiceService {


    List<Invoice> findAll();

    void saveOrUpdate(Invoice invoice);
}
