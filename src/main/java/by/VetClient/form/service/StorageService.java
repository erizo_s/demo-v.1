package by.VetClient.form.service;


import by.VetClient.form.model.Storage;

import java.util.List;

public interface StorageService {


    List<Storage> findAll();

    void saveOrUpdate(Storage storage);
}
