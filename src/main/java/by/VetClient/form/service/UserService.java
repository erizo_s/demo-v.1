package by.VetClient.form.service;

import by.VetClient.form.model.Department;
import by.VetClient.form.model.Doctor;
import by.VetClient.form.model.User;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;


public interface UserService {

	User findById(Integer id);
	
	List<User> findAllToday(LocalDate dateMin, LocalDate dateMax);

	void saveOrUpdate(User user);
	
	void delete(int id);

	List getUsersByName(String searchText);

	List getUsersByDates(LocalDate minDate, LocalDate maxDate);

	List<Doctor> getAllDoctors();

	List<Department> getAllDepartments();
}