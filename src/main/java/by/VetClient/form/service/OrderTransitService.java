package by.VetClient.form.service;

import by.VetClient.form.model.OrderTransit;

import java.util.List;


public interface OrderTransitService {

    OrderTransit findById(Integer id);

    List<OrderTransit> findAll();

    void saveOrUpdate(Integer id);

    void goToStorage();

    void delete(OrderTransit orderTransit);
}
