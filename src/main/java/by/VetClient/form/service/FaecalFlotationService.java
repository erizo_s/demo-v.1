package by.VetClient.form.service;

import by.VetClient.form.model.FaecalFlotation;
import by.VetClient.form.model.FaecalFlotationChemical;
import by.VetClient.form.model.FaecalFlotationMicroscopic;
import by.VetClient.form.model.UroscopyAnalysis;

import java.util.List;



public interface FaecalFlotationService {

    List<FaecalFlotation> findAll();

    void save(FaecalFlotation faecalFlotation);

    void saveFaecalFlotationChemical(FaecalFlotationChemical faecalFlotationChemical);

    void saveFaecalFlotationMicroscopic(FaecalFlotationMicroscopic faecalFlotationMicroscopic);

    FaecalFlotation findById(Integer userId);

    FaecalFlotationChemical findByIdFaecalFlotationChemical(Integer userId);

    FaecalFlotationMicroscopic findByIdFaecalFlotationMicroscopic(Integer userId);
}
