package by.VetClient.form.service;

import by.VetClient.form.model.Worker;

import java.util.List;

public interface WorkerService {


    Worker findById(Integer id);

    List<Worker> findAll();

    void saveOrUpdate(Worker worker);

    void delete(int id);

    void updateUser(Integer id);

    Worker findByLogin(String login);


}
