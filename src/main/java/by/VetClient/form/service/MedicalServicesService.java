package by.VetClient.form.service;


import by.VetClient.form.model.MedicalServices;
import by.VetClient.form.model.ReceptionAndConsultation;

import java.util.List;

public interface MedicalServicesService {

    List<MedicalServices> getAll();

    void saveOrUpdate(MedicalServices medicalServices);

    void delete(Integer id);

    List<ReceptionAndConsultation> getAllReception();

    void saveOrUpdateReception(ReceptionAndConsultation receptionAndConsultation);

    void deleteReceptionService(int id);

    ReceptionAndConsultation findByIdReception(Integer id);
}
