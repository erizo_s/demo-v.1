package by.VetClient.form.service.impl;

import by.VetClient.form.dao.BloodAnalysisDao;
import by.VetClient.form.model.BloodAnalysis;
import by.VetClient.form.model.BloodAnalysisLeukogram;
import by.VetClient.form.model.BloodAnalysisMorphological;
import by.VetClient.form.service.BloodAnalysisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class BloodAnalysisServiceImpl implements BloodAnalysisService {


    @Autowired
    private BloodAnalysisDao bloodAnalysisDao;


    @Transactional
    public List<BloodAnalysis> findAll() {
        return bloodAnalysisDao.findAll();
    }

    @Transactional
    public void save(BloodAnalysis bloodAnalysis) {
        bloodAnalysisDao.save(bloodAnalysis);
    }

    @Transactional
    public void saveBloodAnalysisMorphological(BloodAnalysisMorphological bloodAnalysisMorphological) {
        bloodAnalysisDao.saveBloodAnalysisMorphological(bloodAnalysisMorphological);
    }

    @Transactional
    public void saveBloodAnalysisLeukogram(BloodAnalysisLeukogram bloodAnalysisLeukogram) {
        bloodAnalysisDao.saveBloodAnalysisLeukogram(bloodAnalysisLeukogram);
    }

    @Transactional
    public BloodAnalysis findById(Integer id) {
        return bloodAnalysisDao.findById(id);
    }

    @Transactional
    public BloodAnalysisMorphological findByIdBloodAnalysisMorphological(Integer id) {
        return bloodAnalysisDao.findByIdBloodAnalysisMorphological(id);
    }

    @Transactional
    public BloodAnalysisLeukogram findByIdBloodAnalysisLeukogram(Integer id) {
        return bloodAnalysisDao.findByIdBloodAnalysisLeukogram(id);
    }
}
