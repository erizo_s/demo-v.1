package by.VetClient.form.service.impl;

import by.VetClient.form.dao.UserDao;
import by.VetClient.form.model.Department;
import by.VetClient.form.model.Doctor;
import by.VetClient.form.model.User;
import by.VetClient.form.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserDao userDao;

    @Transactional
    public User findById(Integer id) {
        return userDao.findById(id);
    }

    @Transactional
    public List<User> findAllToday(LocalDate dateMin, LocalDate dateMax) {
        return userDao.findAllToday(dateMin, dateMax);
    }

    @Transactional
    public void saveOrUpdate(User user) {
        userDao.saveOrUpdate(user);
    }

    @Transactional
    public void delete(int id) {
        userDao.delete(id);
    }

    @Transactional
    public List getUsersByName(String searchText) {
        return userDao.loadUsersByName(searchText);
    }

    @Transactional
    public List getUsersByDates(LocalDate minDate, LocalDate maxDate) {
        return userDao.loadUsersByDates(minDate, maxDate);
    }

    @Transactional
    public List<Doctor> getAllDoctors() {
        return userDao.loadAllDoctors();
    }

    @Transactional
    public List<Department> getAllDepartments() {
        return userDao.loadAllDepartments();
    }

}