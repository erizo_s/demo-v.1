package by.VetClient.form.service.impl;

import by.VetClient.form.dao.BloodAnalysisDao;
import by.VetClient.form.dao.UroscopyAnalysisDao;
import by.VetClient.form.model.UroscopyAnalysis;
import by.VetClient.form.model.UroscopyAnalysisChemical;
import by.VetClient.form.model.UroscopyAnalysisMicroscopic;
import by.VetClient.form.service.UroscopyAnalysisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class UroscopyAnalysisServiceImpl implements UroscopyAnalysisService {



    @Autowired
    private UroscopyAnalysisDao uroscopyAnalysisDao;


    @Transactional
    public List<UroscopyAnalysis> findAll() {
        return uroscopyAnalysisDao.findAll();
    }

    @Transactional
    public void save(UroscopyAnalysis uroscopyAnalysis) {
        uroscopyAnalysisDao.save(uroscopyAnalysis);
    }

    @Transactional
    public void saveChemicalAnalysis(UroscopyAnalysisChemical uroscopyAnalysisChemical) {
        uroscopyAnalysisDao.saveChemicalAnalysis(uroscopyAnalysisChemical);
    }

    @Transactional
    public void saveUroscopyAnalysisMicroscopic(UroscopyAnalysisMicroscopic uroscopyAnalysisMicroscopic) {
        uroscopyAnalysisDao.saveUroscopyAnalysisMicroscopic(uroscopyAnalysisMicroscopic);
    }

    @Transactional
    public UroscopyAnalysis findById(Integer userId) {
        return uroscopyAnalysisDao.findById(userId);
    }

    @Transactional
    public UroscopyAnalysisChemical findByIdUroscopyAnalysisChemical(Integer userId) {
        return uroscopyAnalysisDao.findByIdUroscopyAnalysisChemical(userId);
    }

    @Transactional
    public UroscopyAnalysisMicroscopic findByIdUroscopyAnalysisMicroscopic(Integer userId) {
        return uroscopyAnalysisDao.findByIdUroscopyAnalysisMicroscopic(userId);
    }


}
