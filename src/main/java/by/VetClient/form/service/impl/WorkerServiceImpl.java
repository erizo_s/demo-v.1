package by.VetClient.form.service.impl;

import by.VetClient.form.dao.WorkerDao;
import by.VetClient.form.model.Worker;
import by.VetClient.form.service.WorkerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
@Service
public class WorkerServiceImpl implements WorkerService {

    @Autowired
    private WorkerDao workerDao;


    @Transactional
    public Worker findById(Integer id) {
        return workerDao.findById(id);
    }

    @Transactional
    public List<Worker> findAll() {
        return workerDao.findAll();
    }

    @Transactional
    public void saveOrUpdate(Worker worker) {
        workerDao.save(worker);
    }

    @Transactional
    public void delete(int id) {
        workerDao.delete(id);
    }

    @Transactional
    public void updateUser(Integer id) {
        workerDao.update(id);
    }

    @Override
    @Transactional
    public Worker findByLogin(String login) {
        return workerDao.findByLogin(login);
    }
}
