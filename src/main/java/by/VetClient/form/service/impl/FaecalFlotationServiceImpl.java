package by.VetClient.form.service.impl;

import by.VetClient.form.dao.FaecalFlotationDao;
import by.VetClient.form.dao.UroscopyAnalysisDao;
import by.VetClient.form.model.FaecalFlotation;
import by.VetClient.form.model.FaecalFlotationChemical;
import by.VetClient.form.model.FaecalFlotationMicroscopic;
import by.VetClient.form.service.FaecalFlotationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class FaecalFlotationServiceImpl implements FaecalFlotationService {


    @Autowired
    private FaecalFlotationDao faecalFlotationDao;

    @Transactional
    public List<FaecalFlotation> findAll() {
        return faecalFlotationDao.findAll();
    }

    @Transactional
    public void save(FaecalFlotation faecalFlotation) {
        faecalFlotationDao.save(faecalFlotation);
    }

    @Transactional
    public void saveFaecalFlotationChemical(FaecalFlotationChemical faecalFlotationChemical) {
        faecalFlotationDao.saveFaecalFlotationChemical(faecalFlotationChemical);
    }

    @Transactional
    public void saveFaecalFlotationMicroscopic(FaecalFlotationMicroscopic faecalFlotationMicroscopic) {
        faecalFlotationDao.saveFaecalFlotationMicroscopic(faecalFlotationMicroscopic);
    }

    @Transactional
    public FaecalFlotation findById(Integer userId) {
        return faecalFlotationDao.findById(userId);
    }

    @Transactional
    public FaecalFlotationChemical findByIdFaecalFlotationChemical(Integer userId) {
        return faecalFlotationDao.findByIdFaecalFlotationChemical(userId);
    }

    @Transactional
    public FaecalFlotationMicroscopic findByIdFaecalFlotationMicroscopic(Integer userId) {
        return faecalFlotationDao.findByIdFaecalFlotationMicroscopic(userId);
    }
}
