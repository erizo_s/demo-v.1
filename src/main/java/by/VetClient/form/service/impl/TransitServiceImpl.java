package by.VetClient.form.service.impl;

import by.VetClient.form.dao.TransitDao;
import by.VetClient.form.model.OrderTransit;
import by.VetClient.form.model.Transit;
import by.VetClient.form.service.TransitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class TransitServiceImpl implements TransitService {


    @Autowired
    private TransitDao transitDao;


    @Transactional
    public List<Transit> findAll() {
        return transitDao.findAll();
    }

    @Transactional
    public Transit findById(Integer id) {
        return transitDao.findById(id);
    }
}
