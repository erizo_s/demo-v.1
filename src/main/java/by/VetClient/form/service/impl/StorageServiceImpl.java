package by.VetClient.form.service.impl;


import by.VetClient.form.dao.StorageDao;
import by.VetClient.form.dao.TransitDao;
import by.VetClient.form.model.Storage;
import by.VetClient.form.model.Transit;
import by.VetClient.form.service.StorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class StorageServiceImpl implements StorageService {

    @Autowired
    private StorageDao storageDao;


    @Transactional
    public List<Storage> findAll() {
        return storageDao.findAll();
    }

    @Transactional
    public void saveOrUpdate(Storage storage) {
        storageDao.save(storage);
    }


}
