package by.VetClient.form.service.impl;

import by.VetClient.form.dao.InvoiceDao;
import by.VetClient.form.dao.StorageDao;
import by.VetClient.form.model.Invoice;
import by.VetClient.form.service.InvoiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class InvoiceServiceImpl implements InvoiceService {

    @Autowired
    private InvoiceDao invoiceDao;


    @Transactional
    public List<Invoice> findAll() {
        return invoiceDao.findAll();
    }

    @Transactional
    public void saveOrUpdate(Invoice invoice) {
        invoiceDao.save(invoice);
    }
}
