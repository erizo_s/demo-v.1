package by.VetClient.form.service.impl;

import by.VetClient.form.dao.MedicalRecordsDao;
import by.VetClient.form.model.MedicalOrders;
import by.VetClient.form.model.MedicalRecords;
import by.VetClient.form.model.MedicalServices;
import by.VetClient.form.model.ReceptionAndConsultation;
import by.VetClient.form.service.MedicalRecordsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class MedicalRecordsServiceImpl implements MedicalRecordsService {

    @Autowired
    private MedicalRecordsDao medicalRecordsDao;

    @Transactional
    public List<MedicalRecords> findAll() {

        return medicalRecordsDao.findAll();
    }

    @Transactional
    public void saveOrUpdate(MedicalRecords medicalRecords) {
        medicalRecordsDao.save(medicalRecords);
    }

    @Transactional
    public MedicalRecords findById(Integer userId) {
        return medicalRecordsDao.findById(userId);
    }

    @Transactional
    public List<MedicalOrders> findByIdAllServices(Integer id) {
        return medicalRecordsDao.getAllServices(id);
    }

    @Transactional
    public void save(ReceptionAndConsultation receptionAndConsultation) {
        medicalRecordsDao.saveOrders(receptionAndConsultation);
    }

    @Transactional
    public List<ReceptionAndConsultation> findByName(String tagName) {
        return medicalRecordsDao.findByName(tagName);
    }

    @Transactional
    public void saveR(ReceptionAndConsultation receptionAndConsultation) {
        medicalRecordsDao.saveR(receptionAndConsultation);
    }
}
