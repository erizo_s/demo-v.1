package by.VetClient.form.service.impl;


import by.VetClient.form.dao.MedicalServicesDao;
import by.VetClient.form.model.MedicalServices;
import by.VetClient.form.model.ReceptionAndConsultation;
import by.VetClient.form.service.MedicalServicesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class MedicalServicesServiceImpl implements MedicalServicesService{

    @Autowired
    private MedicalServicesDao medicalServicesDao;

    @Transactional
    public List<MedicalServices> getAll() {
        return medicalServicesDao.loadAll();
    }

    @Transactional
    public void saveOrUpdate(MedicalServices medicalServices) {
        medicalServicesDao.save(medicalServices);
    }

    @Transactional
    public void delete(Integer id) {
        medicalServicesDao.delete(id);
    }

    @Transactional
    public List<ReceptionAndConsultation> getAllReception() {
        return medicalServicesDao.loadAllReception();
    }

    @Transactional
    public void saveOrUpdateReception(ReceptionAndConsultation receptionAndConsultation) {
        medicalServicesDao.saveReception(receptionAndConsultation);
    }

    @Transactional
    public void deleteReceptionService(int id) {
        medicalServicesDao.deleteReceptionService(id);
    }

    @Transactional
    public ReceptionAndConsultation findByIdReception(Integer id) {
       return medicalServicesDao.getIdReception(id);
    }
}
