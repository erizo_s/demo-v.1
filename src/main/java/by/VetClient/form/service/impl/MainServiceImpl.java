package by.VetClient.form.service.impl;


import by.VetClient.form.dao.MainDao;
import by.VetClient.form.service.MainService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class MainServiceImpl implements MainService {

    @Autowired
    private MainDao mainDao;


    @Transactional
    public List findByName(String tagName) {
        return mainDao.findById(tagName);
    }
}
