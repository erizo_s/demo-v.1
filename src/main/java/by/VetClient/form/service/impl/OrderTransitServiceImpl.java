package by.VetClient.form.service.impl;

import by.VetClient.form.dao.OrderTransitDao;
import by.VetClient.form.model.OrderTransit;
import by.VetClient.form.service.OrderTransitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class OrderTransitServiceImpl implements OrderTransitService {


    @Autowired
    private OrderTransitDao orderTransitDao;


    @Transactional
    public OrderTransit findById(Integer id) {
        return orderTransitDao.findById(id);
    }

    @Transactional
    public List<OrderTransit> findAll() {
        return orderTransitDao.findAll();
    }


    @Transactional
    public void saveOrUpdate(Integer id) {
        orderTransitDao.save(id);
    }

    @Transactional
    public void goToStorage() {
        orderTransitDao.goToStorage();
    }

    @Transactional
    public void delete(OrderTransit orderTransit) {
         orderTransitDao.delete();
    }
}
