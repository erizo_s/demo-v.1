package by.VetClient.form.service;

import by.VetClient.form.model.MedicalOrders;
import by.VetClient.form.model.MedicalRecords;
import by.VetClient.form.model.MedicalServices;
import by.VetClient.form.model.ReceptionAndConsultation;

import java.util.List;


public interface MedicalRecordsService {

    List<MedicalRecords> findAll();

    void saveOrUpdate(MedicalRecords medicalRecords);

    MedicalRecords findById(Integer userId);

    List<MedicalOrders> findByIdAllServices(Integer id);

    void save(ReceptionAndConsultation receptionAndConsultation);

    List<ReceptionAndConsultation> findByName(String tagName);

    void saveR(ReceptionAndConsultation receptionAndConsultation);
}
