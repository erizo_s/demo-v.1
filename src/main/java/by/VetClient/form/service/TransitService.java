package by.VetClient.form.service;

import by.VetClient.form.model.Transit;

import java.util.List;

public interface TransitService {

    List<Transit> findAll();

    Transit findById(Integer id);
}
