package by.VetClient.form.service;

import by.VetClient.form.model.BloodAnalysis;
import by.VetClient.form.model.BloodAnalysisLeukogram;
import by.VetClient.form.model.BloodAnalysisMorphological;

import java.util.List;

public interface BloodAnalysisService {

    List<BloodAnalysis> findAll();

    void save(BloodAnalysis bloodAnalysis);

    void saveBloodAnalysisMorphological(BloodAnalysisMorphological bloodAnalysisMorphological);

    void saveBloodAnalysisLeukogram(BloodAnalysisLeukogram bloodAnalysisLeukogram);

    BloodAnalysis findById(Integer userId);

    BloodAnalysisMorphological findByIdBloodAnalysisMorphological(Integer userId);

    BloodAnalysisLeukogram findByIdBloodAnalysisLeukogram(Integer userId);
}
