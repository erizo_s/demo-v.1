package by.VetClient.form.web;

import org.springframework.ui.Model;

import java.util.LinkedHashMap;
import java.util.Map;

public class ListsForFirstOrder {
    protected static void populateDefaultModel(Model model) {

        Map<String, String> country = new LinkedHashMap<String, String>();
        country.put("Mts", "MTS");
        country.put("Vel", "VELCOM");
        country.put("Life", "LIFE");
        model.addAttribute("countryList", country); // Выпадающий список оператор

        Map<String, String> animal = new LinkedHashMap<String, String>();
        animal.put("Кот", "Кот");
        animal.put("Собака", "Собака");
        animal.put("Птица", "Птица");
        animal.put("Земноводные", "Земноводные");
        animal.put("КРС", "КРС");
        animal.put("МРС", "МРС");
        model.addAttribute("animalList", animal);  // Выпадающий список Вид животного

        Map<String, String> passport = new LinkedHashMap<String, String>();
        passport.put("Есть", "Есть");
        passport.put("Нет", "Нет");
        model.addAttribute("passportlList", passport);

        Map<String, String> vaccination = new LinkedHashMap<String, String>();
        vaccination.put("Есть", "Есть");
        vaccination.put("Нет", "Нет");
        model.addAttribute("vaccinationList", vaccination);

        Map<String, String> status = new LinkedHashMap<String, String>();
        status.put("Обработана", "Обработана");
        status.put("Ожидание", "Ожидание");
        model.addAttribute("statusList", status);

        Map<String, String> position = new LinkedHashMap<String, String>();
        position.put("Владелец", "Владелец");
        position.put("Администратор", "Администратор");
        model.addAttribute("positionList", position);

        Map<String, String> sex = new LinkedHashMap<String, String>();
        sex.put("Мужской", "Мужской");
        sex.put("Женский", "Женский");
        model.addAttribute("sexList", sex);

        Map<String, String> fatness = new LinkedHashMap<String, String>();
        fatness.put("1-я категория", "1-я категория");
        fatness.put("2-я категория", "2-я категория");
        fatness.put("3-я категория", "3-я категория");
        fatness.put("4-я категория", "4-я категория");
        fatness.put("5-я категория", "5-я категория");
        model.addAttribute("fatnessList", fatness);
    }
}
