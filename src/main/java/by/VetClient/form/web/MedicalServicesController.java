package by.VetClient.form.web;

import by.VetClient.form.model.MedicalServices;
import by.VetClient.form.model.ReceptionAndConsultation;
import by.VetClient.form.service.MedicalServicesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(value = "/medical_services")
public class MedicalServicesController {

    @Autowired
    private MedicalServicesService medicalServicesService;

    @RequestMapping(value = "/all")
    public ModelAndView getAllMedicalServices() {
        ModelAndView mv = new ModelAndView("medical_services/listMedicalServices", "medicalServiceForm", new MedicalServices());
        mv.addObject("medical_services", medicalServicesService.getAll());
        return mv;
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public String saveOrUpdateMedicalServices(@ModelAttribute("medicalServiceForm") @Validated MedicalServices medicalServices) {
        medicalServicesService.saveOrUpdate(medicalServices);
        ModelAndView mv = new ModelAndView("medical_services/listMedicalServices");
        mv.addObject("medical_services", medicalServicesService.getAll());
        return "redirect:/medical_services/all";
    }

    @RequestMapping(value = "/{Id}/delete")
    public String deleteMedicalServices(@PathVariable("Id") Integer Id) {
        medicalServicesService.delete(Id);
        ModelAndView mv = new ModelAndView("medical_services/listMedicalServices");
        mv.addObject("medicalServiceForm");
        mv.addObject("medical_services", medicalServicesService.getAll());
        return "redirect:/medical_services/all";
    }

    @RequestMapping(value = "/1/all")
    public ModelAndView getAllConsultation() {
        ModelAndView mv = new ModelAndView("medical_services/listReceptionAndConsultation", "ReceptionAndConsultationForm", new ReceptionAndConsultation());
        mv.addObject("reception_and_consultation", medicalServicesService.getAllReception());
        return mv;
    }

    @RequestMapping(value = "1/saveReception", method = RequestMethod.POST)
    public String saveOrUpdateReceptionForm(@ModelAttribute("ReceptionAndConsultationForm") @Validated ReceptionAndConsultation receptionAndConsultation) {
        medicalServicesService.saveOrUpdateReception(receptionAndConsultation);
        ModelAndView mv = new ModelAndView("medical_services/listReceptionAndConsultation");
        mv.addObject("reception_and_consultation", medicalServicesService.getAllReception());
        return "redirect:/medical_services/1/all";
    }

    @RequestMapping(value = "1/{Id}/saveReception", method = RequestMethod.POST)
    public String saveReception(@ModelAttribute("ReceptionAndConsultationForm") @Validated ReceptionAndConsultation receptionAndConsultation) {
        medicalServicesService.saveOrUpdateReception(receptionAndConsultation);
        ModelAndView mv = new ModelAndView("medical_services/listReceptionAndConsultation");
        mv.addObject("reception_and_consultation", medicalServicesService.getAllReception());
        return "redirect:/medical_services/1/all";
    }

    @RequestMapping(value = "/{Id}/deleteReception")
    public String deleteReceptionService(@PathVariable("Id") Integer Id) {
        medicalServicesService.deleteReceptionService(Id);
        ModelAndView mv = new ModelAndView("medical_services/listReceptionAndConsultation");
        mv.addObject("ReceptionAndConsultationForm");
        mv.addObject("reception_and_consultation", medicalServicesService.getAllReception());
        return "redirect:/medical_services/1/all";
    }

    @RequestMapping(value = "/1/{Id}/edit", method = RequestMethod.GET)
    public String getIdReception(@PathVariable("Id") int Id, Model model) {
        ReceptionAndConsultation receptionAndConsultation = medicalServicesService.findByIdReception(Id);
        model.addAttribute("ReceptionAndConsultationForm", receptionAndConsultation);
        model.addAttribute("list", medicalServicesService.getAllReception());
        return "medical_services/listReceptionAndConsultation";
    }
}
