package by.VetClient.form.web;

import by.VetClient.form.model.BloodAnalysis;
import by.VetClient.form.model.UroscopyAnalysis;
import by.VetClient.form.model.UroscopyAnalysisChemical;
import by.VetClient.form.model.UroscopyAnalysisMicroscopic;
import by.VetClient.form.service.BloodAnalysisService;
import by.VetClient.form.service.UroscopyAnalysisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;


@Controller
public class UroscopyAnalysisController {

    @Autowired
    private UroscopyAnalysisService uroscopyAnalysisService;


    @RequestMapping("/medical_records/uroscopy_analysis")
    public ModelAndView toPageBloodAnalysisAdd(Model model) {
        ListsForFirstOrder.populateDefaultModel(model);
        ModelAndView mv = new ModelAndView("medical_records/uroscopy_analysis", "uroscopy_analysis", new UroscopyAnalysis());
        mv.addObject("uroscopy_analysis");

        return mv;
    }

    // saveOrUpdate  user
    @RequestMapping(value = "/UroscopyAnalysisAdd", method = RequestMethod.POST)
    public String saveMedicalRecords(@ModelAttribute("uroscopy_analysis") @Validated UroscopyAnalysis uroscopyAnalysis,
                                     BindingResult result, Model model, final RedirectAttributes redirectAttributes) {


        uroscopyAnalysisService.save(uroscopyAnalysis);

        System.out.println("работает сохранение");
        // POST/REDIRECT/GET
        return "redirect:/uroscopy_analysis/" + uroscopyAnalysis.getId();

    }

    @RequestMapping("/medical_records/uroscopy_analysis_chemical")
    public ModelAndView toPageBloodAnalysisChemicalAdd(Model model) {
        ListsForFirstOrder.populateDefaultModel(model);
        ModelAndView mv = new ModelAndView("medical_records/uroscopy_analysis_chemical", "uroscopy_analysis_chemical", new UroscopyAnalysisChemical());
        mv.addObject("uroscopy_analysis_chemical");

        return mv;
    }

    // saveOrUpdate  user
    @RequestMapping(value = "/UroscopyAnalysisChemicalAdd", method = RequestMethod.POST)
    public String saveUroscopyAnalysisChemical(@ModelAttribute("uroscopy_analysis_chemical") @Validated UroscopyAnalysisChemical uroscopyAnalysisChemical,
                                     BindingResult result, Model model, final RedirectAttributes redirectAttributes) {


        uroscopyAnalysisService.saveChemicalAnalysis(uroscopyAnalysisChemical);

        System.out.println("работает сохранение");
        // POST/REDIRECT/GET
        return "redirect:/uroscopy_analysis/" + uroscopyAnalysisChemical.getId();

    }

    @RequestMapping("/medical_records/uroscopy_analysis_microscopic")
    public ModelAndView toPageUroscopyAnalysisMicroscopicAdd(Model model) {
        ListsForFirstOrder.populateDefaultModel(model);
        ModelAndView mv = new ModelAndView("medical_records/uroscopy_analysis_microscopic", "uroscopy_analysis_microscopic", new UroscopyAnalysisMicroscopic());
        mv.addObject("uroscopy_analysis_microscopic");

        return mv;
    }

    // saveOrUpdate  user
    @RequestMapping(value = "/UroscopyAnalysisMicroscopicAdd", method = RequestMethod.POST)
    public String saveUroscopyAnalysisMicroscopic(@ModelAttribute("uroscopy_analysis_microscopic") @Validated UroscopyAnalysisMicroscopic uroscopyAnalysisMicroscopic,
                                     BindingResult result, Model model, final RedirectAttributes redirectAttributes) {


        uroscopyAnalysisService.saveUroscopyAnalysisMicroscopic(uroscopyAnalysisMicroscopic);

        System.out.println("работает сохранение");
        // POST/REDIRECT/GET
        return "redirect:/uroscopy_analysis/" + uroscopyAnalysisMicroscopic.getId();

    }

    // show add user form
    @RequestMapping(value = "/uroscopy_analysis/{userId}", method = RequestMethod.GET)
    public ModelAndView showAddUserForm(@PathVariable("userId") Integer userId) {
        ModelAndView mv = new ModelAndView("medical_records/uroscopy_analysis_show");
        mv.addObject("uroscopy_analysis",  uroscopyAnalysisService.findById(userId));
        mv.addObject("uroscopy_analysis_chemical",  uroscopyAnalysisService.findByIdUroscopyAnalysisChemical(userId));
        mv.addObject("uroscopy_analysis_microscopic",  uroscopyAnalysisService.findByIdUroscopyAnalysisMicroscopic(userId));
        System.out.println("работает просмотр");
        return mv;

    }














}
