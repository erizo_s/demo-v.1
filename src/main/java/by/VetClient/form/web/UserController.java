package by.VetClient.form.web;

import by.VetClient.form.converters.UserToUserViewModelConverter;
import by.VetClient.form.converters.UserViewModelToUserConverter;
import by.VetClient.form.model.User;
import by.VetClient.form.model.UserViewModel;
import by.VetClient.form.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.sql.Time;
import java.time.LocalDate;
import java.time.LocalTime;

@Controller
@SessionAttributes(value = "userForm")
public class UserController {


    @Autowired
    private UserService userService;

    @RequestMapping(value = "/users", method = RequestMethod.GET)
    public ModelAndView listContacts() {
        ModelAndView mv = new ModelAndView("users/list");
        LocalDate dateMin = LocalDate.now();
        LocalDate dateMax = dateMin.plusDays(1);
        mv.addObject("users", userService.findAllToday(dateMin, dateMax));
        return mv;
    }

    // show add user form
    @RequestMapping(value = "/users/{userId}", method = RequestMethod.GET)
    public ModelAndView showAddUserForm(@PathVariable("userId") Integer userId) {
        ModelAndView mv = new ModelAndView("users/show");
        mv.addObject("users", userService.findById(userId));
        return mv;
    }

    @RequestMapping("/users/add")
    public ModelAndView toPageAdd(Model model) {
        ModelAndView mv = new ModelAndView("users/userForm", "userForm", new UserViewModel());
        mv.addObject("userForm");
        mv.addObject("listDoctors", userService.getAllDoctors());
        mv.addObject("listDepartment", userService.getAllDepartments());
        return mv;
    }

    @RequestMapping(value = "/users/{userId}/delete")
    public String deleteContact(@PathVariable("userId") Integer userId) {
        userService.delete(userId);
        ModelAndView mv = new ModelAndView("users/list");
        LocalDate dateMin = LocalDate.now();
        LocalDate dateMax = dateMin.plusDays(1);
        mv.addObject("users", userService.findAllToday(dateMin, dateMax));
        return "redirect:/users";
    }

    // show update form
    @RequestMapping(value = "/users/{userId}/update", method = RequestMethod.GET)
    public String showUpdateUserForm(@PathVariable("userId") Integer userId, Model model, @ModelAttribute(value = "mode") String mode) {
        ListsForFirstOrder.populateDefaultModel(model);
        UserToUserViewModelConverter userToUserViewModelConverter = new UserToUserViewModelConverter();
        User user = userService.findById(userId);
        UserViewModel userViewModel = userToUserViewModelConverter.convert(user);
        model.addAttribute("mode", mode);
        model.addAttribute("userForm", userViewModel);
        model.addAttribute("listDoctors", userService.getAllDoctors());
        model.addAttribute("listDepartment", userService.getAllDepartments());
        return "users/userForm";
    }

    // saveOrUpdate  user
    @RequestMapping(value = "/usersAdd", method = RequestMethod.POST)
    public String saveOrUpdateUser(@ModelAttribute("userForm") @Validated UserViewModel userViewModel) {
        UserViewModelToUserConverter userViewModelToUserConverter = new UserViewModelToUserConverter();
        User user = userViewModelToUserConverter.convert(userViewModel);
        userService.saveOrUpdate(user);
        return "redirect:/users/" + user.getId();
    }

    @RequestMapping(value = "/search", method = RequestMethod.GET)
    public ModelAndView searchUserByName(@RequestParam(name = "searchText") String searchText) {
        ModelAndView mv = new ModelAndView("users/list");
        mv.addObject("users", userService.getUsersByName(searchText));
        return mv;
    }

    @RequestMapping(value = "/searchForDates", method = RequestMethod.POST)
    public ModelAndView searchForDates(@RequestParam(name = "minDate") @DateTimeFormat(pattern = "dd.MM.yyyy") LocalDate minDate, @RequestParam(name = "maxDate") @DateTimeFormat(pattern = "dd.MM.yyyy") LocalDate maxDate) {
        ModelAndView mv = new ModelAndView("users/list");
        LocalDate maxD = maxDate.plusDays(1);
        mv.addObject("users", userService.getUsersByDates(minDate, maxD));
        return mv;
    }
}




