package by.VetClient.form.web;

import by.VetClient.form.model.BloodAnalysis;
import by.VetClient.form.model.BloodAnalysisLeukogram;
import by.VetClient.form.model.BloodAnalysisMorphological;
import by.VetClient.form.service.BloodAnalysisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;


@Controller
public class BloodAnalysisController {

    @Autowired
    private BloodAnalysisService bloodAnalysisService;


    @RequestMapping("/medical_records/blood_analysis")
    public ModelAndView toPageBloodAnalysisAdd(Model model) {
        ListsForFirstOrder.populateDefaultModel(model);
        ModelAndView mv = new ModelAndView("medical_records/blood_analysis", "blood_analysis", new BloodAnalysis());
        mv.addObject("blood_analysis");

        return mv;
    }

    // show add user form
    @RequestMapping(value = "/blood_analysis/{userId}", method = RequestMethod.GET)
    public ModelAndView showAddUserForm(@PathVariable("userId") Integer userId) {
        ModelAndView mv = new ModelAndView("medical_records/show");
        mv.addObject("blood_analysis", bloodAnalysisService.findById(userId));
        mv.addObject("blood_analysis_morphological", bloodAnalysisService.findByIdBloodAnalysisMorphological(userId));
        mv.addObject("blood_analysis_leukogram", bloodAnalysisService.findByIdBloodAnalysisLeukogram(userId));
        System.out.println("работает просмотр");
        return mv;

    }

    // saveOrUpdate  user
    @RequestMapping(value = "/BloodAnalysisAdd", method = RequestMethod.POST)
    public String saveMedicalRecords(@ModelAttribute("blood_analysis") @Validated BloodAnalysis bloodAnalysis,
                                             BindingResult result, Model model, final RedirectAttributes redirectAttributes) {


        bloodAnalysisService.save(bloodAnalysis);

        System.out.println("работает сохранение");
        // POST/REDIRECT/GET
        return "redirect:/blood_analysis/" + bloodAnalysis.getId();

    }

    @RequestMapping("/medical_records/blood_analysis_morphological")
    public ModelAndView toPageBloodAnalysis2Add(Model model) {
        ListsForFirstOrder.populateDefaultModel(model);
        ModelAndView mv = new ModelAndView("medical_records/blood_analysis_morphological", "blood_analysis_morphological", new BloodAnalysisMorphological());
        mv.addObject("blood_analysis_morphological");

        return mv;
    }

    // saveOrUpdate  user
    @RequestMapping(value = "/BloodAnalysisAddMorphological", method = RequestMethod.POST)
    public String saveMedicalRecordsMorphological(@ModelAttribute("blood_analysis_morphological") @Validated BloodAnalysisMorphological bloodAnalysisMorphological,
                                     BindingResult result, Model model, final RedirectAttributes redirectAttributes) {


        bloodAnalysisService.saveBloodAnalysisMorphological(bloodAnalysisMorphological);

        System.out.println("работает сохранение");
        // POST/REDIRECT/GET
        return "redirect:/blood_analysis/" + bloodAnalysisMorphological.getId();

    }

    @RequestMapping("/medical_records/blood_analysis_leukogram")
    public ModelAndView toPageBloodAnalysisLeucogramAdd(Model model) {
        ListsForFirstOrder.populateDefaultModel(model);
        ModelAndView mv = new ModelAndView("medical_records/blood_analysis_leukogram", "blood_analysis_leukogram", new BloodAnalysisLeukogram());
        mv.addObject("blood_analysis_leukogram");

        return mv;
    }

    // saveOrUpdate  user
    @RequestMapping(value = "/BloodAnalysisAddLeukogram", method = RequestMethod.POST)
    public String saveMedicalRecordsLeucogram(@ModelAttribute("blood_analysis_leukogram") @Validated BloodAnalysisLeukogram bloodAnalysisLeukogram,
                                                  BindingResult result, Model model, final RedirectAttributes redirectAttributes) {


        bloodAnalysisService.saveBloodAnalysisLeukogram(bloodAnalysisLeukogram);

        System.out.println("работает сохранение");
        // POST/REDIRECT/GET
        return "redirect:/blood_analysis/" + bloodAnalysisLeukogram.getId();

    }
}
