package by.VetClient.form.web;

import by.VetClient.form.model.MedicalOrders;
import by.VetClient.form.model.MedicalRecords;
import by.VetClient.form.model.ReceptionAndConsultation;
import by.VetClient.form.model.Transit;
import by.VetClient.form.service.MainService;
import by.VetClient.form.service.MedicalRecordsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.List;


@Controller
public class MedicalRecordsController {

    @Autowired
    private MedicalRecordsService medicalRecordsService;

    @Autowired
    private MainService mainService;

    @RequestMapping("/medical_records")
    public ModelAndView listMedicalRecords() {
        ModelAndView mv = new ModelAndView("medical_records/list");
        mv.addObject("medical_records", medicalRecordsService.findAll());
        return mv;
    }

    @RequestMapping("/medical_records/add")
    public ModelAndView toPageMedicalRecordsAdd(Model model) {
        ListsForFirstOrder.populateDefaultModel(model);
        ModelAndView mv = new ModelAndView("medical_records/formMedicalRecords", "formMedicalRecords", new MedicalRecords());
        mv.addObject("formMedicalRecords");
        return mv;
    }


    @RequestMapping(value = "/medical_records/{userId}", method = RequestMethod.GET)
    public ModelAndView showAddUserForm(@PathVariable("userId") Integer userId) {
        ModelAndView mv = new ModelAndView("medical_records/records_show");
        mv.addObject("medical_records", medicalRecordsService.findById(userId));
        return mv;
    }

    @RequestMapping(value = "/MedicalRecordsAdd", method = RequestMethod.POST)
    public String saveOrUpdateMedicalRecords(@ModelAttribute("formMedicalRecords") MedicalRecords medicalRecords) {

        medicalRecordsService.saveOrUpdate(medicalRecords);
        ModelAndView mv = new ModelAndView("medical_records/list");
        mv.addObject("medicalRecordsService", medicalRecordsService.findAll());
        return "redirect:/medical_records/";
    }


    @RequestMapping(value = "/services/{Id}", method = RequestMethod.GET)
    public ModelAndView showAllMedicalServicesInOrder(@PathVariable("Id") Integer Id) {
        ModelAndView mv = new ModelAndView("medical_records/listServices");
        mv.addObject("listServices", medicalRecordsService.findByIdAllServices(Id));
        return mv;
    }

    @RequestMapping(value = "services/saveServices", method = RequestMethod.POST)
    public String saveOrUpdateReceptionForm( @ModelAttribute("servicesForm") ReceptionAndConsultation receptionAndConsultation, Integer id) {
        id = 1;
        receptionAndConsultation.s
        medicalRecordsService.save(receptionAndConsultation);
        ModelAndView mv = new ModelAndView("medical_records/listServices", "servicesForm", new ReceptionAndConsultation());
        mv.addObject("listServices", medicalRecordsService.findByIdAllServices(id));
        return "redirect:/services/1";
    }

    @ResponseBody
    @RequestMapping(value = "/services/1/getTags/{name}", method = RequestMethod.GET)
    public List<Transit> getTags(@PathVariable(value = "name") String name) {
        return simulateSearchResult(name);
    }

    private List simulateSearchResult(String tagName) {
        return mainService.findByName(tagName);
    }
}
