package by.VetClient.form.web;

import by.VetClient.form.model.OrderTransit;
import by.VetClient.form.model.Storage;
import by.VetClient.form.service.OrderTransitService;
import by.VetClient.form.service.StorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;

@Controller
public class StorageController {

    @Autowired
    private StorageService storageService;

    @Autowired
    private OrderTransitService orderTransitService;

    @RequestMapping("/storage")
    public ModelAndView listStorage(OrderTransit orderTransit) {
        orderTransitService.delete(orderTransit);
        ModelAndView mv = new ModelAndView("storage/list");
        mv.addObject("storage", storageService.findAll());
        return mv;
    }

    @RequestMapping(value = "/addPos", method = RequestMethod.POST)
    public String saveOrUpdateUser(@ModelAttribute("new_position_storage") @Validated Storage storage,
                                   BindingResult result, Model model, final RedirectAttributes redirectAttributes) {
        storageService.saveOrUpdate(storage);
        return "redirect:/storage/";
    }

    @RequestMapping("/storage/add")
    public ModelAndView toPageAdd(Model model) {
        System.out.println("kdfhk");
        ModelAndView mv = new ModelAndView("storage/new_position_storage", "new_position_storage", new Storage());
        mv.addObject("new_position_storage");
        return mv;
    }

    @RequestMapping(value = "/goToStorage")
    public ModelAndView showUpdateUserForm(Model model, OrderTransit orderTransit) {
        orderTransitService.goToStorage();
        ModelAndView mv = new ModelAndView("storage/list", "list", new Storage());
        mv.addObject("storage", storageService.findAll());
        orderTransitService.delete(orderTransit);
        return mv;
    }
}
