package by.VetClient.form.web;

import by.VetClient.form.model.FaecalFlotation;
import by.VetClient.form.model.FaecalFlotationChemical;
import by.VetClient.form.model.FaecalFlotationMicroscopic;
import by.VetClient.form.model.UroscopyAnalysis;
import by.VetClient.form.service.FaecalFlotationService;
import by.VetClient.form.service.MedicalRecordsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;




@Controller
public class FaecalFlotationController {


    @Autowired
    private FaecalFlotationService faecalFlotationService;

    @RequestMapping("/medical_records/faecal_flotation")
    public ModelAndView toPageFaecalFlotationAdd(Model model) {
        ListsForFirstOrder.populateDefaultModel(model);
        ModelAndView mv = new ModelAndView("medical_records/faecal_flotation", "faecal_flotation", new FaecalFlotation());
        mv.addObject("faecal_flotation");

        return mv;
    }


    // saveOrUpdate  user
    @RequestMapping(value = "/FaecalFlotationAdd", method = RequestMethod.POST)
    public String saveFaecalFlotation(@ModelAttribute("faecal_flotation") @Validated FaecalFlotation faecalFlotation,
                                     BindingResult result, Model model, final RedirectAttributes redirectAttributes) {



        faecalFlotationService.save(faecalFlotation);

        System.out.println("работает сохранение");
        // POST/REDIRECT/GET
        return "redirect:/faecal_flotation/" + faecalFlotation.getId();

    }

    @RequestMapping("/medical_records/faecal_flotation_chemical")
    public ModelAndView toPageFaecalFlotationChemicalAdd(Model model) {
        ListsForFirstOrder.populateDefaultModel(model);
        ModelAndView mv = new ModelAndView("medical_records/faecal_flotation_chemical", "faecal_flotation_chemical", new FaecalFlotationChemical());
        mv.addObject("faecal_flotation_chemical");

        return mv;
    }


    // saveOrUpdate  user
    @RequestMapping(value = "/FaecalFlotationChemicalAdd", method = RequestMethod.POST)
    public String saveFaecalFlotationChemical(@ModelAttribute("faecal_flotation_chemical") @Validated FaecalFlotationChemical faecalFlotationChemical,
                                     BindingResult result, Model model, final RedirectAttributes redirectAttributes) {



        faecalFlotationService.saveFaecalFlotationChemical(faecalFlotationChemical);

        System.out.println("работает сохранение");
        // POST/REDIRECT/GET
        return "redirect:/faecal_flotation/" + faecalFlotationChemical.getId();

    }

    @RequestMapping("/medical_records/faecal_flotation_microscopic")
    public ModelAndView toPageFaecalFlotationMicroscopicAdd(Model model) {
        ListsForFirstOrder.populateDefaultModel(model);
        ModelAndView mv = new ModelAndView("medical_records/faecal_flotation_microscopic", "faecal_flotation_microscopic", new FaecalFlotationMicroscopic());
        mv.addObject("faecal_flotation_microscopic");

        return mv;
    }


    // saveOrUpdate  user
    @RequestMapping(value = "/FaecalFlotationMicroscopicAdd", method = RequestMethod.POST)
    public String saveFaecalFlotationMicroscopic(@ModelAttribute("faecal_flotation_microscopic") @Validated FaecalFlotationMicroscopic faecalFlotationMicroscopic,
                                     BindingResult result, Model model, final RedirectAttributes redirectAttributes) {



        faecalFlotationService.saveFaecalFlotationMicroscopic(faecalFlotationMicroscopic);

        System.out.println("работает сохранение");
        // POST/REDIRECT/GET
        return "redirect:/faecal_flotation/" + faecalFlotationMicroscopic.getId();

    }

    // show add user form
    @RequestMapping(value = "/faecal_flotation/{userId}", method = RequestMethod.GET)
    public ModelAndView showAddUserForm(@PathVariable("userId") Integer userId) {
        ModelAndView mv = new ModelAndView("medical_records/faecal_flotation_show");
        mv.addObject("faecal_flotation", faecalFlotationService.findById(userId));
        mv.addObject("faecal_flotation_chemical", faecalFlotationService.findByIdFaecalFlotationChemical(userId));
        mv.addObject("faecal_flotation_microscopic", faecalFlotationService.findByIdFaecalFlotationMicroscopic(userId));
        System.out.println("работает просмотр");
        return mv;

    }
}
