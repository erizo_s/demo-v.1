package by.VetClient.form.web;

import by.VetClient.form.model.Invoice;
import by.VetClient.form.service.InvoiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class InvoiceController {

    @Autowired
    private InvoiceService invoiceService;


    @RequestMapping("/invoice")
    public ModelAndView listInvoice() {
        ModelAndView mv = new ModelAndView("invoice/list");
        mv.addObject("invoice", invoiceService.findAll());
        return mv;
    }

    @RequestMapping(value = "/addInvoice", method = RequestMethod.POST)
    public String saveOrUpdateInvoice(@ModelAttribute("new_invoice") @Validated Invoice invoice, Model model, final RedirectAttributes redirectAttributes) {

        invoiceService.saveOrUpdate(invoice);

        // POST/REDIRECT/GET
        return "redirect:/invoice/";


    }

    @RequestMapping("/invoice/add")
    public ModelAndView toPageAdd(Model model) {
        System.out.println("kdfhk");
        ListsForFirstOrder.populateDefaultModel(model);
        ModelAndView mv = new ModelAndView("invoice/new_invoice", "new_invoice", new Invoice());
        mv.addObject("new_invoice");

        return mv;
    }
}
