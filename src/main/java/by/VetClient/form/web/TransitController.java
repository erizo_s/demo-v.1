package by.VetClient.form.web;

import by.VetClient.form.parser.EXCELTODB;
import by.VetClient.form.service.TransitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.io.IOException;
import java.sql.SQLException;

@Controller
public class TransitController {

    @Autowired
    private TransitService transitService;

    @RequestMapping("/transit")
    public ModelAndView listTransit(Model model) {
        ListsForFirstOrder.populateDefaultModel(model);
        ModelAndView mv = new ModelAndView("transit/list");
        mv.addObject("transit", transitService.findAll());
        return mv;
    }

    @RequestMapping("/upload")
    public ModelAndView uploadPrice() {
        ModelAndView mv = new ModelAndView("transit/upload_price");
        return mv;
    }

    @RequestMapping("/upload_price")
    public String uploadFilePrice() {
        EXCELTODB.main();
        return "redirect:transit";
    }

    @RequestMapping("/update_price")
    public String updatePrice() throws SQLException, IOException, ClassNotFoundException {
        EXCELTODB.updatePrice();
        return "redirect:transit";
    }
}
