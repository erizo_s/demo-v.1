package by.VetClient.form.web;


import by.VetClient.form.model.*;
import by.VetClient.form.service.OrderTransitService;
import by.VetClient.form.service.StorageService;
import by.VetClient.form.service.TransitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.ArrayList;
import java.util.List;


@Controller
public class OrderTransitController {


    @Autowired
    private OrderTransitService orderTransitService;

    @Autowired
    private TransitService transitService;

    @RequestMapping("/order_transit")
    public ModelAndView toPageAdd(Model model) {
        System.out.println("kdfhk");
        ModelAndView mv = new ModelAndView("order_transit/list", "list", new OrderTransit());
        mv.addObject("order_transit", orderTransitService.findAll());

        return mv;
    }

    @RequestMapping(value = "/delivered_transit", method = RequestMethod.POST)
    public String saveOrUpdateOrder(@ModelAttribute("list")@Validated OrderTransit orderTransit, Integer id) {

        id = orderTransit.getSku();
        orderTransitService.saveOrUpdate(id);
        orderTransitService.findAll();
        return "redirect:/order_transit/";

    }




}
