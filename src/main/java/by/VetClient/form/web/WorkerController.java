package by.VetClient.form.web;

import by.VetClient.form.model.Worker;
import by.VetClient.form.service.WorkerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class WorkerController {


    private WorkerService workerService;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    public void setWorkerService(WorkerService workerService){
        this.workerService = workerService;
    }


    @RequestMapping("/workers")
    public ModelAndView listWorkers() {
        ModelAndView mv = new ModelAndView("workers/list");
        mv.addObject("workers", workerService.findAll());
        return mv;
    }
//    // show add user form
//    @RequestMapping(value = "/workers/{workerId}", method = RequestMethod.GET)
//    public ModelAndView showAddUserForm(@PathVariable("workerId") Integer workerId) {
//        ModelAndView mv = new ModelAndView("workers/show");
//        mv.addObject("workers", workerService.findById(workerId));
//        System.out.println("kdfhk");
//        return mv;
//
//    }


    @RequestMapping("/workers/add")
    public ModelAndView toPageAdd(Model model) {
        ListsForFirstOrder.populateDefaultModel(model);
        ModelAndView mv = new ModelAndView("workers/workForm", "workForm", new Worker());
        mv.addObject("workForm");

        return mv;
    }

    @RequestMapping("/workers/{workerId}/delete")
    public String deleteContact(@PathVariable("workerId") Integer workerId) {
        workerService.delete(workerId);
        ModelAndView mv = new ModelAndView("workers/list");
        mv.addObject("workers", workerService.findAll());
        return "redirect:/workers";
    }

    // saveOrUpdate  user
    @RequestMapping(value = "/workersAdd", method = RequestMethod.POST)
    public String saveOrUpdateWorker(@ModelAttribute("workForm") @Validated Worker worker,
                                   BindingResult result, Model model, final RedirectAttributes redirectAttributes) {

        worker.setPassword(bCryptPasswordEncoder.encode(worker.getPassword()));
        workerService.saveOrUpdate(worker);
        ModelAndView mv = new ModelAndView("workers/list");

        mv.addObject("workers", workerService.findAll());
        // POST/REDIRECT/GET
        return "redirect:/workers/";

    }
}
