<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ page session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html>
<html lang="en">

<jsp:include page="../fragments/header.jsp" />
<spring:url value="/medical_records/faecal_flotation" var="FaecalFlotationAdd" />
<spring:url value="/medical_records/faecal_flotation_chemical" var="FaecalFlotationChemicalAdd" />
<spring:url value="/medical_records/faecal_flotation_microscopic" var="FaecalFlotationMicroscopicAdd" />
<body>
<div class="container">

    <div style="text-align: center">
        <table style="table-layout: fixed; width: 700px; align-self: stretch" class="table table-bordered">
            <thead>
            <tr>
                <td align="center" colspan="3">Физические свойства фекалий</td>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>Дата</td>
                <td align="center" colspan="2">${faecal_flotation.dateAnalysis}</td>
            </tr>
            <tr>
                <td>Время</td>
                <td align="center" colspan="2">${faecal_flotation.timeAnalysis}</td>
            </tr>
            <tr>
                <td>Владелец</td>
                <td align="center" colspan="2">${faecal_flotation.owner}</td>
            </tr>
            <tr>
                <td>Животное</td>
                <td align="center" colspan="2">${faecal_flotation.kind}</td>
            </tr>
            <tr>
                <td></td>
                <td align="center">Норма</td>
                <td align="center">Показатель</td>
            </tr>
            <tr>
                <td>Количество</td>
                <td align="center">Норма</td>
                <td align="center">${faecal_flotation.amount}</td>
            </tr>
            <tr>
                <td>Цвет</td>
                <td align="center">Норма</td>
                <td align="center">${faecal_flotation.color}</td>
            </tr>
            <tr>
                <td>Запах</td>
                <td align="center">Норма</td>
                <td align="center">${faecal_flotation.smell}</td>
            </tr>
            <tr>
                <td>Форма</td>
                <td align="center">Норма</td>
                <td align="center">${faecal_flotation.form}</td>
            </tr>
            <tr>
                <td>Консистенция</td>
                <td align="center">Норма</td>
                <td align="center">${faecal_flotation.consistency}</td>
            </tr>
            <tr>
                <td>Переваримость</td>
                <td align="center">Норма</td>
                <td align="center">${faecal_flotation.digestibility}</td>
            </tr>
            <tr>
                <td>Слизь</td>
                <td align="center">Норма</td>
                <td align="center">${faecal_flotation.slime}</td>
            </tr>
            <tr>
                <td>Лаборант</td>
                <td align="center">Норма</td>
                <td align="center">${faecal_flotation.assistant}</td>
            </tr>
            <tr>
                <td>Заключение</td>
                <td align="center">Норма</td>
                <td align="center">${faecal_flotation.conclusion}</td>
            </tr>
            </tbody>
        </table>
        <button class="btn btn-danger" onclick="location.href='${FaecalFlotationAdd}'">Провести анализ</button><br><br><br>
        <table style="table-layout: fixed; width: 700px; align-self: stretch" class="table table-bordered">
            <thead>
            <tr>
                <td align="center" colspan="3">Химический анализ фекалий</td>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>Дата</td>
                <td align="center" colspan="2">${faecal_flotation_chemical.chemistryDateAnalysis}</td>
            </tr>
            <tr>
                <td>Время</td>
                <td align="center" colspan="2">${faecal_flotation_chemical.chemistryTimeAnalysis}</td>
            </tr>
            <tr>
                <td>Владелец</td>
                <td align="center" colspan="2">${faecal_flotation_chemical.chemicalOwner}</td>
            </tr>
            <tr>
                <td>Животное</td>
                <td align="center" colspan="2">${faecal_flotation_chemical.chemicalKind}</td>
            </tr>
            <tr>
                <td></td>
                <td align="center">Норма</td>
                <td align="center">Показатель</td>
            </tr>
            <tr>
                <td>Реакция</td>
                <td align="center">Норма</td>
                <td align="center">${faecal_flotation_chemical.reaction}</td>
            </tr>
            <tr>
                <td>Общая кислотность</td>
                <td align="center">Норма</td>
                <td align="center">${faecal_flotation_chemical.totalAcidity}</td>
            </tr>
            <tr>
                <td>Белок</td>
                <td align="center">Норма</td>
                <td align="center" >${faecal_flotation_chemical.protein}</td>
            </tr>
            <tr>
                <td>Пигменты крови</td>
                <td align="center">Норма</td>
                <td align="center" >${faecal_flotation_chemical.pigmentsBlood}</td>
            </tr>
            <tr>
                <td>Желчные пигменты</td>
                <td align="center">Норма</td>
                <td align="center" >${faecal_flotation_chemical.cholochrome}</td>
            </tr>
            <tr>
                <td>Аммиак</td>
                <td align="center">Норма</td>
                <td align="center" >${faecal_flotation_chemical.ammonia}</td>
            </tr>
            <tr>
                <td>Бродильная проба</td>
                <td align="center">Норма</td>
                <td align="center" >${faecal_flotation_chemical.fermentationTest}</td>
            </tr>
            <tr>
                <td>Лаборант</td>
                <td align="center">Норма</td>
                <td align="center" >${faecal_flotation_chemical.chemistryAssistant}</td>
            </tr>
            <tr>
                <td>Заключение</td>
                <td align="center">Норма</td>
                <td align="center" >${faecal_flotation_chemical.chemistryConclusion}</td>
            </tr>
            </tbody>
        </table>
        <button class="btn btn-danger" onclick="location.href='${FaecalFlotationChemicalAdd}'">Провести анализ</button><br><br><br>
    <table style="table-layout: fixed; width: 700px; align-self: stretch" class="table table-bordered">
        <thead>
        <tr>
            <td align="center" colspan="3">Микроскопическое исследование фекалий</td>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>Дата</td>
            <td align="center" colspan="2">${faecal_flotation_microscopic.microscopicDateAnalysis}</td>
        </tr>
        <tr>
            <td>Время</td>
            <td align="center" colspan="2">${faecal_flotation_microscopic.microscopicTimeAnalysis}</td>
        </tr>
        <tr>
            <td>Владелец</td>
            <td align="center" colspan="2">${faecal_flotation_microscopic.microscopicOwner}</td>
        </tr>
        <tr>
            <td>Животное</td>
            <td align="center" colspan="2">${faecal_flotation_microscopic.microscopicKind}</td>
        </tr>
        <tr>
            <td></td>
            <td align="center">Норма</td>
            <td align="center">Показатель</td>
        </tr>
        <tr>
            <td>Кровь</td>
            <td align="center">Норма</td>
            <td align="center">${faecal_flotation_microscopic.blood}</td>
        </tr>
        <tr>
            <td>Слизь</td>
            <td align="center">Норма</td>
            <td align="center">${faecal_flotation_microscopic.microscopicSlime}</td>
        </tr>
        <tr>
            <td>Метод Бермана-Орлова</td>
            <td align="center">Норма</td>
            <td align="center">${faecal_flotation_microscopic.diseasesByDarling}</td>
        </tr>
        <tr>
            <td>Метод нативного мазка</td>
            <td align="center">Норма</td>
            <td align="center">${faecal_flotation_microscopic.diseasesByBermanOrlov}</td>
        </tr>
        <tr>
            <td>Метод последовательных промываний</td>
            <td align="center">Норма</td>
            <td align="center">${faecal_flotation_microscopic.diseasesBySuccessiveWashings}</td>
        </tr>
        <tr>
            <td>Лаборант</td>
            <td align="center">Норма</td>
            <td align="center">${faecal_flotation_microscopic.microscopicAssistant}</td>
        </tr>
        <tr>
            <td>Заключение</td>
            <td align="center">Норма</td>
            <td align="center">${faecal_flotation_microscopic.microscopicConclusion}</td>
        </tr>
        </tbody>
    </table>
        <button class="btn btn-danger" onclick="location.href='${FaecalFlotationMicroscopicAdd}'">Провести анализ</button><br><br>


</div>


<jsp:include page="../fragments/footer.jsp" />

</body>
</html>