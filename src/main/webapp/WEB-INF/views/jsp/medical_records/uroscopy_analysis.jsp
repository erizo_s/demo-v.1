<%@ page language="java" contentType="text/html;  charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ page session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html>
<html lang="en">
<jsp:include page="../fragments/header.jsp" />
<body>

<div class="container" >
        <form:form method="POST" modelAttribute="uroscopy_analysis" class="form-horizontal" action="/UroscopyAnalysisAdd">
            <form:input type="hidden" path="id" id="id" />


                <spring:bind path="dateAnalysis">
                    <div class="form-group ${status.error ? 'has-error' : ''}">
                        <label class="col-sm-2 control-label">Дата исследования</label>
                        <div class="col-sm-3">
                            <div class="input-group date" id="datetimepicker4">
                                <form:input path="dateAnalysis" type="text" class="form-control" placeholder="Дата исследования" />
                                        <span class="input-group-addon">
                                         </span>
                            </div>
                            <script type="text/javascript">
                                $(function () {
                                    $('#datetimepicker4').datetimepicker(
                                            {pickTime: false, language: 'ru'}
                                    );
                                });
                            </script>
                        </div>
                    </div>
                </spring:bind>

                <spring:bind path="timeAnalysis">
                    <div class="form-group ${status.error ? 'has-error' : ''}">
                        <label class="col-sm-2 control-label">Время исследования</label>
                        <div class="col-sm-3">
                            <div class="input-group date" id="datetimepicker5">
                                <form:input path="timeAnalysis" type="text" class="form-control" placeholder="Время исследования" />
                                        <span class="input-group-addon">
                                         </span>
                            </div>
                            <script type="text/javascript">
                                $(function () {
                                    $('#datetimepicker5').datetimepicker(
                                            {pickDate: false, language: 'ru'}
                                    );
                                });
                            </script>
                        </div>
                    </div>
                </spring:bind>

                <spring:bind path="owner">
                    <div class="form-group ${status.error ? 'has-error' : ''}">
                        <label class="col-sm-2 control-label">Владелец</label>
                        <div class="col-sm-3">
                            <form:input path="owner" class="form-control" id="owner" placeholder="Ф.И.О." />
                            <form:errors path="owner" class="control-label" />
                        </div>
                    </div>
                </spring:bind>

                <spring:bind path="kind">
                    <div class="form-group ${status.error ? 'has-error' : ''}">
                        <label class="col-sm-2 control-label">Животное</label>
                        <div class="col-sm-3">
                            <form:input path="kind" class="form-control" id="kind" placeholder="Животное" />
                            <form:errors path="kind" class="control-label" />
                        </div>
                    </div>
                </spring:bind>

                <spring:bind path="number">
                    <div class="form-group ${status.error ? 'has-error' : ''}">
                        <label class="col-sm-2 control-label">Количество</label>
                        <div class="col-sm-3">
                            <form:input path="number" class="form-control" id="number" placeholder="Количество" />
                            <form:errors path="number" class="control-label" />
                        </div>
                    </div>
                </spring:bind>

                <spring:bind path="color">
                    <div class="form-group ${status.error ? 'has-error' : ''}">
                        <label class="col-sm-2 control-label">Цвет</label>
                        <div class="col-sm-3">
                            <form:input path="color" class="form-control" id="color" placeholder="Цвет" />
                            <form:errors path="color" class="control-label" />
                        </div>
                    </div>
                </spring:bind>

                <spring:bind path="transparency">
                    <div class="form-group ${status.error ? 'has-error' : ''}">
                        <label class="col-sm-2 control-label">Прозрачность</label>
                        <div class="col-sm-3">
                            <form:input path="transparency" class="form-control" id="transparency" placeholder="Прозрачность" />
                            <form:errors path="transparency" class="control-label" />
                        </div>
                    </div>
                </spring:bind>

                <spring:bind path="consistency">
                    <div class="form-group ${status.error ? 'has-error' : ''}">
                        <label class="col-sm-2 control-label">Консистенция</label>
                        <div class="col-sm-3">
                            <form:input path="consistency" class="form-control" id="consistency" placeholder="Консистенция" />
                            <form:errors path="consistency" class="control-label" />
                        </div>
                    </div>
                </spring:bind>

                <spring:bind path="smell">
                    <div class="form-group ${status.error ? 'has-error' : ''}">
                        <label class="col-sm-2 control-label">Запах</label>
                        <div class="col-sm-3">
                            <form:input path="smell" class="form-control" id="smell" placeholder="Запах" />
                            <form:errors path="smell" class="control-label" />
                        </div>
                    </div>
                </spring:bind>

                <spring:bind path="density">
                    <div class="form-group ${status.error ? 'has-error' : ''}">
                        <label class="col-sm-2 control-label">Плотность</label>
                        <div class="col-sm-3">
                            <form:input path="density" class="form-control" id="density" placeholder="Плотность" />
                            <form:errors path="density" class="control-label" />
                        </div>
                    </div>
                </spring:bind>

                <spring:bind path="assistant">
                    <div class="form-group ${status.error ? 'has-error' : ''}">
                        <label class="col-sm-2 control-label">Лаборант</label>
                        <div class="col-sm-3">
                            <form:input path="assistant" class="form-control" id="assistant" placeholder="Лаборант" />
                            <form:errors path="assistant" class="control-label" />
                        </div>
                    </div>
                </spring:bind>

                <spring:bind path="conclusion">
                    <div class="form-group ${status.error ? 'has-error' : ''}">
                        <label class="col-sm-2 control-label">Заключение</label>
                        <div class="col-sm-3">
                            <form:input path="conclusion" class="form-control" id="conclusion" placeholder="Заключение" />
                            <form:errors path="conclusion" class="control-label" />
                        </div>
                    </div>
                </spring:bind>

                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn-lg pull-right">Сохранить</button>
                    </div>
                </div>
        </form:form>
</div>
</body>
<jsp:include page="../fragments/footer.jsp" />
</html>