<%@ page language="java" contentType="text/html;  charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ page session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html>
<html lang="en">

<jsp:include page="../fragments/header.jsp" />
<body>
<div class="container">

    <h1 align="center">Медицинская карта</h1><br>
    <form:form class="form-horizontal" method="post" modelAttribute="formMedicalRecords" action="/MedicalRecordsAdd">

        <form:input type="hidden" path="id" id="id" />

            <spring:bind path="name">
                <div class="form-group ${status.error ? 'has-error' : ''}">
                    <label class="col-sm-2 control-label">Имя</label>
                    <div class="col-sm-3">
                        <form:input path="name" type="text" class="form-control " id="name" placeholder="Имя" />
                        <form:errors path="name" class="control-label" />
                    </div>
                </div>
            </spring:bind>

            <spring:bind path="lastName">
                <div class="form-group ${status.error ? 'has-error' : ''}">
                    <label class="col-sm-2 control-label">Фамилия</label>
                    <div class="col-sm-3">
                        <form:input path="lastName" type="text" class="form-control " id="lastName" placeholder="Фамилия" />
                        <form:errors path="lastName" class="control-label" />
                    </div>
                </div>
            </spring:bind>

            <spring:bind path="secondName">
                <div class="form-group ${status.error ? 'has-error' : ''}">
                    <label class="col-sm-2 control-label">Отчество</label>
                    <div class="col-sm-3">
                        <form:input path="secondName" class="form-control" id="secondName" placeholder="Отчество" />
                        <form:errors path="secondName" class="control-label" />
                    </div>
                </div>
            </spring:bind>

            <spring:bind path="numberPassport">
                <div class="form-group ${status.error ? 'has-error' : ''}">
                    <label class="col-sm-2 control-label">Номер паспорта</label>
                    <div class="col-sm-3">
                        <form:input path="numberPassport" class="form-control" id="numberPassport" placeholder="Номер паспорта" />
                        <form:errors path="numberPassport" class="control-label" />
                    </div>
                </div>
            </spring:bind>

            <spring:bind path="mobilePhone">
                <div class="form-group ${status.error ? 'has-error' : ''}">
                    <label class="col-sm-2 control-label">Мобильный телефон</label>
                    <div class="col-sm-3">
                        <form:input path="mobilePhone" class="form-control" id="mobilePhone" placeholder="Мобильный телефон" />
                        <form:errors path="mobilePhone" class="control-label" />
                    </div>
                </div>
            </spring:bind>

            <spring:bind path="country">
                <div class="form-group ${status.error ? 'has-error' : ''}">
                    <label class="col-sm-2 control-label">Страна</label>
                    <div class="col-sm-3">
                        <form:input path="country" class="form-control" id="country" placeholder="Страна" />
                        <form:errors path="country" class="control-label" />
                    </div>
                </div>
            </spring:bind>



            <spring:bind path="district">
                <div class="form-group ${status.error ? 'has-error' : ''}">
                    <label class="col-sm-2 control-label">Область</label>
                    <div class="col-sm-3">
                        <form:input path="district" class="form-control" id="district" placeholder="Область" />
                        <form:errors path="district" class="control-label" />
                    </div>
                </div>
            </spring:bind>


            <spring:bind path="city">
                <div class="form-group ${status.error ? 'has-error' : ''}">
                    <label class="col-sm-2 control-label">Город</label>
                    <div class="col-sm-3">
                        <form:input path="city" class="form-control" id="city" placeholder="Город" />
                        <form:errors path="city" class="control-label" />
                    </div>
                </div>
            </spring:bind>


            <spring:bind path="street">
                <div class="form-group ${status.error ? 'has-error' : ''}">
                    <label class="col-sm-2 control-label">Улица</label>
                    <div class="col-sm-3">
                        <form:input path="street" class="form-control" id="street" placeholder="Улица" />
                        <form:errors path="street" class="control-label" />
                    </div>
                </div>
            </spring:bind>


            <spring:bind path="numberHome">
                <div class="form-group ${status.error ? 'has-error' : ''}">
                    <label class="col-sm-2 control-label">Номер дома</label>
                    <div class="col-sm-3">
                        <form:input path="numberHome" class="form-control" id="numberHome" placeholder="Номер дома" />
                        <form:errors path="numberHome" class="control-label" />
                    </div>
                </div>
            </spring:bind>


            <spring:bind path="numberFlat">
                <div class="form-group ${status.error ? 'has-error' : ''}">
                    <label class="col-sm-2 control-label">Мобильный телефон</label>
                    <div class="col-sm-3">
                        <form:input path="numberFlat" class="form-control" id="numberFlat" placeholder="Мобильный телефон" />
                        <form:errors path="numberFlat" class="control-label" />
                    </div>
                </div>
            </spring:bind>

        <h1 align="center">Лечащий врач</h1><br>

        <spring:bind path="doctor">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <label class="col-sm-2 control-label">Лечащий врач</label>
                <div class="col-sm-3">
                    <form:select path="doctor" class="form-control">
                        <form:option value="NONE" label="--- Select ---" />
                        <form:options items="${doctorList}" />
                    </form:select>
                </div>
                <div class="col-sm-3"></div>
            </div>
        </spring:bind>

        <spring:bind path="target">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <label class="col-sm-2 control-label">Отделение</label>
                <div class="col-sm-3">
                    <form:select path="target" class="form-control">
                        <form:option value="NONE" label="--- Select ---" />
                        <form:options items="${targetList}" />
                    </form:select>
                </div>
                <div class="col-sm-3"></div>
            </div>
        </spring:bind>

        <h1 align="center">Описание животного</h1><br>

        <spring:bind path="kind">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <label class="col-sm-2 control-label">Вид</label>
                <div class="col-sm-3">
                    <form:input path="kind" class="form-control" id="kind" placeholder="Вид" />
                    <form:errors path="kind" class="control-label" />
                </div>
            </div>
        </spring:bind>

        <spring:bind path="animalName">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <label class="col-sm-2 control-label">Кличка</label>
                <div class="col-sm-3">
                    <form:input path="animalName" class="form-control" id="animalName" placeholder="Кличка" />
                    <form:errors path="animalName" class="control-label" />
                </div>
            </div>
        </spring:bind>

        <spring:bind path="sex">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <label class="col-sm-2 control-label">Пол</label>
                <div class="col-sm-3">
                    <form:select path="sex" class="form-control">
                        <form:option value="NONE" label="--- Select ---" />
                        <form:options items="${sexList}" />
                    </form:select>
                </div>
                <div class="col-sm-3"></div>
            </div>
        </spring:bind>

        <spring:bind path="age">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <label class="col-sm-2 control-label">Возраст</label>
                <div class="col-sm-3">
                    <form:input path="age" class="form-control" id="age" placeholder="Возраст" />
                    <form:errors path="age" class="control-label" />
                </div>
            </div>
        </spring:bind>

        <spring:bind path="breed">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <label class="col-sm-2 control-label">Порода</label>
                <div class="col-sm-3">
                    <form:input path="breed" class="form-control" id="breed" placeholder="Порода" />
                    <form:errors path="breed" class="control-label" />
                </div>
            </div>
        </spring:bind>

        <spring:bind path="fatness">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <label class="col-sm-2 control-label">Упитанность</label>
                <div class="col-sm-3">
                    <form:select path="fatness" class="form-control">
                        <form:option value="NONE" label="--- Select ---" />
                        <form:options items="${fatnessList}" />
                    </form:select>
                </div>
                <div class="col-sm-3"></div>
            </div>
        </spring:bind>

        <h1 align="center">Вет. паспорт</h1><br>

        <spring:bind path="deworming">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <label class="col-sm-2 control-label">Дегельминтация</label>
                <div class="col-sm-3">
                    <form:input path="deworming" class="form-control" id="breed" placeholder="Дегельминтация" />
                    <form:errors path="deworming" class="control-label" />
                </div>
            </div>
        </spring:bind>

        <spring:bind path="numberVetPassport">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <label class="col-sm-2 control-label">Номер Вет. паспорта</label>
                <div class="col-sm-3">
                    <form:input path="numberVetPassport" class="form-control" id="numberVetPassport" placeholder="Номер Вет. паспорта" />
                    <form:errors path="numberVetPassport" class="control-label" />
                </div>
            </div>
        </spring:bind>

        <spring:bind path="dateOfIssue">
         <div class="form-group ${status.error ? 'has-error' : ''}">
            <label class="col-sm-2 control-label">Дата выдачи</label>
            <div class="col-sm-3">
            <div class="input-group date" id="datetimepicker4">
                <form:input path="dateOfIssue" type="text" class="form-control" placeholder="Дата выдачи" />
                    <span class="input-group-addon">
                     </span>
            </div>
            <script type="text/javascript">
                $(function () {
                    $('#datetimepicker4').datetimepicker(
                            {pickTime: false, language: 'ru'}
                    );
                });
            </script>
            </div>
         </div>
        </spring:bind>

        <spring:bind path="issued">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <label class="col-sm-2 control-label">Номер Вет. паспорта</label>
                <div class="col-sm-3">
                    <form:input path="issued" class="form-control" id="issued" placeholder="Номер Вет. паспорта" />
                    <form:errors path="issued" class="control-label" />
                </div>
            </div>
        </spring:bind>

        <h1 align="center">Вакцинация</h1><br>

        <spring:bind path="preDiagnosis">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <label class="col-sm-2 control-label">Предварительный диагноз</label>
                <div class="col-sm-3">
                    <form:input path="preDiagnosis" class="form-control" id="preDiagnosis" placeholder="Предварительный диагноз" />
                    <form:errors path="preDiagnosis" class="control-label" />
                </div>
            </div>
        </spring:bind>


        <spring:bind path="vaccinedBy">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <label class="col-sm-2 control-label">Кем проведена</label>
                <div class="col-sm-3">
                    <form:input path="vaccinedBy" class="form-control" id="vaccinedBy" placeholder="Кем проведена" />
                    <form:errors path="vaccinedBy" class="control-label" />
                </div>
            </div>
        </spring:bind>

        <spring:bind path="specialSigns">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <label class="col-sm-2 control-label">Особые приметы</label>
                <div class="col-sm-3">
                    <form:input path="specialSigns" class="form-control" id="specialSigns" placeholder="Особые приметы" />
                    <form:errors path="specialSigns" class="control-label" />
                </div>
            </div>
        </spring:bind>

        <h1 align="center">Общая информация</h1><br>

        <spring:bind path="dataAndTimeRequest">
             <div class="form-group ${status.error ? 'has-error' : ''}">
                <label class="col-sm-2 control-label">Дата и время обращения</label>
                    <div class="col-sm-3">
                     <div class="input-group date" id="datetimepicker2">
                        <form:input path="dataAndTimeRequest" type="text" class="form-control" placeholder="Дата и время обращения" />
                        <span class="input-group-addon">
                        </span>
                        <script type="text/javascript">
                            $(function () {
                                //Установим для виджета русскую локаль с помощью параметра language и значения ru
                                $('#datetimepicker2').datetimepicker(
                                        {language: 'ru'}
                                );
                            });
                        </script>
                     </div>
                    </div>
             </div>
        </spring:bind>

        <spring:bind path="preDiagnosis">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <label class="col-sm-2 control-label">Предварительный диагноз</label>
                <div class="col-sm-3">
                    <form:input path="preDiagnosis" class="form-control" id="preDiagnosis" placeholder="Предварительный диагноз" />
                    <form:errors path="preDiagnosis" class="control-label" />
                </div>
            </div>
        </spring:bind>

        <spring:bind path="dateAndTimeAnalysis">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <label class="col-sm-2 control-label">Дата и время проведения исследования</label>
                <div class="col-sm-3">
                    <div class="input-group date" id="datetimepicker3">
                        <form:input path="dateAndTimeAnalysis" type="text" class="form-control"  placeholder="Дата и время проведения исследования" />
                        <span class="input-group-addon">
                        </span>
                        <script type="text/javascript">
                            $(function () {
                                //Установим для виджета русскую локаль с помощью параметра language и значения ru
                                $('#datetimepicker3').datetimepicker(
                                        {language: 'ru'}
                                );
                            });
                        </script>
                    </div>
                </div>
            </div>
        </spring:bind>

        <spring:bind path="temperature">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <label class="col-sm-2 control-label">Температура</label>
                <div class="col-sm-3">
                    <form:input path="temperature" class="form-control" id="temperature" placeholder="Температура" />
                    <form:errors path="temperature" class="control-label" />
                </div>
            </div>
        </spring:bind>

        <spring:bind path="pulse">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <label class="col-sm-2 control-label">Пульс</label>
                <div class="col-sm-3">
                    <form:input path="pulse" class="form-control" id="pulse" placeholder="Пульс" />
                    <form:errors path="pulse" class="control-label" />
                </div>
            </div>
        </spring:bind>

        <spring:bind path="breath">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <label class="col-sm-2 control-label">Дыхание</label>
                <div class="col-sm-3">
                    <form:input path="breath" class="form-control" id="breath" placeholder="Дыхание" />
                    <form:errors path="breath" class="control-label" />
                </div>
            </div>
        </spring:bind>

        <spring:bind path="habit">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <label class="col-sm-2 control-label">Габитус</label>
                <div class="col-sm-3">
                    <form:input path="habit" class="form-control" id="habit" placeholder="Габитус" />
                    <form:errors path="habit" class="control-label" />
                </div>
            </div>
        </spring:bind>

        <spring:bind path="dermisAnalysis">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <label class="col-sm-2 control-label">Исследование кожи</label>
                <div class="col-sm-3">
                    <form:input path="dermisAnalysis" class="form-control" id="dermisAnalysis" placeholder="Исследование кожи" />
                    <form:errors path="dermisAnalysis" class="control-label" />
                </div>
            </div>
        </spring:bind>

        <spring:bind path="lymphAnalysis">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <label class="col-sm-2 control-label">Исследование лимфоузлов</label>
                <div class="col-sm-3">
                    <form:input path="lymphAnalysis" class="form-control" id="lymphAnalysis" placeholder="Исследование лимфоузлов" />
                    <form:errors path="lymphAnalysis" class="control-label" />
                </div>
            </div>
        </spring:bind>

        <spring:bind path="mucousAnalysis">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <label class="col-sm-2 control-label">Исследование слизистых оболочек</label>
                <div class="col-sm-3">
                    <form:input path="mucousAnalysis" class="form-control" id="mucousAnalysis" placeholder="Исследование слизистых оболочек" />
                    <form:errors path="mucousAnalysis" class="control-label" />
                </div>
            </div>
        </spring:bind>

        <h1 align="center">Исследование отдельных систем</h1><br>

        <spring:bind path="systemCirculatory">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <label class="col-sm-2 control-label">Система органов кровообращения</label>
                <div class="col-sm-3">
                    <form:input path="systemCirculatory" class="form-control" id="systemCirculatory" placeholder="Система органов кровообращения" />
                    <form:errors path="systemCirculatory" class="control-label" />
                </div>
            </div>
        </spring:bind>

        <spring:bind path="systemBreath">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <label class="col-sm-2 control-label">Система органов дыхания</label>
                <div class="col-sm-3">
                    <form:input path="systemBreath" class="form-control" id="systemBreath" placeholder="Система органов дыхания" />
                    <form:errors path="systemBreath" class="control-label" />
                </div>
            </div>
        </spring:bind>

        <spring:bind path="systemIndigestion">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <label class="col-sm-2 control-label">Система органов пищеварения</label>
                <div class="col-sm-3">
                    <form:input path="systemIndigestion" class="form-control" id="systemIndigestion" placeholder="Система органов пищеварения" />
                    <form:errors path="systemIndigestion" class="control-label" />
                </div>
            </div>
        </spring:bind>

        <spring:bind path="systemUrination">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <label class="col-sm-2 control-label">Система органов мочеотделения</label>
                <div class="col-sm-3">
                    <form:input path="systemUrination" class="form-control" id="systemUrination" placeholder="Система органов мочеотделения" />
                    <form:errors path="systemUrination" class="control-label" />
                </div>
            </div>
        </spring:bind>

        <spring:bind path="systemGenital">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <label class="col-sm-2 control-label">Система половых органов</label>
                <div class="col-sm-3">
                    <form:input path="systemGenital" class="form-control" id="systemGenital" placeholder="Система половых органов" />
                    <form:errors path="systemGenital" class="control-label" />
                </div>
            </div>
        </spring:bind>

        <spring:bind path="systemBreast">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <label class="col-sm-2 control-label">Молочная железа</label>
                <div class="col-sm-3">
                    <form:input path="systemBreast" class="form-control" id="systemBreast" placeholder="Молочная железа" />
                    <form:errors path="systemBreast" class="control-label" />
                </div>
            </div>
        </spring:bind>

        <spring:bind path="systemMovement">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <label class="col-sm-2 control-label">Система органов движения</label>
                <div class="col-sm-3">
                    <form:input path="systemMovement" class="form-control" id="systemMovement" placeholder="Система органов движения" />
                    <form:errors path="systemMovement" class="control-label" />
                </div>
            </div>
        </spring:bind>

        <spring:bind path="systemSenses">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <label class="col-sm-2 control-label">Органы чувств</label>
                <div class="col-sm-3">
                    <form:input path="systemSenses" class="form-control" id="systemSenses" placeholder="Органы чувств" />
                    <form:errors path="systemSenses" class="control-label" />
                </div>
            </div>
        </spring:bind>

        <spring:bind path="systemNervous">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <label class="col-sm-2 control-label">Нервная система</label>
                <div class="col-sm-3">
                    <form:input path="systemNervous" class="form-control" id="systemNervous" placeholder="Нервная система" />
                    <form:errors path="systemNervous" class="control-label" />
                </div>
            </div>
        </spring:bind>

        <h1 align="center">Специальные исследования</h1><br>

        <spring:bind path="serologicalAnalysis">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <label class="col-sm-2 control-label">Серологические</label>
                <div class="col-sm-3">
                    <form:input path="serologicalAnalysis" class="form-control" id="serologicalAnalysis" placeholder="Серологические" />
                    <form:errors path="serologicalAnalysis" class="control-label" />
                </div>
            </div>
        </spring:bind>

        <spring:bind path="allergicAnalysis">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <label class="col-sm-2 control-label">Аллергические</label>
                <div class="col-sm-3">
                    <form:input path="allergicAnalysis" class="form-control" id="allergicAnalysis" placeholder="Аллергические" />
                    <form:errors path="allergicAnalysis" class="control-label" />
                </div>
            </div>
        </spring:bind>

        <spring:bind path="bacteriologicalAnalysis">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <label class="col-sm-2 control-label">Бактериологические</label>
                <div class="col-sm-3">
                    <form:input path="bacteriologicalAnalysis" class="form-control" id="bacteriologicalAnalysis" placeholder="Бактериологические" />
                    <form:errors path="bacteriologicalAnalysis" class="control-label" />
                </div>
            </div>
        </spring:bind>

        <spring:bind path="othersAnalysis">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <label class="col-sm-2 control-label">Другие</label>
                <div class="col-sm-3">
                    <form:input path="othersAnalysis" class="form-control" id="othersAnalysis" placeholder="Другие" />
                    <form:errors path="othersAnalysis" class="control-label" />
                </div>
            </div>
        </spring:bind>

        <spring:url value="/medical_records/blood_analysis" var="userUrl" />
        <spring:url value="/blood_analysis/${blood_analysis.id}" var="showUrl" />


        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn-lg pull-right">Сохранить</button>
            </div>
        </div>
    </form:form>

</div>

<jsp:include page="../fragments/footer.jsp" />

</body>
</html>