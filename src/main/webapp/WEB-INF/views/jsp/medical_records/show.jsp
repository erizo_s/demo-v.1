<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ page session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html>
<html lang="en">

<jsp:include page="../fragments/header.jsp" />
<body>
<div class="container">

    <div style="text-align: center;"><table style="table-layout: fixed; width: 700px; align-self: stretch" class="table table-bordered">
        <thead>
        <tr>
            <td align="center" colspan="3">Физико-химическое и биохимическое исследование крови</td>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>Дата</td>
            <td align="center" colspan="2">${blood_analysis.dateAnalysis}</td>
        </tr>
        <tr>
            <td>Время</td>
            <td align="center" colspan="2">${blood_analysis.timeAnalysis}</td>
        </tr>
        <tr>
            <td>Владелец</td>
            <td align="center" colspan="2">${blood_analysis.owner}</td>
        </tr>
        <tr>
            <td>Животное</td>
            <td align="center" colspan="2">${blood_analysis.kind}</td>
        </tr>
        <tr>
            <td></td>
            <td align="center">Норма</td>
            <td align="center">Показатель</td>
        </tr>
        <tr>
            <td>Удельный вес</td>
            <td align="center">Норма</td>
            <td align="center">${blood_analysis.specificGravity}</td>
        </tr>
        <tr>
            <td>Вязкость</td>
            <td align="center">Норма</td>
            <td align="center">${blood_analysis.viscosity}</td>
        </tr>
        <tr>
            <td>СОЭ</td>
            <td align="center">Норма</td>
            <td align="center">${blood_analysis.soe}</td>
        </tr>
        <tr>
            <td>Гемоглобин</td>
            <td align="center">Норма</td>
            <td align="center">${blood_analysis.hemoglobin}</td>
        </tr>
        <tr>
            <td>Кальций</td>
            <td align="center">Норма</td>
            <td align="center">${blood_analysis.calcium}</td>
        </tr>
        <tr>
            <td>Неорганический фосфор</td>
            <td align="center">Норма</td>
            <td align="center">${blood_analysis.inogranicPhosphorus}</td>
        </tr>
        <tr>
            <td>Резервная щелочность</td>
            <td align="center">Норма</td>
            <td align="center">${blood_analysis.reserveAlkalinity}</td>
        </tr>
        <tr>
            <td>Каротин</td>
            <td align="center">Норма</td>
            <td align="center">${blood_analysis.carotene}</td>
        </tr>
        <tr>
            <td>Общий белок</td>
            <td align="center">Норма</td>
            <td align="center">${blood_analysis.totalProtein}</td>
        </tr>
        <tr>
            <td>Глюкоза</td>
            <td align="center">Норма</td>
            <td align="center">${blood_analysis.glucose}</td>
        </tr>
        <tr>
            <td>Билирубин</td>
            <td align="center">Норма</td>
            <td align="center">${blood_analysis.biliRubin}</td>
        </tr>
        <tr>
            <td>Лаборант</td>
            <td align="center" colspan="2">${blood_analysis.assistant}</td>
        </tr>
        <tr>
            <td>Заключение</td>
            <td align="center" colspan="2">${blood_analysis.conclusion}</td>
        </tr>


        </tbody>

    </table>
        <spring:url value="/medical_records/blood_analysis" var="blood_analysis" />
        <button class="btn btn-danger" onclick="location.href='${blood_analysis}'">Провести анализ</button><br><br><br><br>
        <table style="table-layout: fixed; width: 700px; align-self: stretch" class="table table-bordered">
            <thead>
            <tr>
                <td align="center" colspan="3">Морфологическое исследование крови</td>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>Дата</td>
                <td align="center" colspan="2">${blood_analysis_morphological.dateAnalysisMorphological}</td>
            </tr>
            <tr>
                <td>Время</td>
                <td align="center" colspan="2">${blood_analysis_morphological.timeAnalysisMorphological}</td>
            </tr>
            <tr>
                <td>Владелец</td>
                <td align="center" colspan="2">${blood_analysis_morphological.ownerMorphological}</td>
            </tr>
            <tr>
                <td>Животное</td>
                <td align="center" colspan="2">${blood_analysis_morphological.kindMorphological}</td>
            </tr>
            <tr>
                <td></td>
                <td align="center">Норма</td>
                <td align="center">Показатель</td>
            </tr>
            <tr>
                <td>Эритроциты</td>
                <td align="center">Норма</td>
                <td align="center">${blood_analysis_morphological.redBloodCells}</td>
            </tr>
            <tr>
                <td>Лейкоциты</td>
                <td align="center">Норма</td>
                <td align="center">${blood_analysis_morphological.leukocyte}</td>
            </tr>
            <tr>
                <td>Лаборант</td>
                <td align="center" colspan="2">${blood_analysis_morphological.assistantMorphological}</td>
            </tr>
            <tr>
                <td>Заключение</td>
                <td align="center" colspan="2">${blood_analysis_morphological.m_conclusion}</td>
            </tr>
            </tbody>
        </table>
        <spring:url value="/medical_records/blood_analysis_morphological" var="morphological" />
        <button class="btn btn-danger" onclick="location.href='${morphological}'">Провести анализ</button><br><br><br><br>
    </div><br><br><br><br>

    <table class="table table-bordered">
        <thead>
        <tr>
            <td align="center" colspan="16">Лейкограмма</td>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>Дата</td>
            <td>Время</td>
            <td>Владелец</td>
            <td>Животное</td>
            <td>Баз.</td>
            <td>Эозин.</td>
            <td colspan="4">Нейтрофилы</td>
            <td>Лимф</td>
            <td>Мон.</td>
            <td>Инд. ядерн. сдвига</td>
            <td>Лаборант</td>
            <td>Примечания</td>
        </tr>
        <tr>
            <td>${blood_analysis_leukogram.dateAnalysisLeukogram}</td>
            <td>${blood_analysis_leukogram.timeAnalysisLeukogram}</td>
            <td>${blood_analysis_leukogram.ownerLeukogram}</td>
            <td>${blood_analysis_leukogram.kindLeukogram}</td>
            <td>${blood_analysis_leukogram.base}</td>
            <td>${blood_analysis_leukogram.eosin}</td>
            <td>М</td>
            <td>Ю</td>
            <td>П</td>
            <td>С</td>
            <td>${blood_analysis_leukogram.lymphoma}</td>
            <td>${blood_analysis_leukogram.mont}</td>
            <td>${blood_analysis_leukogram.nuclear}</td>
            <td>${blood_analysis_leukogram.assistantLeukogram}</td>
            <td>${blood_analysis_leukogram.l_conclusion}</td>
        </tr>
        </tbody>
    </table>
    <spring:url value="/medical_records/blood_analysis_leukogram" var="leukogram" />
    <button class="btn btn-danger" onclick="location.href='${leukogram}'">Провести анализ</button><br><br><br><br>





</div>




<jsp:include page="../fragments/footer.jsp" />

</body>
</html>