<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <title>История болезни</title>
</head>
<body>
<jsp:include page="../fragments/header.jsp" />

<div class="container" style="width: 70%">

    <h1 align="center">Медицинские карты</h1>

    <table class="table table-striped">
        <thead>
        <tr>
            <th>Номер №</th>
            <th>Имя</th>
            <th>Фамилия</th>
        </tr>
        </thead>
        <c:forEach var="medical_records"  items="${medical_records}">
            <tr>
                <td>${medical_records.id}</td>
                <td>${medical_records.name}</td>
                <td>${medical_records.lastName}</td>
                <spring:url value="/medical_records/${medical_records.id}" var="showUrl"/>
                <spring:url value="/blood_analysis/${medical_records.id}" var="updateUrl"/>
                <spring:url value="/uroscopy_analysis/${medical_records.id}" var="showUrina"/>
                <spring:url value="/faecal_flotation/${medical_records.id}" var="showFic"/>
                <spring:url value="/services/${medical_records.id}" var="showServices"/>
                <spring:url value="/example" var="example" />
                <td><button class="btn btn-success" onclick="location.href='${showUrl}'">Просмотр карты</button>
                    <button class="btn btn-danger" onclick="location.href='${updateUrl}'">Просмотр анализа крови</button>
                    <button class="btn btn-danger" onclick="location.href='${showUrina}'">Просмотр анализа мочи</button>
                    <button class="btn btn-danger" onclick="location.href='${showFic}'">Просмотр анализа фикалий</button>
                    <button class="btn btn-danger" onclick="location.href='${showServices}'">Оказанные услуги</button>
                    <button class="btn btn-danger" onclick="location.href='${example}'">example</button>
                </td>
            </tr>
        </c:forEach>
    </table>
</div>
<jsp:include page="../fragments/footer.jsp" />
</body>
</html>