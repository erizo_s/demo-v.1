<%@ page language="java" contentType="text/html;  charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ page session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html lang="en">
<jsp:include page="../fragments/header.jsp" />
<body>
<body>
<h4 class="panel-title">
    <a data-toggle="collapse" data-parent="#accordion" href="#collapse3"><h1>Микроскопические исследования мочи</h1></a><br></h4><br>
                <div id="collapse3" class="panel-collapse collapse">
                    <form:form method="POST" modelAttribute="uroscopy_analysis_microscopic" class="form-horizontal" action="/UroscopyAnalysisMicroscopicAdd">
                    <form:input type="hidden" path="id" id="id" />
                    <spring:bind path="microscopicDateAnalysis">
                        <div class="form-group ${status.error ? 'has-error' : ''}">
                            <label class="col-sm-2 control-label">Дата исследования</label>
                            <div class="col-sm-3">
                                <div class="input-group date" id="datetimepicker3">
                                    <form:input path="microscopicDateAnalysis" type="text" class="form-control" placeholder="Дата исследования" />
                                                                    <span class="input-group-addon">
                                                                     </span>
                                </div>
                                <script type="text/javascript">
                                    $(function () {
                                        $('#datetimepicker3').datetimepicker(
                                                {pickTime: false, language: 'ru'}
                                        );
                                    });
                                </script>
                            </div>
                        </div>
                    </spring:bind>

                    <spring:bind path="microscopicTimeAnalysis">
                        <div class="form-group ${status.error ? 'has-error' : ''}">
                            <label class="col-sm-2 control-label">Время исследования</label>
                            <div class="col-sm-3">
                                <div class="input-group date" id="datetimepicker5">
                                    <form:input path="microscopicTimeAnalysis" type="text" class="form-control" placeholder="Время исследования" />
                                                                    <span class="input-group-addon">
                                                                     </span>
                                </div>
                                <script type="text/javascript">
                                    $(function () {
                                        $('#datetimepicker5').datetimepicker(
                                                {pickDate: false, language: 'ru'}
                                        );
                                    });
                                </script>
                            </div>
                        </div>
                    </spring:bind>

                    <spring:bind path="microscopicOwner">
                        <div class="form-group ${status.error ? 'has-error' : ''}">
                            <label class="col-sm-2 control-label">Владелец</label>
                            <div class="col-sm-3">
                                <form:input path="microscopicOwner" class="form-control" id="microscopicOwner" placeholder="Владелец" />
                                <form:errors path="microscopicOwner" class="control-label" />
                            </div>
                        </div>
                    </spring:bind>

                    <spring:bind path="microscopicKind">
                        <div class="form-group ${status.error ? 'has-error' : ''}">
                            <label class="col-sm-2 control-label">Животное</label>
                            <div class="col-sm-3">
                                <form:input path="microscopicKind" class="form-control" id="microscopicKind" placeholder="Животное" />
                                <form:errors path="microscopicKind" class="control-label" />
                            </div>
                        </div>
                    </spring:bind>

                    <spring:bind path="fugitiveRainfall">
                        <div class="form-group ${status.error ? 'has-error' : ''}">
                            <label class="col-sm-2 control-label">Неорганизованные осадки</label>
                            <div class="col-sm-3">
                                <form:input path="fugitiveRainfall" class="form-control" id="fugitiveRainfall" placeholder="Неорганизованные осадки" />
                                <form:errors path="fugitiveRainfall" class="control-label" />
                            </div>
                        </div>
                    </spring:bind>

                    <spring:bind path="organizedRainfall">
                        <div class="form-group ${status.error ? 'has-error' : ''}">
                            <label class="col-sm-2 control-label">Организованные осадки</label>
                            <div class="col-sm-3">
                                <form:input path="organizedRainfall" class="form-control" id="organizedRainfall" placeholder="Организованные осадки" />
                                <form:errors path="organizedRainfall" class="control-label" />
                            </div>
                        </div>
                    </spring:bind>

                    <spring:bind path="microscopicAssistant">
                        <div class="form-group ${status.error ? 'has-error' : ''}">
                            <label class="col-sm-2 control-label">Лаборант</label>
                            <div class="col-sm-3">
                                <form:input path="microscopicAssistant" class="form-control" id="microscopicAssistant" placeholder="Лаборант" />
                                <form:errors path="microscopicAssistant" class="control-label" />
                            </div>
                        </div>
                    </spring:bind>

                    <spring:bind path="microscopicConclusion">
                        <div class="form-group ${status.error ? 'has-error' : ''}">
                            <label class="col-sm-2 control-label">Заключение</label>
                            <div class="col-sm-3">
                                <form:input path="microscopicConclusion" class="form-control" id="microscopicConclusion" placeholder="Заключение" />
                                <form:errors path="microscopicConclusion" class="control-label" />
                            </div>
                        </div>
                    </spring:bind>
                </div>

                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn-lg pull-right">Сохранить</button>
                    </div>
                </div>
            </form:form>



</body>
<jsp:include page="../fragments/footer.jsp" />
</html>
