<%@ page language="java" contentType="text/html;  charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ page session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html>
<html lang="en">
<jsp:include page="../fragments/header.jsp" />
<body>

<div class="container" >

    <div class="panel-group" id="accordion">

        <h1>Лейкограмма</h1>
        <form:form method="POST" modelAttribute="blood_analysis_leukogram" class="form-horizontal" action="/BloodAnalysisAddLeukogram">
            <form:input type="hidden" path="id" id="id" />
                <spring:bind path="dateAnalysisLeukogram">
                    <div class="form-group ${status.error ? 'has-error' : ''}">
                        <label class="col-sm-2 control-label">Дата исследования</label>
                        <div class="col-sm-3">
                            <div class="input-group date" id="datetimepicker3">
                                <form:input path="dateAnalysisLeukogram" type="text" class="form-control" placeholder="Дата исследования" />
                                                        <span class="input-group-addon">
                                                         </span>
                            </div>
                            <script type="text/javascript">
                                $(function () {
                                    $('#datetimepicker3').datetimepicker(
                                            {pickTime: false, language: 'ru'}
                                    );
                                });
                            </script>
                        </div>
                    </div>
                </spring:bind>

                <spring:bind path="timeAnalysisLeukogram">
                    <div class="form-group ${status.error ? 'has-error' : ''}">
                        <label class="col-sm-2 control-label">Время исследования</label>
                        <div class="col-sm-3">
                            <div class="input-group date" id="datetimepicker5">
                                <form:input path="timeAnalysisLeukogram" type="text" class="form-control" placeholder="Время исследования" />
                                                        <span class="input-group-addon">
                                                         </span>
                            </div>
                            <script type="text/javascript">
                                $(function () {
                                    $('#datetimepicker5').datetimepicker(
                                            {pickDate: false, language: 'ru'}
                                    );
                                });
                            </script>
                        </div>
                    </div>
                </spring:bind>

                <spring:bind path="ownerLeukogram">
                    <div class="form-group ${status.error ? 'has-error' : ''}">
                        <label class="col-sm-2 control-label">Владелец</label>
                        <div class="col-sm-3">
                            <form:input path="ownerLeukogram" class="form-control" id="ownerLeukogram" placeholder="Ф.И.О." />
                            <form:errors path="ownerLeukogram" class="control-label" />
                        </div>
                    </div>
                </spring:bind>

                <spring:bind path="kindLeukogram">
                    <div class="form-group ${status.error ? 'has-error' : ''}">
                        <label class="col-sm-2 control-label">Животное</label>
                        <div class="col-sm-3">
                            <form:input path="kindLeukogram" class="form-control" id="kindLeukogram" placeholder="Животное" />
                            <form:errors path="kindLeukogram" class="control-label" />
                        </div>
                    </div>
                </spring:bind>

            <spring:bind path="base">
                <div class="form-group ${status.error ? 'has-error' : ''}">
                    <label class="col-sm-2 control-label">Баз.</label>
                    <div class="col-sm-3">
                        <form:input path="base" class="form-control" id="base" placeholder="Баз." />
                        <form:errors path="base" class="control-label" />
                    </div>
                </div>
            </spring:bind>

            <spring:bind path="eosin">
                <div class="form-group ${status.error ? 'has-error' : ''}">
                    <label class="col-sm-2 control-label">Эозин.</label>
                    <div class="col-sm-3">
                        <form:input path="eosin" class="form-control" id="eosin" placeholder="Эозин." />
                        <form:errors path="eosin" class="control-label" />
                    </div>
                </div>
            </spring:bind>

            <spring:bind path="neutrophils">
                <div class="form-group ${status.error ? 'has-error' : ''}">
                    <label class="col-sm-2 control-label">Нейтрофилы</label>
                    <div class="col-sm-3">
                        <form:input path="neutrophils" class="form-control" id="neutrophils" placeholder="Нейтрофилы" />
                        <form:errors path="neutrophils" class="control-label" />
                    </div>
                </div>
            </spring:bind>

            <spring:bind path="lymphoma">
                <div class="form-group ${status.error ? 'has-error' : ''}">
                    <label class="col-sm-2 control-label">Лимф.</label>
                    <div class="col-sm-3">
                        <form:input path="lymphoma" class="form-control" id="lymphoma" placeholder="Лимф." />
                        <form:errors path="lymphoma" class="control-label" />
                    </div>
                </div>
            </spring:bind>

            <spring:bind path="mont">
                <div class="form-group ${status.error ? 'has-error' : ''}">
                    <label class="col-sm-2 control-label">Мон.</label>
                    <div class="col-sm-3">
                        <form:input path="mont" class="form-control" id="mont" placeholder="Мон." />
                        <form:errors path="mont" class="control-label" />
                    </div>
                </div>
            </spring:bind>

            <spring:bind path="nuclear">
                <div class="form-group ${status.error ? 'has-error' : ''}">
                    <label class="col-sm-2 control-label">Инд. ядерн. сдвига</label>
                    <div class="col-sm-3">
                        <form:input path="nuclear" class="form-control" id="nuclear" placeholder="Инд. ядерн. сдвига" />
                        <form:errors path="nuclear" class="control-label" />
                    </div>
                </div>
            </spring:bind>

            <spring:bind path="assistantLeukogram">
                <div class="form-group ${status.error ? 'has-error' : ''}">
                    <label class="col-sm-2 control-label">Лаборант</label>
                    <div class="col-sm-3">
                        <form:input path="assistantLeukogram" class="form-control" id="assistantLeukogram" placeholder="Лаборант" />
                        <form:errors path="assistantLeukogram" class="control-label" />
                    </div>
                </div>
            </spring:bind>

            <spring:bind path="l_conclusion">
                <div class="form-group ${status.error ? 'has-error' : ''}">
                    <label class="col-sm-2 control-label">Примечание</label>
                    <div class="col-sm-3">
                        <form:input path="l_conclusion" class="form-control" id="l_conclusion" placeholder="Примечание" />
                        <form:errors path="l_conclusion" class="control-label" />
                    </div>
                </div>
            </spring:bind>

    </div>

    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn-lg pull-right">Сохранить</button>
        </div>
    </div>
    </form:form>



</div>




</body>
<jsp:include page="../fragments/footer.jsp" />
</html>