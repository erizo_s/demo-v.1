<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ page session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html>
<html lang="en">

<jsp:include page="../fragments/header.jsp" />
<spring:url value="/medical_records/uroscopy_analysis" var="uroscopyAddAnalysis" />
<spring:url value="/medical_records/uroscopy_analysis_chemical" var="UroscopyAnalysisChemicalAdd" />
<spring:url value="/medical_records/uroscopy_analysis_microscopic" var="UroscopyAnalysisMicroscopicAdd" />
<body>
<div class="container">

    <div style="text-align: center">
        <table style="table-layout: fixed; width: 700px; align-self: stretch" class="table table-bordered">
        <thead>
        <tr>
            <td align="center" colspan="3">Физические свойства мочи</td>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>Дата</td>
            <td align="center" colspan="2">${uroscopy_analysis.dateAnalysis}</td>
        </tr>
        <tr>
            <td>Время</td>
            <td align="center" colspan="2">${uroscopy_analysis.timeAnalysis}</td>
        </tr>
        <tr>
            <td>Владелец</td>
            <td align="center" colspan="2">${uroscopy_analysis.owner}</td>
        </tr>
        <tr>
            <td>Животное</td>
            <td align="center" colspan="2">${uroscopy_analysis.kind}</td>
        </tr>
        <tr>
            <td></td>
            <td align="center">Норма</td>
            <td align="center">Показатель</td>
        </tr>
        <tr>
            <td>Количество</td>
            <td align="center">Норма</td>
            <td align="center">${uroscopy_analysis.number}</td>
        </tr>
        <tr>
            <td>Цвет</td>
            <td align="center">Норма</td>
            <td align="center">${uroscopy_analysis.color}</td>
        </tr>
        <tr>
            <td>Прозрачность</td>
            <td align="center">Норма</td>
            <td align="center">${uroscopy_analysis.transparency}</td>
        </tr>
        <tr>
            <td>Консистенция</td>
            <td align="center">Норма</td>
            <td align="center">${uroscopy_analysis.consistency}</td>
        </tr>
        <tr>
            <td>Запах</td>
            <td align="center">Норма</td>
            <td align="center">${uroscopy_analysis.smell}</td>
        </tr>
        <tr>
            <td>Плотность</td>
            <td align="center">Норма</td>
            <td align="center">${uroscopy_analysis.density}</td>
        </tr>
        <tr>
            <td>Лаборант</td>
            <td align="center">Норма</td>
            <td align="center">${uroscopy_analysis.assistant}</td>
        </tr>
        <tr>
            <td>Заключение</td>
            <td align="center">Норма</td>
            <td align="center">${uroscopy_analysis.conclusion}</td>
        </tr>
        </tbody>
    </table>

        <button class="btn btn-danger" onclick="location.href='${uroscopyAddAnalysis}'">Провести анализ</button><br><br><br><br>
        <table style="table-layout: fixed; width: 700px; align-self: stretch" class="table table-bordered">
            <thead>
            <tr>
                <td align="center" colspan="3">Химический анализ мочи</td>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>Дата</td>
                <td align="center" colspan="2">${uroscopy_analysis_chemical.chemistryDateAnalysis}</td>
            </tr>
            <tr>
                <td>Время</td>
                <td align="center" colspan="2">${uroscopy_analysis_chemical.chemistryTimeAnalysis}</td>
            </tr>
            <tr>
                <td>Владелец</td>
                <td align="center" colspan="2">${uroscopy_analysis_chemical.ownerChemical}</td>
            </tr>
            <tr>
                <td>Животное</td>
                <td align="center" colspan="2">${uroscopy_analysis_chemical.kindChemical}</td>
            </tr>
            <tr>
                <td></td>
                <td align="center">Норма</td>
                <td align="center">Показатель</td>
            </tr>
            <tr>
                <td>Реакция</td>
                <td align="center">Норма</td>
                <td align="center">${uroscopy_analysis_chemical.reaction}</td>
            </tr>
            <tr>
                <td>Белок</td>
                <td align="center">Норма</td>
                <td align="center">${uroscopy_analysis_chemical.protein}</td>
            </tr>
            <tr>
                <td>Протеозы</td>
                <td align="center">Норма</td>
                <td align="center" >${uroscopy_analysis_chemical.proteose}</td>
            </tr>
            <tr>
                <td>Сахар</td>
                <td align="center">Норма</td>
                <td align="center" >${uroscopy_analysis_chemical.sugar}</td>
            </tr>
            <tr>
                <td>Пигменты крови</td>
                <td align="center">Норма</td>
                <td align="center" >${uroscopy_analysis_chemical.pigmentsBlood}</td>
            </tr>
            <tr>
                <td>Желчные пигменты</td>
                <td align="center">Норма</td>
                <td align="center" >${uroscopy_analysis_chemical.cholochrome}</td>
            </tr>
            <tr>
                <td>Индикан</td>
                <td align="center">Норма</td>
                <td align="center" >${uroscopy_analysis_chemical.indican}</td>
            </tr>
            <tr>
                <td>Уробилин</td>
                <td align="center">Норма</td>
                <td align="center" >${uroscopy_analysis_chemical.urobilin}</td>
            </tr>
            <tr>
                <td>Кетоновые тела</td>
                <td align="center">Норма</td>
                <td align="center" >${uroscopy_analysis_chemical.ketoneBodies}</td>
            </tr>
            <tr>
                <td>Лаборант</td>
                <td align="center">Норма</td>
                <td align="center" >${uroscopy_analysis_chemical.assistantChemical}</td>
            </tr>
            <tr>
                <td>Заключение</td>
                <td align="center">Норма</td>
                <td align="center" >${uroscopy_analysis_chemical.chemistryConclusion}</td>
            </tr>
            </tbody>
        </table>
    <button class="btn btn-danger" onclick="location.href='${UroscopyAnalysisChemicalAdd}'">Провести анализ</button><br><br><br><br>
    <table style="table-layout: fixed; width: 700px; align-self: stretch" class="table table-bordered">
        <thead>
        <tr>
            <td align="center" colspan="3">Микроскопические исследования мочи</td>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>Дата</td>
            <td align="center" colspan="2">${uroscopy_analysis_microscopic.microscopicDateAnalysis}</td>
        </tr>
        <tr>
            <td>Время</td>
            <td align="center" colspan="2">${uroscopy_analysis_microscopic.microscopicTimeAnalysis}</td>
        </tr>
        <tr>
            <td>Владелец</td>
            <td align="center" colspan="2">${uroscopy_analysis_microscopic.microscopicOwner}</td>
        </tr>
        <tr>
            <td>Животное</td>
            <td align="center" colspan="2">${uroscopy_analysis_microscopic.microscopicKind}</td>
        </tr>
        <tr>
            <td></td>
            <td align="center">Норма</td>
            <td align="center">Показатель</td>
        </tr>
        <tr>
            <td>Неорганизованные осадки</td>
            <td align="center">Норма</td>
            <td align="center">${uroscopy_analysis_microscopic.fugitiveRainfall}</td>
        </tr>
        <tr>
            <td>Организованные осадки</td>
            <td align="center">Норма</td>
            <td align="center">${uroscopy_analysis_microscopic.organizedRainfall}</td>
        </tr>
        <tr>
            <td>Лаборант</td>
            <td align="center">Норма</td>
            <td align="center">${uroscopy_analysis_microscopic.microscopicAssistant}</td>
        </tr>
        <tr>
            <td>Заключение</td>
            <td align="center">Норма</td>
            <td align="center">${uroscopy_analysis_microscopic.microscopicConclusion}</td>
        </tr>
        </tbody>
    </table>
        <button class="btn btn-danger" onclick="location.href='${UroscopyAnalysisMicroscopicAdd}'">Провести анализ</button><br><br><br><br>

</div>


<jsp:include page="../fragments/footer.jsp" />

</body>
</html>