<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page session="false" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<html>
<head>
    <title>История болезни</title>
</head>
<body>
<jsp:include page="../fragments/header.jsp"/>

<div class="container" style="width: 50% ">

    <h1 align="center">Оказанные услуги</h1>

    <table class="table table-striped">
        <thead>
        <tr>
            <th>Наименование</th>
            <th>Стоимость, BYN</th>
        </tr>
        </thead>
        <c:set var="total" value="${0}"/>
            <c:forEach var="listServices" items="${listServices}">
                <c:set var="total" value="${total + listServices.receptionAndConsultation.price}" />
            <tr>
                <td>${listServices.receptionAndConsultation.nameService}</td>
                <td>${listServices.receptionAndConsultation.price}</td>
            </tr>
            </c:forEach>
            <tr>
                <td align="right">Итого:</td>
                <td>${total}</td>
            </tr>
    </table>


    <form:form class="form-horizontal" method="post" modelAttribute="servicesForm" action="saveServices">
    <div class="col-sm-5">
        <input class="form-control" type="text" id="search">
    </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn-lg pull-right">Сохранить</button>
            </div>
        </div>
    </form:form>
    <script>$(document).ready(function () {
        $("#search").typeahead({
            source: function (request, response) {
                $.ajax({
                    url: "1/getTags/" + request,
                    dataType: "json",
                    type: "GET",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        var arrBooks = [];
                        response($.map(data, function (item) {
                            arrBooks.push(item.nameService);
                        }))
                        response(arrBooks);

                        $(".dropdown-menu").css("width", "auto");
                        $(".dropdown-menu").css("height", "auto");
                        $(".dropdown-menu").css("font", "15px Verdana");
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        alert(textStatus);
                    }
                });
            },
            hint: true, // SHOW HINT (DEFAULT IS "true").
            highlight: true, // HIGHLIGHT (SET <strong> or <b> BOLD). DEFAULT IS "true".
            minLength: 1 // MINIMUM 1 CHARACTER TO START WITH.
        });
    });
    </script>
    </div>
<jsp:include page="../fragments/footer.jsp"/>
</body>
</html>