<%@ page language="java" contentType="text/html;  charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ page session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html>
<html lang="en">
<jsp:include page="../fragments/header.jsp" />
<body>

<div class="container" >

    <div class="panel-group" id="accordion">

        <h4 class="panel-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#collapse1"><h1>Физико-химическое и биохимическое исследование крови</h1></a><br></h4><br>
                <form:form method="POST" modelAttribute="blood_analysis" class="form-horizontal" action="/BloodAnalysisAdd">
                    <form:input type="hidden" path="id" id="id" />

                        <div id="collapse1" class="panel-collapse collapse">
                            <spring:bind path="dateAnalysis">
                                <div class="form-group ${status.error ? 'has-error' : ''}">
                                    <label class="col-sm-2 control-label">Дата исследования</label>
                                    <div class="col-sm-3">
                                        <div class="input-group date" id="datetimepicker4">
                                            <form:input path="dateAnalysis" type="text" class="form-control" placeholder="Дата исследования" />
                                        <span class="input-group-addon">
                                         </span>
                                        </div>
                                        <script type="text/javascript">
                                            $(function () {
                                                $('#datetimepicker4').datetimepicker(
                                                        {pickTime: false, language: 'ru'}
                                                );
                                            });
                                        </script>
                                    </div>
                                </div>
                            </spring:bind>

                            <spring:bind path="timeAnalysis">
                                <div class="form-group ${status.error ? 'has-error' : ''}">
                                    <label class="col-sm-2 control-label">Время исследования</label>
                                    <div class="col-sm-3">
                                        <div class="input-group date" id="datetimepicker5">
                                            <form:input path="timeAnalysis" type="text" class="form-control" placeholder="Время исследования" />
                                        <span class="input-group-addon">
                                         </span>
                                        </div>
                                        <script type="text/javascript">
                                            $(function () {
                                                $('#datetimepicker5').datetimepicker(
                                                        {pickDate: false, language: 'ru'}
                                                );
                                            });
                                        </script>
                                    </div>
                                </div>
                            </spring:bind>

                            <spring:bind path="owner">
                                <div class="form-group ${status.error ? 'has-error' : ''}">
                                    <label class="col-sm-2 control-label">Владелец</label>
                                    <div class="col-sm-3">
                                        <form:input path="owner" class="form-control" id="owner" placeholder="Ф.И.О." />
                                        <form:errors path="owner" class="control-label" />
                                    </div>
                                </div>
                            </spring:bind>

                            <spring:bind path="kind">
                                <div class="form-group ${status.error ? 'has-error' : ''}">
                                    <label class="col-sm-2 control-label">Животное</label>
                                    <div class="col-sm-3">
                                        <form:input path="kind" class="form-control" id="kind" placeholder="Животное" />
                                        <form:errors path="kind" class="control-label" />
                                    </div>
                                </div>
                            </spring:bind>

                            <spring:bind path="specificGravity">
                                <div class="form-group ${status.error ? 'has-error' : ''}">
                                    <label class="col-sm-2 control-label">Удельный вес</label>
                                    <div class="col-sm-3">
                                        <form:input path="specificGravity" class="form-control" id="specificGravity" placeholder="Удельный вес" />
                                        <form:errors path="specificGravity" class="control-label" />
                                    </div>
                                </div>
                            </spring:bind>

                            <spring:bind path="viscosity">
                                <div class="form-group ${status.error ? 'has-error' : ''}">
                                    <label class="col-sm-2 control-label">Вязкость</label>
                                    <div class="col-sm-3">
                                        <form:input path="viscosity" class="form-control" id="viscosity" placeholder="Вязкость" />
                                        <form:errors path="viscosity" class="control-label" />
                                    </div>
                                </div>
                            </spring:bind>

                            <spring:bind path="soe">
                                <div class="form-group ${status.error ? 'has-error' : ''}">
                                    <label class="col-sm-2 control-label">СОЭ</label>
                                    <div class="col-sm-3">
                                        <form:input path="soe" class="form-control" id="soe" placeholder="СОЭ" />
                                        <form:errors path="soe" class="control-label" />
                                    </div>
                                </div>
                            </spring:bind>

                            <spring:bind path="hemoglobin">
                                <div class="form-group ${status.error ? 'has-error' : ''}">
                                    <label class="col-sm-2 control-label">Гемоглобин</label>
                                    <div class="col-sm-3">
                                        <form:input path="hemoglobin" class="form-control" id="hemoglobin" placeholder="Гемоглобин" />
                                        <form:errors path="hemoglobin" class="control-label" />
                                    </div>
                                </div>
                            </spring:bind>

                            <spring:bind path="calcium">
                                <div class="form-group ${status.error ? 'has-error' : ''}">
                                    <label class="col-sm-2 control-label">Кальций</label>
                                    <div class="col-sm-3">
                                        <form:input path="calcium" class="form-control" id="calcium" placeholder="Кальций" />
                                        <form:errors path="calcium" class="control-label" />
                                    </div>
                                </div>
                            </spring:bind>

                            <spring:bind path="inogranicPhosphorus">
                                <div class="form-group ${status.error ? 'has-error' : ''}">
                                    <label class="col-sm-2 control-label">Неорганический фосфор</label>
                                    <div class="col-sm-3">
                                        <form:input path="inogranicPhosphorus" class="form-control" id="inogranicPhosphorus" placeholder="Неорганический фосфор" />
                                        <form:errors path="inogranicPhosphorus" class="control-label" />
                                    </div>
                                </div>
                            </spring:bind>

                            <spring:bind path="reserveAlkalinity">
                                <div class="form-group ${status.error ? 'has-error' : ''}">
                                    <label class="col-sm-2 control-label">Резервная щелочность</label>
                                    <div class="col-sm-3">
                                        <form:input path="reserveAlkalinity" class="form-control" id="reserveAlkalinity" placeholder="Резервная щелочность" />
                                        <form:errors path="reserveAlkalinity" class="control-label" />
                                    </div>
                                </div>
                            </spring:bind>

                            <spring:bind path="carotene">
                                <div class="form-group ${status.error ? 'has-error' : ''}">
                                    <label class="col-sm-2 control-label">Каротин</label>
                                    <div class="col-sm-3">
                                        <form:input path="carotene" class="form-control" id="carotene" placeholder="Каротин" />
                                        <form:errors path="carotene" class="control-label" />
                                    </div>
                                </div>
                            </spring:bind>

                            <spring:bind path="totalProtein">
                                <div class="form-group ${status.error ? 'has-error' : ''}">
                                    <label class="col-sm-2 control-label">Общий белок</label>
                                    <div class="col-sm-3">
                                        <form:input path="totalProtein" class="form-control" id="totalProtein" placeholder="Общий белок" />
                                        <form:errors path="totalProtein" class="control-label" />
                                    </div>
                                </div>
                            </spring:bind>

                            <spring:bind path="biliRubin">
                                <div class="form-group ${status.error ? 'has-error' : ''}">
                                    <label class="col-sm-2 control-label">Билирубин</label>
                                    <div class="col-sm-3">
                                        <form:input path="biliRubin" class="form-control" id="biliRubin" placeholder="Билирубин" />
                                        <form:errors path="biliRubin" class="control-label" />
                                    </div>
                                </div>
                            </spring:bind>

                            <spring:bind path="glucose">
                                <div class="form-group ${status.error ? 'has-error' : ''}">
                                    <label class="col-sm-2 control-label">Глюкоза</label>
                                    <div class="col-sm-3">
                                        <form:input path="glucose" class="form-control" id="glucose" placeholder="Глюкоза" />
                                        <form:errors path="glucose" class="control-label" />
                                    </div>
                                </div>
                            </spring:bind>

                            <spring:bind path="assistant">
                                <div class="form-group ${status.error ? 'has-error' : ''}">
                                    <label class="col-sm-2 control-label">Лаборант</label>
                                    <div class="col-sm-3">
                                        <form:input path="assistant" class="form-control" id="assistant" placeholder="Ф.И.О." />
                                        <form:errors path="assistant" class="control-label" />
                                    </div>
                                </div>
                            </spring:bind>

                            <spring:bind path="conclusion">
                    <div class="form-group ${status.error ? 'has-error' : ''}">
                        <label class="col-sm-2 control-label">Заключение</label>
                        <div class="col-sm-3">
                            <form:input path="conclusion" class="form-control" id="conclusion" placeholder="Заключение" />
                            <form:errors path="conclusion" class="control-label" />
                        </div>
                    </div>
                </spring:bind>
                        </div>



                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn-lg pull-right">Сохранить</button>
                    </div>
                </div>
            </form:form>



    </div>

</div>


</body>
<jsp:include page="../fragments/footer.jsp" />
</html>