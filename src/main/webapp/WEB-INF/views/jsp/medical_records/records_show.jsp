<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ page session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html>
<html lang="en">

<jsp:include page="../fragments/header.jsp" />
<body>
<div class="container">

    <c:if test="${not empty msg}">
        <div class="alert alert-${css} alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <strong>${msg}</strong>
        </div>
    </c:if>

    <h1 align="center">История болезни № ${medical_records.id} </h1>
    <br />


    <h3 align="center">Владелец</h3><br>

    <div class="row">
        <label class="col-sm-2">Имя</label>
        <div class="col-sm-10">${medical_records.name}</div>
    </div>

    <div class="row">
        <label class="col-sm-2">Фамилия</label>
        <div class="col-sm-10">${medical_records.lastName}</div>
    </div>

    <div class="row">
        <label class="col-sm-2">Отчество</label>
        <div class="col-sm-10">${medical_records.secondName}</div>
    </div>
    <div class="row">
        <label class="col-sm-2">Номер паспорта</label>
        <div class="col-sm-10">${medical_records.numberPassport}</div>
    </div>

    <div class="row">
        <label class="col-sm-2">Мобильный телефон</label>
        <div class="col-sm-10">${medical_records.mobilePhone}</div>
    </div>

    <h3 align="center">Адрес</h3><br>


    <div class="row">
        <label class="col-sm-2">Страна</label>
        <div class="col-sm-10">${medical_records.country}</div>
    </div>

    <div class="row">
        <label class="col-sm-2">Район</label>
        <div class="col-sm-10">${medical_records.district}</div>
    </div>

    <div class="row">
        <label class="col-sm-2">Город</label>
        <div class="col-sm-10">${medical_records.city}</div>
    </div>

    <div class="row">
        <label class="col-sm-2">Улица</label>
        <div class="col-sm-10">${medical_records.street}</div>
    </div>

    <div class="row">
        <label class="col-sm-2">Номер дома</label>
        <div class="col-sm-10">${medical_records.numberHome}</div>
    </div>

    <div class="row">
        <label class="col-sm-2">Номер квартиры</label>
        <div class="col-sm-10">${medical_records.numberFlat}</div>
    </div>

    <div class="row">
        <label class="col-sm-2">Лечащий врач</label>
        <div class="col-sm-10">${medical_records.doctor}</div>
    </div>



    <div class="row">
        <label class="col-sm-2">Отделение</label>
        <div class="col-sm-10">${medical_records.target}</div>
    </div>

    <h3 align="center">Описание живтоного</h3><br>

    <div class="row">
        <label class="col-sm-2">Вид</label>
        <div class="col-sm-10">${medical_records.kind}</div>
    </div>

    <div class="row">
        <label class="col-sm-2">Кличка</label>
        <div class="col-sm-10">${medical_records.animalName}</div>
    </div>

    <div class="row">
        <label class="col-sm-2">Пол</label>
        <div class="col-sm-10">${medical_records.sex}</div>
    </div>

    <div class="row">
        <label class="col-sm-2">Возраст</label>
        <div class="col-sm-10">${medical_records.age}</div>
    </div>

    <div class="row">
        <label class="col-sm-2">Порода</label>
        <div class="col-sm-10">${medical_records.breed}</div>
    </div>

    <div class="row">
        <label class="col-sm-2">Упитанность</label>
        <div class="col-sm-10">${medical_records.fatness}</div>
    </div>

    <h3 align="center">ВетПаспорт</h3><br>

    <div class="row">
        <label class="col-sm-2">Дегильминтизация</label>
        <div class="col-sm-10">${medical_records.deworming}</div>
    </div>

    <div class="row">
        <label class="col-sm-2">Номер</label>
        <div class="col-sm-10">${medical_records.numberVetPassport}</div>
    </div>

    <div class="row">
        <label class="col-sm-2">Дата выдачио</label>
        <div class="col-sm-10">${medical_records.dateOfIssue}</div>
    </div>

    <div class="row">
        <label class="col-sm-2">Кем выдан</label>
        <div class="col-sm-10">${medical_records.issued}</div>
    </div>

    <h3 align="center">Вакцинация</h3><br>

    <div class="row">
        <label class="col-sm-2">Вакцинация</label>
        <div class="col-sm-10">${medical_records.vaccine}</div>
    </div>

    <div class="row">
        <label class="col-sm-2">Дата проведения</label>
        <div class="col-sm-10">${medical_records.dateVaccine}</div>
    </div>

    <div class="row">
        <label class="col-sm-2">Кем проведена</label>
        <div class="col-sm-10">${medical_records.vaccinedBy}</div>
    </div>

    <div class="row">
        <label class="col-sm-2">Особые приметы</label>
        <div class="col-sm-10">${medical_records.specialSigns}</div>
    </div>

    <h3 align="center">Общая информация</h3><br>

    <div class="row">
        <label class="col-sm-2">Дата и время обращения</label>
        <div class="col-sm-10">${medical_records.dataAndTimeRequest}</div>
    </div>

    <div class="row">
        <label class="col-sm-2">Цель обращения</label>
        <div class="col-sm-10">${medical_records.request}</div>
    </div>

    <div class="row">
        <label class="col-sm-2">Предварительный диагноз</label>
        <div class="col-sm-10">${medical_records.preDiagnosis}</div>
    </div>

    <h3 align="center">Общее исследование</h3><br>

    <div class="row">
        <label class="col-sm-2">Дата и время проведения</label>
        <div class="col-sm-10">${medical_records.dateAndTimeAnalysis}</div>
    </div>

    <div class="row">
        <label class="col-sm-2">Температура</label>
        <div class="col-sm-10">${medical_records.temperature}</div>
    </div>

    <div class="row">
        <label class="col-sm-2">Пульс</label>
        <div class="col-sm-10">${medical_records.pulse}</div>
    </div>

    <div class="row">
        <label class="col-sm-2">Дыхание</label>
        <div class="col-sm-10">${medical_records.breath}</div>
    </div>

    <div class="row">
        <label class="col-sm-2">Габитус</label>
        <div class="col-sm-10">${medical_records.habit}</div>
    </div>

    <div class="row">
        <label class="col-sm-2">Исследование кожи</label>
        <div class="col-sm-10">${medical_records.dermisAnalysis}</div>
    </div>

    <div class="row">
        <label class="col-sm-2">Исследование лимфоузлов</label>
        <div class="col-sm-10">${medical_records.lymphAnalysis}</div>
    </div>

    <div class="row">
        <label class="col-sm-2">Исследование слизистых оболочек</label>
        <div class="col-sm-10">${medical_records.mucousAnalysis}</div>
    </div>

    <h3 align="center">Исследование отдельных систем</h3><br>

    <div class="row">
        <label class="col-sm-2">Система органов кровообращения</label>
        <div class="col-sm-10">${medical_records.systemCirculatory}</div>
    </div>

    <div class="row">
        <label class="col-sm-2">Система органов дыхания</label>
        <div class="col-sm-10">${medical_records.systemBreath}</div>
    </div>

    <div class="row">
        <label class="col-sm-2">Система органов пищеварения</label>
        <div class="col-sm-10">${medical_records.systemIndigestion}</div>
    </div>

    <div class="row">
        <label class="col-sm-2">Система органов мочеотделения</label>
        <div class="col-sm-10">${medical_records.systemUrination}</div>
    </div>

    <div class="row">
        <label class="col-sm-2">Система половых органов</label>
        <div class="col-sm-10">${medical_records.systemGenital}</div>
    </div>

    <div class="row">
        <label class="col-sm-2">Молочная железа</label>
        <div class="col-sm-10">${medical_records.systemBreast}</div>
    </div>
    <div class="row">
        <label class="col-sm-2">Система органов движения</label>
        <div class="col-sm-10">${medical_records.systemMovement}</div>
    </div>

    <div class="row">
        <label class="col-sm-2">Органы чувств</label>
        <div class="col-sm-10">${medical_records.systemSenses}</div>
    </div>
    <div class="row">
        <label class="col-sm-2">Нервная система</label>
        <div class="col-sm-10">${medical_records.systemNervous}</div>
    </div>

    <h3 align="center">Специальные исследования</h3><br>

    <div class="row">
        <label class="col-sm-2">Сереологические</label>
        <div class="col-sm-10">${medical_records.serologicalAnalysis}</div>
    </div>
    <div class="row">
        <label class="col-sm-2">Аллергические</label>
        <div class="col-sm-10">${medical_records.allergicAnalysis}</div>
    </div>

    <div class="row">
        <label class="col-sm-2">Бактериологические</label>
        <div class="col-sm-10">${medical_records.bacteriologicalAnalysis}</div>
    </div>
    <div class="row">
        <label class="col-sm-2">Другие</label>
        <div class="col-sm-10">${medical_records.othersAnalysis}</div>
    </div>



</div>

<jsp:include page="../fragments/footer.jsp" />

</body>
</html>