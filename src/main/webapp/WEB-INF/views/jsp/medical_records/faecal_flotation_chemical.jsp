<%@ page language="java" contentType="text/html;  charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ page session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html>
<html lang="en">
<jsp:include page="../fragments/header.jsp" />
<body>
    <div class="container" >
        <div class="panel-group" id="accordion">

            <h4 class="panel-title">
                 <a data-toggle="collapse" data-parent="#accordion" href="#collapse1"><h1>Химический анализ фекалий</h1></a><br></h4><br>
                    <form:form method="POST" modelAttribute="faecal_flotation_chemical" class="form-horizontal" action="/FaecalFlotationChemicalAdd">
                        <form:input type="hidden" path="id" id="id" />
                                <spring:bind path="chemistryDateAnalysis">
                                    <div class="form-group ${status.error ? 'has-error' : ''}">
                                        <label class="col-sm-2 control-label">Дата исследования</label>
                                        <div class="col-sm-3">
                                            <div class="input-group date" id="datetimepicker3">
                                                <form:input path="chemistryDateAnalysis" type="text" class="form-control" placeholder="Дата исследования" />
                                                            <span class="input-group-addon">
                                                             </span>
                                            </div>
                                            <script type="text/javascript">
                                                $(function () {
                                                    $('#datetimepicker3').datetimepicker(
                                                            {pickTime: false, language: 'ru'}
                                                    );
                                                });
                                            </script>
                                        </div>
                                    </div>
                                </spring:bind>

                                <spring:bind path="chemistryTimeAnalysis">
                                    <div class="form-group ${status.error ? 'has-error' : ''}">
                                        <label class="col-sm-2 control-label">Время исследования</label>
                                        <div class="col-sm-3">
                                            <div class="input-group date" id="datetimepicker5">
                                                <form:input path="chemistryTimeAnalysis" type="text" class="form-control" placeholder="Время исследования" />
                                                            <span class="input-group-addon">
                                                             </span>
                                            </div>
                                            <script type="text/javascript">
                                                $(function () {
                                                    $('#datetimepicker5').datetimepicker(
                                                            {pickDate: false, language: 'ru'}
                                                    );
                                                });
                                            </script>
                                        </div>
                                    </div>
                                </spring:bind>

                                <spring:bind path="reaction">
                                    <div class="form-group ${status.error ? 'has-error' : ''}">
                                        <label class="col-sm-2 control-label">Реакция</label>
                                        <div class="col-sm-3">
                                            <form:input path="reaction" class="form-control" id="reaction" placeholder="Реакция" />
                                            <form:errors path="reaction" class="control-label" />
                                        </div>
                                    </div>
                                </spring:bind>

                                <spring:bind path="totalAcidity">
                                    <div class="form-group ${status.error ? 'has-error' : ''}">
                                        <label class="col-sm-2 control-label">Белок</label>
                                        <div class="col-sm-3">
                                            <form:input path="totalAcidity" class="form-control" id="totalAcidity" placeholder="Белок" />
                                            <form:errors path="totalAcidity" class="control-label" />
                                        </div>
                                    </div>
                                </spring:bind>

                                <spring:bind path="protein">
                                    <div class="form-group ${status.error ? 'has-error' : ''}">
                                        <label class="col-sm-2 control-label">Протеозы</label>
                                        <div class="col-sm-3">
                                            <form:input path="protein" class="form-control" id="protein" placeholder="Протеозы" />
                                            <form:errors path="protein" class="control-label" />
                                        </div>
                                    </div>
                                </spring:bind>

                                <spring:bind path="pigmentsBlood">
                                    <div class="form-group ${status.error ? 'has-error' : ''}">
                                        <label class="col-sm-2 control-label">Сахар</label>
                                        <div class="col-sm-3">
                                            <form:input path="pigmentsBlood" class="form-control" id="pigmentsBlood" placeholder="Сахар" />
                                            <form:errors path="pigmentsBlood" class="control-label" />
                                        </div>
                                    </div>
                                </spring:bind>

                                <spring:bind path="cholochrome">
                                    <div class="form-group ${status.error ? 'has-error' : ''}">
                                        <label class="col-sm-2 control-label">Пигменты крови</label>
                                        <div class="col-sm-3">
                                            <form:input path="cholochrome" class="form-control" id="cholochrome" placeholder="Пигменты крови" />
                                            <form:errors path="cholochrome" class="control-label" />
                                        </div>
                                    </div>
                                </spring:bind>

                                <spring:bind path="ammonia">
                                    <div class="form-group ${status.error ? 'has-error' : ''}">
                                        <label class="col-sm-2 control-label">Желчные пигменты</label>
                                        <div class="col-sm-3">
                                            <form:input path="ammonia" class="form-control" id="ammonia" placeholder="Желчные пигменты" />
                                            <form:errors path="ammonia" class="control-label" />
                                        </div>
                                    </div>
                                </spring:bind>

                                <spring:bind path="fermentationTest">
                                    <div class="form-group ${status.error ? 'has-error' : ''}">
                                        <label class="col-sm-2 control-label">Уробилин</label>
                                        <div class="col-sm-3">
                                            <form:input path="fermentationTest" class="form-control" id="fermentationTest" placeholder="Уробилин" />
                                            <form:errors path="fermentationTest" class="control-label" />
                                        </div>
                                    </div>
                                </spring:bind>

                                <spring:bind path="chemistryConclusion">
                                    <div class="form-group ${status.error ? 'has-error' : ''}">
                                        <label class="col-sm-2 control-label">Кетоновые тела</label>
                                        <div class="col-sm-3">
                                            <form:input path="chemistryConclusion" class="form-control" id="chemistryConclusion" placeholder="Кетоновые тела" />
                                            <form:errors path="chemistryConclusion" class="control-label" />
                                        </div>
                                    </div>
                                </spring:bind>
                            </div>

                                <div class="form-group">
                                    <div class="col-sm-offset-2 col-sm-10">
                                        <button type="submit" class="btn-lg pull-right">Сохранить</button>
                                    </div>
                                </div>
                    </form:form>
</body>
</html>
