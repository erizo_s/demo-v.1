<%@ page language="java" contentType="text/html;  charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ page session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html lang="en">
<jsp:include page="../fragments/header.jsp" />
<body>
<h4 class="panel-title">
    <a data-toggle="collapse" data-parent="#accordion" href="#collapse3"><h1>Микроскопические исследования мочи</h1></a><br></h4><br>
            <div id="collapse3" class="panel-collapse collapse">
            <form:form method="POST" modelAttribute="faecal_flotation_microscopic" class="form-horizontal" action="/FaecalFlotationMicroscopicAdd">
                <form:input type="hidden" path="id" id="id" />
                    <spring:bind path="microscopicDateAnalysis">
                        <div class="form-group ${status.error ? 'has-error' : ''}">
                            <label class="col-sm-2 control-label">Дата исследования</label>
                            <div class="col-sm-3">
                                <div class="input-group date" id="datetimepicker3">
                                    <form:input path="microscopicDateAnalysis" type="text" class="form-control" placeholder="Дата исследования" />
                                                            <span class="input-group-addon">
                                                             </span>
                                </div>
                                <script type="text/javascript">
                                    $(function () {
                                        $('#datetimepicker3').datetimepicker(
                                                {pickTime: false, language: 'ru'}
                                        );
                                    });
                                </script>
                            </div>
                        </div>
                    </spring:bind>

                    <spring:bind path="microscopicTimeAnalysis">
                        <div class="form-group ${status.error ? 'has-error' : ''}">
                            <label class="col-sm-2 control-label">Время исследования</label>
                            <div class="col-sm-3">
                                <div class="input-group date" id="datetimepicker5">
                                    <form:input path="microscopicTimeAnalysis" type="text" class="form-control" placeholder="Время исследования" />
                                                            <span class="input-group-addon">
                                                             </span>
                                </div>
                                <script type="text/javascript">
                                    $(function () {
                                        $('#datetimepicker5').datetimepicker(
                                                {pickDate: false, language: 'ru'}
                                        );
                                    });
                                </script>
                            </div>
                        </div>
                    </spring:bind>

                    <spring:bind path="blood">
                        <div class="form-group ${status.error ? 'has-error' : ''}">
                            <label class="col-sm-2 control-label">Кровь</label>
                            <div class="col-sm-3">
                                <form:input path="blood" class="form-control" id="blood" placeholder="Кровь" />
                                <form:errors path="blood" class="control-label" />
                            </div>
                        </div>
                    </spring:bind>

                    <spring:bind path="microscopicSlime">
                        <div class="form-group ${status.error ? 'has-error' : ''}">
                            <label class="col-sm-2 control-label">Слизь</label>
                            <div class="col-sm-3">
                                <form:input path="microscopicSlime" class="form-control" id="microscopicSlime" placeholder="Слизь" />
                                <form:errors path="microscopicSlime" class="control-label" />
                            </div>
                        </div>
                    </spring:bind>

                    <spring:bind path="diseasesByDarling">
                        <div class="form-group ${status.error ? 'has-error' : ''}">
                            <label class="col-sm-2 control-label">Возбудители методом Дарлинга</label>
                            <div class="col-sm-7">
                                <form:input path="diseasesByDarling" class="form-control" id="diseasesByDarling" placeholder="Возбудители методом Дарлинга" />
                                <form:errors path="diseasesByDarling" class="control-label" />
                            </div>
                        </div>
                    </spring:bind>

                    <spring:bind path="diseasesByBermanOrlov">
                        <div class="form-group ${status.error ? 'has-error' : ''}">
                            <label class="col-sm-2 control-label">Возбудители методом Бермана-Орлова</label>
                            <div class="col-sm-7">
                                <form:input path="diseasesByBermanOrlov" class="form-control" id="diseasesByBermanOrlov" placeholder="Возбудители методом Бермана-Орлова" />
                                <form:errors path="diseasesByBermanOrlov" class="control-label" />
                            </div>
                        </div>
                    </spring:bind>

                    <spring:bind path="diseasesByNativeSmear">
                        <div class="form-group ${status.error ? 'has-error' : ''}">
                            <label class="col-sm-2 control-label">Возбудители методом нативного мазка</label>
                            <div class="col-sm-7">
                                <form:input path="diseasesByNativeSmear" class="form-control" id="diseasesByNativeSmear" placeholder="Возбудители методом нативного мазка" />
                                <form:errors path="diseasesByNativeSmear" class="control-label" />
                            </div>
                        </div>
                    </spring:bind>

                    <spring:bind path="diseasesBySuccessiveWashings">
                        <div class="form-group ${status.error ? 'has-error' : ''}">
                            <label class="col-sm-2 control-label">Возбудители методом последовательных промываний</label>
                            <div class="col-sm-7">
                                <form:input path="diseasesBySuccessiveWashings" class="form-control" id="diseasesBySuccessiveWashings" placeholder="Возбудители методом последовательных промываний" />
                                <form:errors path="diseasesBySuccessiveWashings" class="control-label" />
                            </div>
                        </div>
                    </spring:bind>

                    <spring:bind path="microscopicConclusion">
                        <div class="form-group ${status.error ? 'has-error' : ''}">
                            <label class="col-sm-2 control-label">Заключение</label>
                            <div class="col-sm-7">
                                <form:input path="microscopicConclusion" class="form-control" id="microscopicConclusion" placeholder="Заключение" />
                                <form:errors path="microscopicConclusion" class="control-label" />
                            </div>
                        </div>
                    </spring:bind>


                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn-lg pull-right">Сохранить</button>
                        </div>
                    </div>
                </form:form>
</div>

</body>
</html>
