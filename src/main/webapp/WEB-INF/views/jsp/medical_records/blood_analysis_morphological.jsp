<%@ page language="java" contentType="text/html;  charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ page session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html>
<html lang="en">
<jsp:include page="../fragments/header.jsp" />
<body>

<div class="container" >

<div class="panel-group" id="accordion">

   <h1>Морфологическое исследование крови</h1>
<form:form method="POST" modelAttribute="blood_analysis_morphological" class="form-horizontal" action="/BloodAnalysisAddMorphological">
    <form:input type="hidden" path="id" id="id" />
        <spring:bind path="dateAnalysisMorphological">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <label class="col-sm-2 control-label">Дата исследования</label>
                <div class="col-sm-3">
                    <div class="input-group date" id="datetimepicker3">
                        <form:input path="dateAnalysisMorphological" type="text" class="form-control" placeholder="Дата исследования" />
                                                <span class="input-group-addon">
                                                 </span>
                    </div>
                    <script type="text/javascript">
                        $(function () {
                            $('#datetimepicker3').datetimepicker(
                                    {pickTime: false, language: 'ru'}
                            );
                        });
                    </script>
                </div>
            </div>
        </spring:bind>

        <spring:bind path="timeAnalysisMorphological">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <label class="col-sm-2 control-label">Время исследования</label>
                <div class="col-sm-3">
                    <div class="input-group date" id="datetimepicker5">
                        <form:input path="timeAnalysisMorphological" type="text" class="form-control" placeholder="Время исследования" />
                                                <span class="input-group-addon">
                                                 </span>
                    </div>
                    <script type="text/javascript">
                        $(function () {
                            $('#datetimepicker5').datetimepicker(
                                    {pickDate: false, language: 'ru'}
                            );
                        });
                    </script>
                </div>
            </div>
        </spring:bind>

        <spring:bind path="ownerMorphological">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <label class="col-sm-2 control-label">Владелец</label>
                <div class="col-sm-3">
                    <form:input path="ownerMorphological" class="form-control" id="ownerMorphological" placeholder="Ф.И.О." />
                    <form:errors path="ownerMorphological" class="control-label" />
                </div>
            </div>
        </spring:bind>

        <spring:bind path="kindMorphological">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <label class="col-sm-2 control-label">Животное</label>
                <div class="col-sm-3">
                    <form:input path="kindMorphological" class="form-control" id="kindMorphological" placeholder="Животное" />
                    <form:errors path="kindMorphological" class="control-label" />
                </div>
            </div>
        </spring:bind>

        <spring:bind path="redBloodCells">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <label class="col-sm-2 control-label">Эритроциты</label>
                <div class="col-sm-3">
                    <form:input path="redBloodCells" class="form-control" id="redBloodCells" placeholder="Эритроциты" />
                    <form:errors path="redBloodCells" class="control-label" />
                </div>
            </div>
        </spring:bind>

        <spring:bind path="leukocyte">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <label class="col-sm-2 control-label">Лейкоциты</label>
                <div class="col-sm-3">
                    <form:input path="leukocyte" class="form-control" id="leukocyte" placeholder="Лейкоциты" />
                    <form:errors path="leukocyte" class="control-label" />
                </div>
            </div>
        </spring:bind>

        <spring:bind path="m_conclusion">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <label class="col-sm-2 control-label">Заключение</label>
                <div class="col-sm-3">
                    <form:input path="m_conclusion" class="form-control" id="m_conclusion" placeholder="Заключение" />
                    <form:errors path="m_conclusion" class="control-label" />
                </div>
            </div>
        </spring:bind>

        <spring:bind path="assistantMorphological">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <label class="col-sm-2 control-label">Заключение</label>
                <div class="col-sm-3">
                    <form:input path="assistantMorphological" class="form-control" id="assistantMorphological" placeholder="Лаборант" />
                    <form:errors path="assistantMorphological" class="control-label" />
                </div>
            </div>
        </spring:bind>

</div>

    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn-lg pull-right">Сохранить</button>
        </div>
    </div>
</form:form>



</div>




</body>
<jsp:include page="../fragments/footer.jsp" />
</html>