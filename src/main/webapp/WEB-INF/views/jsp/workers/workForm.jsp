<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ page session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html>
<html lang="en">

<jsp:include page="../fragments/header.jsp" />
<body>
<div class="container">


    <form:form class="form-horizontal" method="post" modelAttribute="workForm" action="/workersAdd">

        <form:input type="hidden" path="id" id="id" />

        <spring:bind path="name">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <label class="col-sm-2 control-label">Имя</label>
                <div class="col-sm-3">
                    <form:input path="name" type="text" class="form-control " id="name" placeholder="Имя" />
                    <form:errors path="name" class="control-label" />
                </div>
            </div>
        </spring:bind>

        <spring:bind path="lastName">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <label class="col-sm-2 control-label">Фамилия</label>
                <div class="col-sm-3">
                    <form:input path="lastName" type="text" class="form-control " id="lastName" placeholder="Фамилия" />
                    <form:errors path="lastName" class="control-label" />
                </div>
            </div>
        </spring:bind>

        <spring:bind path="mobilePhone">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <label class="col-sm-2 control-label">Мобильный телефон</label>
                <div class="col-sm-3">
                    <form:input path="mobilePhone" class="form-control" id="mobilePhone" placeholder="Мобильный телефон" />
                    <form:errors path="mobilePhone" class="control-label" />
                </div>
            </div>
        </spring:bind>

        <spring:bind path="position">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <label class="col-sm-2 control-label">Должность</label>
                <div class="col-sm-3">
                    <form:select path="position" class="form-control">
                        <form:option value="NONE" label="--- Select ---" />
                        <form:options items="${positionList}" />
                    </form:select>
                </div>
                <div class="col-sm-3"></div>
            </div>
        </spring:bind>

        <spring:bind path="login">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <label class="col-sm-2 control-label">Логин</label>
                <div class="col-sm-3">
                    <form:input path="login" class="form-control" id="login" placeholder="Логин" />
                    <form:errors path="login" class="control-label" />
                </div>
            </div>
        </spring:bind>

        <spring:bind path="password">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <label class="col-sm-2 control-label">Пароль</label>
                <div class="col-sm-3">
                    <form:input path="password" class="form-control" id="password" placeholder="Пароль" />
                    <form:errors path="password" class="control-label" />
                </div>
            </div>
        </spring:bind>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn-lg btn-primary pull-right">Добавить клиента</button>
            </div>
        </div>
    </form:form>

</div>

<jsp:include page="../fragments/footer.jsp" />

</body>
</html>