<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page session="false" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html lang="en">

<jsp:include page="../fragments/header.jsp"/>

<body>
<div class="container">

    <h1 align="center">Cписок клиентов</h1>

    <table class="table table-striped">
        <thead>
        <tr>
            <th>Имя</th>
            <th>Фамилия</th>
            <th>Мобильный телефон</th>
            <th>Должность</th>

        </tr>
        </thead>


        <c:forEach var="workers" items="${workers}">

            <tr>

                <td>${workers.name}</td>
                <td>${workers.lastName}</td>
                <td>${workers.mobilePhone}</td>
                <td>${workers.position}</td>


                <spring:url value="/workers/add" var="addUrl"/>
                <spring:url value="/workers/${workers.id}/delete" var="deleteUrl"/>
                <spring:url value="/workers/${workers.id}/update" var="updateUrl"/>


                <td>
                    <button class="btn btn-primary" onclick="location.href='${updateUrl}'">Изменить</button>
                    <button class="btn btn-danger" onclick="location.href='${deleteUrl}'">Удалить</button>
                </td>


            </tr>
        </c:forEach>

    </table>
    <spring:url value="/workers/add" var="addUrl"/>
    <td>
        <button class="btn btn-warning" onclick="location.href='${addUrl}'">Добавить</button>
</div>

<jsp:include page="../fragments/footer.jsp"/>

</body>
</html>