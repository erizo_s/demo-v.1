<%@ page language="java" contentType="text/html;  charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ page session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>
<head>
    <jsp:include page="../jsp/fragments/header.jsp" />
</head>
<body>


<div class="col-sm-3">
    <input class="form-control" type="text" id="search">
</div>
<script>$(document).ready(function () {
    $("#search").typeahead({
        source: function (request, response) {
            $.ajax({
                url: "getTags/" + request,
                dataType: "json",
                type: "GET",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    var arrBooks = [];
                    response($.map(data, function (item) {
                        arrBooks.push(item.name);
                    }))
                    response(arrBooks);

// SET THE WIDTH AND HEIGHT OF UI AS "auto" ALONG WITH FONT.
// YOU CAN CUSTOMIZE ITS PROPERTIES.
                    $(".dropdown-menu").css("width", "auto");
                    $(".dropdown-menu").css("height", "auto");
                    $(".dropdown-menu").css("font", "15px Verdana");
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert(textStatus);
                }
            });
        },
        hint: true, // SHOW HINT (DEFAULT IS "true").
        highlight: true, // HIGHLIGHT (SET <strong> or <b> BOLD). DEFAULT IS "true".
        minLength: 1 // MINIMUM 1 CHARACTER TO START WITH.
    });
});
</script>

</body>
</html>