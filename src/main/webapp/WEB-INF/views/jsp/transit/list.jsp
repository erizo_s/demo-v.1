<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page session="false" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <title>VetClient</title>
</head>
<body>
<jsp:include page="../fragments/header.jsp"/>

<div class="container">

    <spring:url value="/invoice" var="urlShowAllInvoices"/>
    <spring:url value="/delivered" var="addToOrder"/>

    <h1 align="center">Транзит</h1>
    <table class="table table-striped">
        <thead>
        <tr>
            <th>Номер №</th>
            <th>Имя</th>
            <th>Объем</th>
            <th>Цена</th>
        </tr>
        </thead>

        <c:forEach var="transit" items="${transit}">
            <tr>
                <td>${transit.id}</td>
                <td>${transit.name}</td>
                <td>${transit.amount}</td>
                <td>${transit.price}</td>
            </tr>
        </c:forEach>
    </table>
</div>
<jsp:include page="../fragments/footer.jsp"/>
</body>
</html>