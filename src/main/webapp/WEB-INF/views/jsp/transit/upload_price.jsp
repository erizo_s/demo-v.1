<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Загрузка прайса</title>
    <%--<!-- 7. Подключить CSS виджета "Bootstrap upload" -->--%>
    <link rel="stylesheet" href="../resources/css/upload.css" />
</head>
<body>
<jsp:include page="../fragments/header.jsp" />
<form:form method="POST" modelAttribute="upload_price"
           enctype="multipart/form-data">

    <div class="file-upload" data-text="Выберите файл">
    <input type="file">

    </div>
</form:form>


<spring:url value="/upload_price" var="upload" />
<spring:url value="/update_price" var="update" />

<div style="text-align: center; float: left; margin-left: 40%"><button class="btn btn-danger" onclick="location.href='${upload}'">Загрузка прайса</button></div>
<div style="text-align: center; float: right; margin-right: 40%"><button class="btn btn-success" onclick="location.href='${update}'">Обновление прайса</button></div>
<br><br><br><br><br>
<jsp:include page="../fragments/footer.jsp" />
</body>
</html>
