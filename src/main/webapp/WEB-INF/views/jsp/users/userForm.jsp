<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ page session="false" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html lang="en">


<jsp:include page="../fragments/header.jsp"/>

<body>
<div class="container">


    <form:form method="POST" modelAttribute="userForm" class="form-horizontal" action="/usersAdd">
        <form:input type="hidden" path="id" id="id"/>

        <spring:bind path="name">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <label class="col-sm-2 control-label">Имя</label>
                <div class="col-sm-3">
                    <form:input path="name" type="text" class="form-control " id="name" placeholder="Имя"/>
                </div>
            </div>
        </spring:bind>

        <spring:bind path="lastName">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <label class="col-sm-2 control-label">Фамилия</label>
                <div class="col-sm-3">
                    <form:input path="lastName" type="text" class="form-control " id="lastName" placeholder="Фамилия"/>
                    <form:errors path="lastName" class="control-label"/>
                </div>
            </div>
        </spring:bind>

        <spring:bind path="email">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <label class="col-sm-2 control-label">Email</label>
                <div class="col-sm-3">
                    <form:input path="email" class="form-control" id="email" placeholder="Email" type="email"/>
                    <form:errors path="email" class="control-label"/>
                </div>
            </div>
        </spring:bind>

        <spring:bind path="mobilePhone">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <label class="col-sm-2 control-label">Телефон</label>
                <div class="col-sm-3">
                    <form:input path="mobilePhone" type="text" class="form-control " id="mobilePhone" placeholder="Телефон"/>
                    <form:errors path="mobilePhone" class="control-label"/>
                </div>
            </div>
        </spring:bind>

        <spring:bind path="country">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <label class="col-sm-2 control-label">Оператор</label>
                <div class="col-sm-3">
                    <form:select path="country" class="form-control" >
                        <form:option value="NONE" label="--- Select ---"/>
                        <form:options items="${countryList}"/>
                    </form:select>
                </div>
                <div class="col-sm-3"></div>
            </div>
        </spring:bind>

        <c:choose>
            <c:when  test="${mode eq 'edit'}">
                <spring:bind path="idDepartment">
                    <div class="form-group ${status.error ? 'has-error' : ''}">
                        <label class="col-sm-2 control-label">Цель обращения</label>
                        <div class="col-sm-3">
                            <form:select path="idDepartment" class="form-control" required="required">
                                <form:option  label="--Select--"  value=""/>
                                <form:options items="${listDepartment}" itemValue="id" itemLabel="nameDepartment"  required="required" />
                            </form:select>
                        </div>
                        <div class="col-sm-4"></div>
                    </div>
                </spring:bind>
            </c:when>

            <c:otherwise>
                <spring:bind path="idDepartment">
                    <div class="form-group ${status.error ? 'has-error' : ''}">
                        <label class="col-sm-2 control-label">Цель обращения</label>
                        <div class="col-sm-3">
                            <form:select path="idDepartment" class="form-control" required="required">
                                <form:option label="--Select--" value=""/>
                                <form:options items="${listDepartment}" itemValue="id" itemLabel="nameDepartment"  required="required"/>
                            </form:select>
                        </div>
                        <div class="col-sm-4"></div>
                    </div>
                </spring:bind>
            </c:otherwise>
        </c:choose>

        <c:choose>
            <c:when  test="${mode eq 'edit'}">
                <spring:bind path="idDoctor">
                    <div class="form-group ${status.error ? 'has-error' : ''}">
                        <label class="col-sm-2 control-label">Лечащий врач</label>
                        <div class="col-sm-3">
                            <form:select path="idDoctor" class="form-control" required="required">
                                <form:option  label="--Select--"  value=""/>
                                <form:options items="${listDoctors}" itemValue="id" itemLabel="doctorName"  required="required"/>
                            </form:select>
                        </div>
                        <div class="col-sm-4"></div>
                    </div>
                </spring:bind>
            </c:when>

            <c:otherwise>
                <spring:bind path="idDoctor">
                    <div class="form-group ${status.error ? 'has-error' : ''}">
                        <label class="col-sm-2 control-label">Лечащий врач</label>
                        <div class="col-sm-3">
                            <form:select path="idDoctor" class="form-control" required="required">
                                <form:option label="--Select--" value=""/>
                                <form:options items="${listDoctors}" itemValue="id" itemLabel="doctorName"  required="required"/>
                            </form:select>
                        </div>
                        <div class="col-sm-4"></div>
                    </div>
                </spring:bind>
            </c:otherwise>
        </c:choose>




        <hr align="center" width="1200" color="Red"/>
        <h1 align="center">Первичный анамнез</h1><br><br>

        <spring:bind path="animal">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <label class="col-sm-2 control-label">Вид животного</label>
                <div class="col-sm-3">
                    <form:select path="animal" class="form-control">
                        <form:option value="NONE" label="--- Select ---"/>
                        <form:options items="${animalList}"/>
                    </form:select>
                </div>
                <div class="col-sm-3"></div>
            </div>
        </spring:bind>

        <spring:bind path="passport">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <label class="col-sm-2 control-label">ВетПаспорт</label>
                <div class="col-sm-3">
                    <form:select path="passport" class="form-control">
                        <form:option value="NONE" label="--- Select ---"/>
                        <form:options items="${passportlList}"/>
                    </form:select>
                </div>
                <div class="col-sm-3"></div>
            </div>
        </spring:bind>


        <spring:bind path="vaccination">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <label class="col-sm-2 control-label">Вакцинация</label>
                <div class="col-sm-3">
                    <form:select path="vaccination" class="form-control">
                        <form:option value="NONE" label="--- Select ---"/>
                        <form:options items="${vaccinationList}"/>
                    </form:select>
                </div>
                <div class="col-sm-3"></div>
            </div>
        </spring:bind>

        <spring:bind path="symptoms">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <label class="col-sm-2 control-label">Симптомы</label>
                <div class="col-sm-3">
                    <form:input path="symptoms" type="text" class="form-control " id="symptoms" placeholder="Симптомы"/>
                </div>
            </div>
        </spring:bind>

        <spring:bind path="status">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <label class="col-sm-2 control-label">Статус заявки</label>
                <div class="col-sm-3">
                    <form:select path="status" class="form-control">
                        <form:option value="NONE" label="--- Select ---"/>
                        <form:options items="${statusList}"/>
                    </form:select>
                </div>
                <div class="col-sm-3"></div>
            </div>
        </spring:bind>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn-lg btn-primary pull-right">Добавить клиента</button>
            </div>
        </div>
    </form:form>


</div>


<jsp:include page="../fragments/footer.jsp"/>
</body>

</html>