<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page session="false" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>VetClient</title>
    <jsp:include page="../fragments/header.jsp"/>
</head>
<body>


<div class="container-fluid">
    <form:form method="POST" modelAttribute="dates"  action="/searchForDates">
        <div class="row">
            <div class="col-md-2">
                <div class="form-group">
                    <div class="input-group date" id="datetimepicker8">
                        <input type="date" name="minDate" class="form-control"  required="required"/>
        <span class="input-group-addon">
          <span class="glyphicon glyphicon-calendar"></span>
        </span>
                    </div>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <div class="input-group date" id="datetimepicker9">
                        <input type="date" name="maxDate" class="form-control"  required="required"/>
        <span class="input-group-addon">
          <span class="glyphicon glyphicon-calendar"></span>
        </span>


                    </div>
                </div>
            </div>
            <button type="submit" class="btn btn-success">Показать</button>
        </div>

        <script type="text/javascript">
            $(function () {
                //Инициализация datetimepicker8 и datetimepicker9
                $("#datetimepicker8").datetimepicker({pickTime: false, language: 'ru'});
                $("#datetimepicker9").datetimepicker({pickTime: false, language: 'ru'});
                //При изменении даты в 8 datetimepicker, она устанавливается как минимальная для 9 datetimepicker
                $("#datetimepicker8").on("dp.change", function (e) {
                    $("#datetimepicker9").data("DateTimePicker").setMinDate(e.date);
                });
                //При изменении даты в 9 datetimepicker, она устанавливается как максимальная для 8 datetimepicker
                $("#datetimepicker9").on("dp.change", function (e) {
                    $("#datetimepicker8").data("DateTimePicker").setMaxDate(e.date);
                });
            });
        </script><br><br>

    </form:form>
    <div class="row">
        <div class="col-md-6">

            <form:form method="get" action="search">
                <div id="custom-search-input">
                    <div class="input-group col-md-4">
                        <input type="text" name="searchText" class="form-control input-sm"
                               placeholder="Поиск по фамилии" required="required"/>
                            <span class="input-group-btn">
                                <button class="btn btn-info btn-sm" type="submit"><i
                                        class="glyphicon glyphicon-search"></i></button>
                            </span>
                    </div>
                </div>
            </form:form>
        </div>
    </div>



    <h1 align="center">Cписок заявок</h1>
    <div class="table-responsive">
        <table class="table table-condensed table-striped">
            <thead>
            <tr align="center">
                <th>Время обращения</th>
                <th>Имя</th>
                <th>Фамилия</th>
                <th>Оператор</th>
                <th>Телефон</th>
                <th>Цель обращения</th>
                <th>Лечащий врач</th>
                <th>Статус</th>
                <th>Просмотр</th>
            </tr>
            </thead>

            <c:forEach var="users" items="${users}">

                <tr>

                    <td>${users.time}</td>
                    <td>${users.name}</td>
                    <td>${users.lastName}</td>
                    <td>${users.country}</td>
                    <td>${users.mobilePhone}</td>
                    <td>${users.department.nameDepartment}</td>
                    <td>${users.doctor.doctorName}</td>
                    <td>${users.status}</td>

                    <spring:url value="/users/${users.id}" var="userUrl"/>
                    <spring:url value="/users/${users.id}/delete" var="deleteUrl"/>
                    <spring:url value="/users/${users.id}/update" var="updateUrl"/>

                    <td>
                        <button class="btn btn-info btn-sm" onclick="location.href='${userUrl}'">Просмотр</button>
                        <button class="btn btn-primary btn-sm" onclick="location.href='${updateUrl}'">Изменить</button>
                        <button class="btn btn-danger btn-sm" onclick="location.href='${deleteUrl}'">Удалить</button>
                    </td>
                </tr>
            </c:forEach>
        </table>
    </div>
</div>
<jsp:include page="../fragments/footer.jsp"/>
</body>
</html>