<%@ page language="java" contentType="text/html;  charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ page session="false" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>VetClient</title>

    <c:set var="contextPath" value="${pageContext.request.contextPath}"/>

    <!-- 1. Подключить CSS платформы Twitter Bootstrap 3 -->
    <link rel="stylesheet" href="${contextPath}/resources/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="${contextPath}/resources/css/bootstrap-theme.min.css"/>
    <link rel="stylesheet" href="${contextPath}/resources/core/main.css"/>
    <!-- 2. Подключить CSS виджета "Bootstrap datetimepicker" -->
    <link rel="stylesheet" href="${contextPath}/resources/css/bootstrap-datetimepicker.min.css"/>
    <!-- 3. Подключить библиотеку jQuery -->
    <script type="text/javascript" src="${contextPath}/resources/js/jquery-1.11.1.min.js"></script>
    <!-- 4. Подключить скрипт moment-with-locales.min.js для работы с датами -->
    <script type="text/javascript" src="${contextPath}/resources/js/moment-with-locales.min.js"></script>
    <!-- 5. Подключить скрипт платформы Twitter Bootstrap 3 -->
    <script type="text/javascript" src="${contextPath}/resources/js/bootstrap.min.js"></script>
    <!-- 6. Подключить скрипт виджета "Bootstrap datetimepicker" -->
    <script type="text/javascript" src="${contextPath}/resources/js/bootstrap-datetimepicker.min.js"></script>
    <script type="text/javascript" src="${contextPath}/resources/core/jquery.autocomplete.min.js"></script>
    <!-- 7. Подключить библиотеку Typeahead -->
    <script type="text/javascript" src="${contextPath}/resources/js/bootstrap-typeahead.js"></script>

</head>
<title>VetClient</title>
<spring:url value="/" var="urlHome"/>
<spring:url value="/users/add" var="urlAddUser"/>
<spring:url value="/workers" var="urlAddWorker"/>
<spring:url value="/users" var="urlAllUsers"/>
<spring:url value="/medical_records" var="urlAddMedicalRecords"/>
<spring:url value="/medical_records/add" var="addUrl"/>
<spring:url value="/transit" var="transit"/>
<spring:url value="/upload" var="upload"/>
<spring:url value="/storage" var="urlShowAllStorage"/>
<spring:url value="/storage/add" var="addPos"/>
<spring:url value="/invoice" var="urlShowAllInvoices"/>
<spring:url value="/invoice/add" var="addInvoice"/>
<spring:url value="/order_transit" var="orderTransit"/>
<spring:url value="/medical_services/all" var="urlShowAllServices"/>
<nav class="navbar navbar-inverse">
    <div class="container">
        <div class="navbar-header">
            <button class="navbar-toggle collapsed" type="button" data-toggle="collapse" data-target=".js-navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="${urlHome}" style="padding:15px 15px;">VetClient</a>
        </div>
        <div class="collapse navbar-collapse js-navbar">
            <ul class="nav navbar-nav">
                <li><a href="${urlAllUsers}">Список заявок</a></li>
                <li class="dropdown">
                    <a id="drop1" href="#" class="dropdown-toggle" data-toggle="dropdown">
                        Заявки
                        <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="${urlAddUser}">Создать заявку</a></li>
                        <%--&lt;%&ndash;<li class="divider"></li>&ndash;%&gt;   //  разделитель--%>
                        <%--<li><a href="#">Текст подпункта</a></li>--%>
                    </ul>
                </li>
                <li class="dropdown">
                    <a id="drop1" href="#" class="dropdown-toggle" data-toggle="dropdown">
                        Медицинские карты
                        <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="${urlAddMedicalRecords}">Список карт</a></li>
                        <li class="divider"></li>
                        <li><a href="${addUrl}">Добавить карту</a></li>
                    </ul>
                </li>
            </ul>
            <ul class="nav navbar-nav">
                <li class="dropdown">
                    <a id="drop1" href="#" class="dropdown-toggle" data-toggle="dropdown">
                        Транзит
                        <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="${transit}">Список</a></li>
                        <li><a href="${upload}">Загрузка прайса</a></li>
                        <li><a href="${orderTransit}">Сделать заказ</a></li>
                    </ul>
                </li>
            </ul>
            <ul class="nav navbar-nav">
                <li class="dropdown">
                    <a id="drop1" href="#" class="dropdown-toggle" data-toggle="dropdown">
                        Счета
                        <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="${urlShowAllInvoices}">Список счетов</a></li>
                        <li><a href="${addInvoice}">Выставить счет</a></li>
                    </ul>
                </li>
            </ul>
            <ul class="nav navbar-nav">
                <li class="dropdown">
                    <a id="drop1" href="#" class="dropdown-toggle" data-toggle="dropdown">Склад<span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="${urlShowAllStorage}">Отобразить склад</a></li>
                        <li><a href="${addPos}">Добавить позицию на склад</a></li>
                    </ul>
                </li>
            </ul>
            <ul class="nav navbar-nav">
                <li class="dropdown">
                    <a id="drop1" href="#" class="dropdown-toggle" data-toggle="dropdown">Услуги<span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="${urlShowAllServices}">Список услуг</a></li>
                    </ul>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <a id="drop1" href="#" class="dropdown-toggle" data-toggle="dropdown">
                        Настройки
                        <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="${urlAddWorker}">Добавить работника</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>
</html>