<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <%--<title>VetClient</title>--%>



    <jsp:include page="../fragments/header.jsp" />
</head>


<body>

<h1 align="center">Склад</h1>

<table class="table table-striped">
    <thead>
    <tr>
        <th align="center">Артикул</th>
        <th align="center">Наименование</th>
        <th align="center">Количество</th>
        <th align="center">Цена</th>
    </tr>
    </thead>


    <c:forEach var="storage" items="${storage}">

        <tr>
            <td>${storage.sku}</td>
            <td>${storage.name}</td>
            <td>${storage.amount}</td>
            <td>${storage.price}</td>
        </tr>
    </c:forEach>
</table>



<jsp:include page="../fragments/footer.jsp" />

</body>
</html>