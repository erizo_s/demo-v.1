<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ page session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <%--<!-- Latest compiled and minified CSS -->--%>
    <%--<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">--%>

    <%--<!-- Optional theme -->--%>
    <%--<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">--%>

    <%--<!-- Latest compiled and minified JavaScript -->--%>
    <%--<script src="//netdna.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>--%>

    <jsp:include page="../fragments/header.jsp" />
</head>
<body>
<div class="container">
    <div class="well lead" align="center">Добавить товар</div>

    <form:form method="POST" modelAttribute="new_position_storage" class="form-horizontal" action="/addPos">
        <form:input type="hidden" path="id" id="id"/>

        <spring:bind path="sku">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <label class="col-sm-2 control-label">Артикул</label>
                <div class="col-sm-3">
                    <form:input path="sku" type="text" class="form-control " id="sku" placeholder="Артикул" />
                </div>
            </div>
        </spring:bind>

        <spring:bind path="doctorName">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <label class="col-sm-2 control-label">Наименование</label>
                <div class="col-sm-3">
                    <form:input path="doctorName" type="text" class="form-control " id="doctorName" placeholder="Наименование" />
                    <form:errors path="doctorName" class="control-label" />
                </div>
            </div>
        </spring:bind>

        <spring:bind path="amount">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <label class="col-sm-2 control-label">Количество</label>
                <div class="col-sm-3">
                    <form:input path="amount" class="form-control" id="amount" placeholder="Количество" />
                    <form:errors path="amount" class="control-label" />
                </div>
            </div>
        </spring:bind>

        <spring:bind path="price">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <label class="col-sm-2 control-label">Цена</label>
                <div class="col-sm-3">
                    <form:input path="price" type="text" class="form-control " id="price" placeholder="Цена" />
                    <form:errors path="price" class="control-label" />
                </div>
            </div>
        </spring:bind>

        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn-lg btn-primary pull-right">Добавить товар</button>
            </div>
        </div>
    </form:form>


</div>
</body>

<jsp:include page="../fragments/footer.jsp" />


</html>