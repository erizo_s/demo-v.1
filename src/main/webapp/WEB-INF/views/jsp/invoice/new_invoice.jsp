<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ page session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <%--<!-- Latest compiled and minified CSS -->--%>
    <%--<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">--%>

    <%--<!-- Optional theme -->--%>
    <%--<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">--%>

    <%--<!-- Latest compiled and minified JavaScript -->--%>
    <%--<script src="//netdna.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>--%>

    <jsp:include page="../fragments/header.jsp" />
</head>
<body>
<div class="container">
    <h1 align="center">Выставить счет</h1><br>

    <form:form method="POST" modelAttribute="new_invoice" class="form-horizontal" action="/addInvoice">
        <form:input type="hidden" path="id" id="id"/>

        <spring:bind path="number_order">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <label class="col-sm-2 control-label">Номер МедКарты</label>
                <div class="col-sm-3">
                    <form:input path="number_order" type="text" class="form-control " id="number_order" placeholder="Номер МедКарты" />
                </div>
            </div>
        </spring:bind>



        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn-lg btn-primary pull-right">Выставить счет</button>
            </div>
        </div>
    </form:form>


</div>
</body>

<jsp:include page="../fragments/footer.jsp" />


</html>