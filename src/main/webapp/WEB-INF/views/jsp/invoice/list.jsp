<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>VetClient</title>


    <jsp:include page="../fragments/header.jsp" />
</head>


<body>

<c:if test="${not empty msg}">
    <div class="alert alert-${css} alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <strong>${msg}</strong>
    </div>
</c:if>

<h1 align="center">Счета</h1>

<table class="table table-striped">
    <thead>
    <tr>
        <th align="center">Номер счета</th>
        <th align="center">Номер медкарты</th>
        <th align="center">Дата</th>
        <th align="center">Ф.И.О</th>
        <th align="center">Артикул</th>
        <th align="center">Наименование</th>
        <th align="center">Количество</th>
        <th align="center">Цена</th>
        <th align="center">Заказ</th>
        <th align="center">Итоговая стоимость</th>
        <th align="center"><div class="btn-group">
            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">Статус заказа <span class="caret"></span></button>
            <ul class="dropdown-menu" role="menu">
                <li><a href="#">Ожидание</a></li>
                <li class="divider"></li>
                <li><a href="#">Оплачен</a></li>
            </ul>
        </div></th>
    </tr>
    </thead>




    <c:forEach var="invoice" items="${invoice}">

        <tr>
            <td>${invoice.id}</td>
            <td>${invoice.number_order}</td>
            <td>${invoice.date}</td>
            <td>${invoice.doctorName}</td>
            <td>${invoice.sku}</td>
            <td>${invoice.nomination}</td>
            <td>${invoice.amount}</td>
            <td>${invoice.price}</td>
            <td>${invoice.order}</td>
            <td>${invoice.total_price}</td>
            <td>${invoice.status}</td>
        </tr>
    </c:forEach>
</table>



<jsp:include page="../fragments/footer.jsp" />

</body>
</html>