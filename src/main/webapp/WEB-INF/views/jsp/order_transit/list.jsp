<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <title>VetClient</title>
</head>
<body>
<jsp:include page="../fragments/header.jsp" />
<spring:url value="/delivered_transit" var="orderAdd" />
<div class="container">
    <h1 align="center">Транзит</h1>
    <form:form method="POST" modelAttribute="list" class="form-horizontal" action="/delivered_transit">


            <spring:bind path="sku">
                <div class="form-group ${status.error ? 'has-error' : ''}">
                    <label class="col-sm-2 control-label">Артикул</label>
                    <div class="col-sm-3">
                        <form:input path="sku" type="text" class="form-control " id="sku" placeholder="Артикул" /><br>
                        <button type="submit" class="btn btn-danger">Заказать товар</button>
                    </div>
                </div>
            </spring:bind>
    </form:form>
    <table class="table table-striped">
        <thead>
        <tr>
            <th>Номер №</th>
            <th>Имя</th>
            <th>Объем</th>
            <th>Цена</th>
        </tr>
        </thead>

        <c:forEach var="order_transit"  items="${order_transit}">
            <tr>
                <td>${order_transit.sku}</td>
                <td>${order_transit.name}</td>
                <td>${order_transit.amount}</td>
                <td>${order_transit.price}</td>
            </tr>
        </c:forEach>
    </table>

    <spring:url value="/goToStorage" var="goToStorage" />
    <button class="btn btn-danger" onclick="location.href='${goToStorage}'">Отправить товар на склад</button>
</div>
<jsp:include page="../fragments/footer.jsp" />
</body>
</html>