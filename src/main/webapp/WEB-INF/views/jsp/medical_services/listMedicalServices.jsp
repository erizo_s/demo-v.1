<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page session="false" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html lang="en">

<jsp:include page="../fragments/header.jsp"/>

<body>
<div class="container">

    <h1 align="center">Cписок услуг</h1>

    <table class="table table-striped">
        <thead>
        <tr>
            <th>Типы услуг</th>
        </tr>
        </thead>


        <c:forEach var="medical_services" items="${medical_services}">

            <tr>
                <td>${medical_services.nameService}</td>

                <spring:url value="/medical_services/${medical_services.id}/all" var="showUrl"/>
                <spring:url value="/medical_services/${medical_services.id}/delete" var="deleteUrl"/>

                <td>
                    <button class="btn btn-info" onclick="location.href='${showUrl}'">Просмотр</button>
                    <button class="btn btn-danger" onclick="location.href='${deleteUrl}'">Удалить</button>
                </td>
            </tr>
        </c:forEach>

    </table>

        <form:form class="form-horizontal" method="post" modelAttribute="medicalServiceForm" action="save">
            <form:input type="hidden" path="id" id="id" />

            <spring:bind path="nameService">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <label class="col-sm-2 control-label">Тип услуги</label>
                <div class="col-sm-3">
                    <form:input path="nameService" type="text" class="form-control " id="nameService" placeholder="Тип услуги" />
                    <form:errors path="nameService" class="control-label" />
                </div>
            </div>
            </spring:bind>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-danger">Добавить услугу</button>
                </div>
            </div>
        </form:form>
</div>

<jsp:include page="../fragments/footer.jsp"/>

</body>
</html>