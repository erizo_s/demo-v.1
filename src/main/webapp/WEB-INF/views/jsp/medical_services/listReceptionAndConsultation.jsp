<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page session="false" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html lang="en">

<jsp:include page="../fragments/header.jsp"/>

<body>
<div class="container" style="width: 65%">

    <h1 align="center">Прием и консультации</h1>

    <table class="table table-striped">
        <thead>
        <tr>
            <th>Название услуги</th>
            <th>Стоимость, BYN</th>

        </tr>
        </thead>


        <c:forEach var="reception_and_consultation" items="${reception_and_consultation}">

            <tr>
                <td>${reception_and_consultation.nameService}</td>
                <td>${reception_and_consultation.price}</td>



                <spring:url value="/medical_services/1/${reception_and_consultation.id}/edit" var="showUrl"/>
                <spring:url value="/medical_services/${reception_and_consultation.id}/deleteReception" var="deleteUrl"/>

                <td>
                    <button class="btn btn-info" onclick="location.href='${showUrl}'">Изменить</button>
                    <button class="btn btn-danger" onclick="location.href='${deleteUrl}'">Удалить</button>
                </td>
            </tr>
        </c:forEach>

    </table>

    <form:form class="form-horizontal" method="post" modelAttribute="ReceptionAndConsultationForm" action="saveReception">
        <form:input type="hidden" path="id" id="id" />

        <spring:bind path="nameService">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <label class="col-sm-2 control-label">Название услуги</label>
                <div class="col-sm-3">
                    <form:input path="nameService" type="text" class="form-control " id="nameService" placeholder="Название услуги" />
                    <form:errors path="nameService" class="control-label" />
                </div>
            </div>
        </spring:bind>

        <spring:bind path="price">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <label class="col-sm-2 control-label">Стоимость, BYN</label>
                <div class="col-sm-3">
                    <form:input path="price" type="text" class="form-control " id="price" placeholder="Стоимость, BYN" />
                    <form:errors path="price" class="control-label" />
                </div>
            </div>
        </spring:bind>


        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-danger">Добавить услугу</button>
            </div>
        </div>
    </form:form>
</div>

<jsp:include page="../fragments/footer.jsp"/>

</body>
</html>